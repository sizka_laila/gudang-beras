<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('pembelian_m');
        $this->load->model('supplier_m');
        $this->load->model('pegawai_m');
        $this->load->model('barang_m');
        $this->load->model('master_m');
        $this->load->model('monitoring_m');
        $this->load->model('laporan_m');
        $this->load->library('message');
        $this->load->library('pdf');
        $this->load->helper('date');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=2 OR $this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $data['pembelian'] = $this->pembelian_m->tampil_data()->result();
        $this->load->view('pembelian/index', $data);
	}

	public function tambah(){
        $data['supplier'] = $this->supplier_m->tampil_data()->result();
        $data['pegawai'] = $this->pegawai_m->tampil_data()->result();
        $data['barang'] = $this->barang_m->tampil_data()->result();
        $data['kode'] = $this->pembelian_m->buat_kode();
        $this->load->view('pembelian/tambah_form', $data);
	}

    public function cari_barang($id_barang){
        $data = $this->barang_m->cari_data($id_barang)->row();
        echo json_encode($data);
    }

    public function tambah_aksi(){
        $this->db->trans_start();
        $id_pembelian = $this->input->post('id_pembelian');
        $id_user = $this->input->post('id_user');
        $id_transaksi = $this->input->post('id_transaksi');
        $tanggal_pembelian = $this->input->post('tanggal_pembelian');
        $id_supplier = $this->input->post('id_supplier');
        $cara_beli = $this->input->post('cara_beli');
        $id_pengambil = $this->input->post('id_pengambil');
        $nama_pengantar = $this->input->post('nama_pengantar');
        $total_beli = $this->input->post('total_beli');
        $ppn_header = $this->input->post('ppn_header');
        $nominal_ppn_header = $this->input->post('nominal_ppn_header');
        $diskon = $this->input->post('diskon');
        // $potongan = $this->input->post('potongan');
        $total_bayar = $this->input->post('total_bayar');
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $non_tunai = $this->input->post('non_tunai');
        $barang = $this->input->post('barang');
        $data_pembelian = array(
            'id_pembelian' => $id_pembelian,
            'id_user' => $id_user,
            'id_transaksi' => $id_transaksi,
            'tanggal_pembelian' => date('Y-m-d', strtotime($tanggal_pembelian)),
            'id_supplier' => $id_supplier,
            'cara_beli' => $cara_beli,
            'id_pengambil' => $id_pengambil,
            'nama_pengantar' => $nama_pengantar,
            'total_beli' => number_unformat($total_beli),
            'ppn_header' => number_unformat($ppn_header),
            'nominal_ppn_header' => number_unformat($nominal_ppn_header),
            'total_diskon' => number_unformat($diskon),
            // 'potongan' => number_unformat($potongan),
            'total_bayar' => number_unformat($total_bayar),
            'jenis_pembayaran' => $jenis_pembayaran,
            'bank' => $non_tunai,
            'status_pembelian' => 1
        );
        $insert_pembelian = $this->master_m->input_data($data_pembelian,'pembelian');
        if($insert_pembelian){
            foreach ($barang as $barang_pembelian) {
                $data_barang_pembelian = array(
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $barang_pembelian['id_barang'],
                    'qty' => $barang_pembelian['qty'],
                    'harga' => number_unformat($barang_pembelian['harga']),
                    'ppn_item' => $barang_pembelian['ppn_item'],
                    'nominal_ppn_item' => number_unformat($barang_pembelian['nominal_ppn_item']),
                    'rafaksi' => $barang_pembelian['rafaksi'],
                    'qty_rafaksi' => $barang_pembelian['qty_rafaksi'],
                    'nominal_qty_rafaksi' => $barang_pembelian['nominal_qty_rafaksi'],
                    // 'harga_rafaksi' => number_unformat($barang_pembelian['harga_rafaksi']),
                    'nominal_harga_rafaksi' => number_unformat($barang_pembelian['nominal_harga_rafaksi']),
                    'sub_total_harga' => number_unformat($barang_pembelian['sub_total_harga'])
                );
                // var_dump($barang_pembelian['qty']);
                // exit;
                // $pembelian_data = array(
                //     'qty' => $barang_pembelian['qty']
                // );
                // $pembelian_where = array(
                //     'id_barang' => $barang_pembelian['id_barang']
                // );
                $this->master_m->input_data($data_barang_pembelian,'detail_pembelian');
                if ($barang_pembelian['nominal_qty_rafaksi'] == 0||!$barang_pembelian['nominal_qty_rafaksi']) {
                    $this->pembelian_m->update_stok($barang_pembelian['qty'], 'barang', $barang_pembelian['id_barang']);
                    /*$monitoring = $this->monitoring_m->tampil_data(date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['id_barang'])->row();
                    if ($monitoring) {
                        $this->monitoring_m->update_monitoring($monitoring->id, date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['qty'], 'monitoring');
                    } else {
                        $monitoring_periode = $this->monitoring_m->tampil_data_pembelian(date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['id_barang'])->row();
                        if ($monitoring_periode) {
                            $this->monitoring_m->update_monitoring_pembelian($monitoring_periode->id, $monitoring_periode->id_barang, $barang_pembelian['qty'], 'monitoring');
                            $data = array(
                                'periode' => date('Y-m'),
                                'id_barang' => $monitoring_periode->id_barang,
                                'stok_periode' => $monitoring->stok_periode+$barang_pembelian['qty']
                            );
                            $this->monitoring_m->input_data($data, 'monitoring');
                        } else {
                            $data = array(
                                'periode' => date('Y-m'),
                                'id_barang' => $barang_pembelian['id_barang'],
                                'stok_periode' => $barang_pembelian['qty']
                            );
                            $this->monitoring_m->input_data($data, 'monitoring');
                        }
                    }*/
                } else {
                    $this->pembelian_m->update_stok($barang_pembelian['nominal_qty_rafaksi'], 'barang', $barang_pembelian['id_barang']);
                    /*$monitoring = $this->monitoring_m->tampil_data(date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['id_barang'])->row();
                    if ($monitoring) {
                        $this->monitoring_m->update_monitoring($monitoring->id, date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['nominal_qty_rafaksi'], 'monitoring');
                    } else {
                        $monitoring_periode = $this->monitoring_m->tampil_data_pembelian(date('Y-m', strtotime($tanggal_pembelian)), $barang_pembelian['id_barang'])->row();
                        if ($monitoring_periode) {
                            $this->monitoring_m->update_monitoring_pembelian($monitoring_periode->id, $monitoring_periode->id_barang, $barang_pembelian['nominal_qty_rafaksi'], 'monitoring');
                            $data = array(
                                'periode' => date('Y-m'),
                                'id_barang' => $monitoring_periode->id_barang,
                                'stok_periode' => $monitoring->stok_periode+$barang_pembelian['nominal_qty_rafaksi']
                            );
                            $this->monitoring_m->input_data($data, 'monitoring');
                        } else {
                            $data = array(
                                'periode' => date('Y-m'),
                                'id_barang' => $barang_pembelian['id_barang'],
                                'stok_periode' => $barang_pembelian['nominal_qty_rafaksi']
                            );
                            $this->monitoring_m->input_data($data, 'monitoring');
                        }
                    }*/
                }
                //update monitoring
                //select monitoring by id_barang and tanggal input strtotime
                //ketemu : update monitoring + transaksi yang id_barang yang ketemu and periode tahun bulan transaksi input dan setelahnya
                //else : ambil monitoring id_barang tersebut and periode maks sebelum tgl transaksi (select monitoring yang periode sebelum transaksi order by periode limit 1)
                //
            }
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $this->message->set_success('pembelian','Data Berhasil Disimpan !');
            redirect('pembelian/index');
        } else {
            $this->db->trans_rollback();
            redirect('pembelian/tambah');
        }
    }

    public function batal($id_pembelian){
        // $id_pembelian = array('id_pembelian' => $id_pembelian);
        $batal_pembelian = $this->pembelian_m->batal_data($id_pembelian)->result();
        // var_dump($batal_pembelian);
        foreach ($batal_pembelian as $batal) {
            // $batal_data = array(
            //     $batal->id_barang,
            //     $batal->qty
            // );
            if($batal->nominal_qty_rafaksi == 0||!$batal->nominal_qty_rafaksi){
                $this->pembelian_m->batal_pembelian($batal->qty, 'barang', $batal->id_barang);
            }else{
                $this->pembelian_m->batal_pembelian($batal->nominal_qty_rafaksi, 'barang', $batal->id_barang);
            }
        }
        $this->pembelian_m->batal_status($id_pembelian);
        $this->message->set_success('pembelian','Data Berhasil Dibatalkan !');
        redirect('pembelian/index');
    }

    public function detail($id_pembelian){
        $data['data_pembelian'] = $this->pembelian_m->data_pembelian($id_pembelian)->result();
        $data['data_detail_pembelian'] = $this->pembelian_m->data_detail_pembelian($id_pembelian)->result();
        $this->load->view('pembelian/detail', $data);
    }

    // public function nota($id_pembelian){
    //     ob_start();
    //         $data['data_pembelian'] = $this->laporan_m->laporan_pembelian($id_pembelian)->result();
    //         $data['data_detail_pembelian'] = $this->laporan_m->laporan_pembelian_detail($id_pembelian)->result();
    //         $this->load->view('pembelian/nota', $data);
    //         $html = ob_get_contents();
    //     ob_end_clean();
    //     require_once('./public/plugin/pdf/html2pdf-4.4.0/html2pdf.class.php');
    //     $pdf = new HTML2PDF('P','A4','en');
    //     $pdf->WriteHTML($html);
    //     $pdf->Output('Detail Pembelian.pdf', 'FD');
    // }

    public function nota($id_pembelian){
        $data['data_pembelian'] = $this->laporan_m->laporan_pembelian($id_pembelian)->result();
        $data['data_detail_pembelian'] = $this->laporan_m->laporan_pembelian_detail($id_pembelian)->result();
        $customPaper = array(0,0,850,550);
        $this->pdf->setPaper($customPaper);
        $this->pdf->filename = 'Nota Pembelian.pdf';
        $this->pdf->load_view('pembelian/nota', $data);
    }

}