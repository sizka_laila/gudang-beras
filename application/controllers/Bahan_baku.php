<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahan_baku extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('bahan_baku_m');
        $this->load->model('barang_m');
        $this->load->library('message');
        $this->load->helper('date');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['bahan_baku'] = $this->bahan_baku_m->tampil_data()->result();
        $this->load->view('master/bahan_baku/index', $data);
	}

	public function tambah(){
		$data['barang_produksi'] = $this->barang_m->barang_produksi()->result();
        $data['bahan_baku'] = $this->barang_m->bahan_baku()->result();
        $data['kode'] = $this->bahan_baku_m->buat_kode();
        $this->load->view('master/bahan_baku/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_bahan_baku = $this->input->post('id_bahan_baku');
        $barang_produksi = $this->input->post('barang_produksi');
        $harga_produksi = $this->input->post('harga_produksi');
        // $id_detail_bahan_baku = $this->input->post('id_detail_bahan_baku');
        // $id_barang_bahan_baku = $this->input->post('id_barang_bahan_baku');
        // $qty = $this->input->post('qty');
        $resep = $this->input->post('resep');
        $data_resep = array(
            'id_bahan_baku' => $id_bahan_baku,
            'tanggal_terdaftar' => date('Y-m-d'),
            'barang_produksi' => $barang_produksi,
            'harga_produksi' => number_unformat($harga_produksi)
        );
        $insert_bahan_baku = $this->master_m->input_data($data_resep,'bahan_baku');
        if($insert_bahan_baku){
            foreach ($resep as $bahan_baku) {
                $data_bahan_baku = array(
                    'id_bahan_baku' => $id_bahan_baku,
                    'id_barang_bahan_baku' => $bahan_baku['id_barang'],
                    'qty' => $bahan_baku['qty']
                );

                $this->master_m->input_data($data_bahan_baku,'detail_bahan_baku');
            }
            $this->message->set_success('detail_bahan_baku','Data Berhasil Disimpan !');
        }
        redirect('bahan_baku/index');
    }

    public function cari_barang($id_barang){
        $data = $this->barang_m->cari_data($id_barang)->row();
        echo json_encode($data);
    }

    // public function addresep($id_barang){
    //     $data = $this->barang_m->cari_data($id_barang)->row();
    //     echo json_encode($data);
    // }

	public function edit($id_bahan_baku){
        $data['barang_produksi'] = $this->barang_m->barang_produksi()->result();
        $data['bahan_baku'] = $this->barang_m->bahan_baku()->result();
        $data['data_bahan_baku'] = $this->bahan_baku_m->edit_data_bahan_baku($id_bahan_baku)->result();
        $data['data_detail_bahan_baku'] = $this->bahan_baku_m->edit_data_detail_bahan_baku($id_bahan_baku)->result();
        $this->load->view('master/bahan_baku/ubah_form', $data);
	}

    function ubah_aksi(){
        $id_bahan_baku = $this->input->post('id_bahan_baku');
        $barang_produksi = $this->input->post('barang_produksi');
        $harga_produksi = $this->input->post('harga_produksi');
        // $id_detail_bahan_baku = $this->input->post('id_detail_bahan_baku');
        // $id_barang_bahan_baku = $this->input->post('id_barang_bahan_baku');
        // $qty = $this->input->post('qty');
        $resep = $this->input->post('resep');

        $data_resep = array(
            'tanggal_terdaftar' => date('Y-m-d'),
            'barang_produksi' => $barang_produksi,
            'harga_produksi' => number_unformat($harga_produksi)
        );
        $where_data_resep = array(
            'id_bahan_baku' => $id_bahan_baku
        );
        $insert_bahan_baku = $this->master_m->update_data($where_data_resep,$data_resep,'bahan_baku');
        if($insert_bahan_baku > 0){
            $delete = $this->master_m->hapus_data($where_data_resep,'detail_bahan_baku');
            if($delete > 0){
                foreach ($resep as $bahan_baku) {
                    $data_bahan_baku = array(
                        'id_bahan_baku' => $id_bahan_baku,
                        'id_barang_bahan_baku' => $bahan_baku['id_barang_bahan_baku'],
                        'qty' => $bahan_baku['qty']
                    );
                    // var_dump($data_bahan_baku);
                    $input_data_bahan_baku = $this->master_m->input_data($data_bahan_baku,'detail_bahan_baku');
                    // print_r($bahan_baku['qty']);
                }
                if($input_data_bahan_baku > 0){
                    $this->message->set_success('bahan_baku','Data Berhasil Disimpan !');
                }
            }
        }
        redirect('bahan_baku/index');
    }

    function hapus($id_bahan_baku){
        $where = array('id_bahan_baku' => $id_bahan_baku);
        $hapus_bahan_baku = $this->master_m->hapus_data($where,'bahan_baku');
        if($hapus_bahan_baku > 0){
           $this->master_m->hapus_data($where,'detail_bahan_baku');
           $this->message->set_error('bahan_baku','Data Berhasil Dihapus !');
        }
        redirect('bahan_baku/index');
    }
}
