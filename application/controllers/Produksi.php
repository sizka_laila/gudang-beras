<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('message');
        $this->load->model('produksi_m');
        $this->load->model('barang_m');
        $this->load->model('bahan_baku_m');
        $this->load->model('master_m');
        $this->load->model('monitoring_m');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=2 OR $this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $data['data_produksi'] = $this->produksi_m->tampil_data()->result();
        $this->load->view('produksi/index', $data);
	}

	public function tambah(){
        $data['barang_produksi'] = $this->bahan_baku_m->tampil_data()->result();
        $data['kode'] = $this->produksi_m->buat_kode();
        $this->load->view('produksi/tambah_form', $data);
	}

    public function cari_barang($id_barang){
        $data['data_bahan_baku'] = $this->produksi_m->data_bahan_baku($id_barang)->row();
        $data['data_detail_bahan_baku'] = $this->produksi_m->data_detail_bahan_baku($id_barang)->result();
        // print_r($data);
        echo json_encode($data);
    }

    public function tambah_aksi(){
        $id_produksi = $this->input->post('id_produksi');
        $id_user = $this->input->post('id_user');
        $id_bahan_baku = $this->input->post('id_bahan_baku');
        $id_barang_produksi = $this->input->post('id_barang_produksi');
        $jumlah = $this->input->post('jumlah');
        $harga_produksi = $this->input->post('harga_produksi');
        $total_produksi = $this->input->post('total_produksi');
        $data_produksi = array(
            'id_produksi' => $id_produksi,
            'id_user' => $id_user,
            'tanggal_produksi' => date('Y-m-d'),
            'id_bahan_baku' => $id_bahan_baku,
            'id_barang_produksi' => $id_barang_produksi,
            'jumlah' => $jumlah,
            'harga_produksi' => number_unformat($harga_produksi),
            'total_produksi' => number_unformat($total_produksi),
            'status_produksi' => 1
        );
        $insert_produksi = $this->master_m->input_data($data_produksi,'produksi');
        $this->produksi_m->update_stok_barang_produksi($jumlah, 'barang', $id_barang_produksi);
        $produksi = $this->input->post('produksi');
        if($insert_produksi > 0){
            foreach ($produksi as $detail) {
                $data_detail = array(
                    'id_produksi' => $id_produksi,
                    'id_barang_bahan_baku' => $detail['id_barang_bahan_baku'],
                    'qty' => $detail['qty'],
                    'total' => $jumlah*$detail['qty']
                );
                $this->master_m->input_data($data_detail,'detail_produksi');
                $this->produksi_m->update_stok_bahan_baku($jumlah*$detail['qty'], 'barang', $detail['id_barang_bahan_baku']);
                $monitoring_bahan_baku = $this->monitoring_m->tampil_data(date('Y-m'), $id_barang_produksi)->row();
                $monitoring_barang_produksi = $this->monitoring_m->tampil_data(date('Y-m'), $detail['id_barang_bahan_baku'])->row();
                // print_r($monitoring_bahan_baku);
                // echo '<br>';
                // print_r($monitoring_barang_produksi);
                // exit();
                if ($monitoring_bahan_baku) {
                    $this->monitoring_m->update_monitoring_produksi_bahan_baku($monitoring_bahan_baku->id, $jumlah, 'monitoring');
                    if ($monitoring_barang_produksi) {
                        $this->monitoring_m->update_monitoring_produksi_barang_produksi($monitoring_barang_produksi->id, $detail['qty'], 'monitoring');
                    } else {
                        $data_detail = array(
                            'periode' => date('Y-m'),
                            'id_barang' => $detail['id_barang_bahan_baku'],
                            'stok_periode' => $detail['qty']
                        );
                        $this->monitoring_m->input_data($data_detail, 'monitoring');
                    }
                } else {                    
                    $data = array(
                        'periode' => date('Y-m'),
                        'id_barang' => $id_barang_produksi,
                        'stok_periode' => $jumlah
                    );
                    $this->monitoring_m->input_data($data, 'monitoring');
                    if ($monitoring_barang_produksi) {
                        $this->monitoring_m->update_monitoring_produksi_barang_produksi($monitoring_barang_produksi->id, $detail['qty'], 'monitoring');
                    } else {
                        $data_detail = array(
                            'periode' => date('Y-m'),
                            'id_barang' => $detail['id_barang_bahan_baku'],
                            'stok_periode' => $detail['qty']
                        );
                        $this->monitoring_m->input_data($data_detail, 'monitoring');
                    }
                }
            }
        }
        $this->message->set_success('produksi','Data Berhasil Disimpan !');
        redirect('produksi/index');
    }

    public function batal($id_produksi){
        $batal_produksi = $this->produksi_m->batal_data($id_produksi);
        if($batal_produksi->num_rows() > 0){
            $hasil = $batal_produksi->row();
            $this->produksi_m->batal_stok_barang_produksi($hasil->jumlah, 'barang', $hasil->id_barang_produksi);
            foreach ($batal_produksi->result() as $batal) {
                $this->produksi_m->batal_stok_bahan_baku($batal->total, 'barang', $batal->id_barang_bahan_baku);
            }
            $this->produksi_m->batal_status($id_produksi);
        }
        $this->message->set_success('produksi','Data Berhasil Dibatalkan !');
        redirect('produksi/index');
    }

    public function detail($id_produksi){
        $data['data_produksi'] = $this->produksi_m->data_produksi($id_produksi)->result();
        $data['data_detail_produksi'] = $this->produksi_m->data_detail_produksi($id_produksi)->result();
        $this->load->view('produksi/detail', $data);
    }

}