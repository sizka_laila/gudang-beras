<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('supplier_m');
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['supplier'] = $this->supplier_m->tampil_data()->result();
        $this->load->view('master/supplier/index', $data);
	}

	public function tambah(){
        $data['kode'] = $this->supplier_m->buat_kode();
        $this->load->view('master/supplier/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_supplier = $this->input->post('id_supplier');
        $nama_supplier = $this->input->post('nama_supplier');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'id_supplier' => $id_supplier,
            'nama_supplier' => $nama_supplier,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email,
            'tanggal_terdaftar' => date('Y-m-d')
        );
        $this->master_m->input_data($data,'supplier');
        $this->message->set_success('supplier','Data Berhasil Ditambahkan !');
        redirect('supplier/index');
    }

    function edit($id_supplier){
        $where = array('id_supplier' => $id_supplier);
        $data['supplier'] = $this->master_m->edit_data($where,'supplier')->result();
        $this->load->view('master/supplier/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_supplier = $this->input->post('id_supplier');
        $nama_supplier = $this->input->post('nama_supplier');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'nama_supplier' => $nama_supplier,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email
        );
        $where = array(
            'id_supplier' => $id_supplier
        );
        $this->master_m->update_data($where,$data,'supplier');
        $this->message->set_success('supplier','Data Berhasil Diubah !');
        redirect('supplier/index');
    }

    function hapus($id_supplier){
        $where = array('id_supplier' => $id_supplier);
        $this->master_m->hapus_data($where,'supplier');
        $this->message->set_success('supplier','Data Berhasil Dihapus !');
        redirect('supplier/index');
    }

}
