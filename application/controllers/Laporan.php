<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('laporan_m');
        $this->load->model('customer_m');
        $this->load->model('supplier_m');
        $this->load->model('user_m');
        $this->load->helper('date');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

    public function pembelian(){
        $data['supplier'] = $this->supplier_m->tampil_data()->result();
        $data['user'] = $this->user_m->tampil_data()->result();
        $data['tampil_pembelian'] = $this->laporan_m->tampil_pembelian()->result();
        $this->load->view('laporan/pembelian/index', $data);
    }

    public function filter_pembelian(){
        $id_supplier = $this->input->post('id_supplier');
        $id_user = $this->input->post('id_user');
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $id_rekap = $this->input->post('id_rekap');
        $status_pembelian = $this->input->post('status_pembelian');
        $this->db->select('pembelian.tanggal_pembelian, pembelian.id_pembelian, pembelian.id_user, user.username, pegawai.nama_pegawai, pembelian.id_transaksi, pembelian.id_supplier, supplier.nama_supplier, detail_pembelian.id_barang, barang.nama_barang, detail_pembelian.qty, detail_pembelian.ppn_item, detail_pembelian.nominal_ppn_item, detail_pembelian.rafaksi, detail_pembelian.qty_rafaksi, detail_pembelian.nominal_qty_rafaksi, detail_pembelian.nominal_harga_rafaksi, detail_pembelian.sub_total_harga, pembelian.total_beli, pembelian.status_pembelian')
                ->from('pembelian')
                ->join('detail_pembelian','detail_pembelian.id_pembelian = pembelian.id_pembelian')
                ->join('user','user.id_user = pembelian.id_user')
                ->join('pegawai','user.id_pegawai = pegawai.id_pegawai')
                ->join('supplier','pembelian.id_supplier = supplier.id_supplier')
                ->join('barang','barang.id_barang = detail_pembelian.id_barang')
                ->order_by('pembelian.tanggal_pembelian', 'DESC');
        if($id_supplier){
            $this->db->where('pembelian.id_supplier', $id_supplier);
        }
        if($id_user){
            $this->db->where('pembelian.id_user', $id_user);
        }
        if($status_pembelian){
            $this->db->where('pembelian.status_pembelian', $status_pembelian);
        }
        if($periode_awal){
            $this->db->where('pembelian.tanggal_pembelian >= ', date('Y-m-d', strtotime($periode_awal)));
        }
        if($periode_akhir){
            $this->db->where('pembelian.tanggal_pembelian <= ', date('Y-m-d', strtotime($periode_akhir)));
        }
        $query = $this->db->get();
        $data['supplier'] = $this->supplier_m->tampil_data()->result();
        $data['user'] = $this->user_m->tampil_data()->result();
        $data['tampil_pembelian'] = $query->result();
        $data['id_supplier'] = $this->input->post('id_supplier');
        $data['id_user'] = $this->input->post('id_user');
        $data['status_pembelian'] = $this->input->post('status_pembelian');
        $data['periode_awal'] = $this->input->post('periode_awal');
        $data['periode_akhir'] = $this->input->post('periode_akhir');
        // $data['id_rekap'] = $this->input->post('id_rekap');
        $this->load->view('laporan/pembelian/index', $data);
    }

    public function cetak_pembelian(){
        $id_supplier = $this->input->get('id_supplier');
        $id_user = $this->input->get('id_user');
        $periode_awal = $this->input->get('periode_awal');
        $periode_akhir = $this->input->get('periode_akhir');
        $status_pembelian = $this->input->get('status_pembelian');
        $id_rekap = $this->input->get('id_rekap');
        // if($id_rekap==1){
        //     echo "1";
        // }elseif ($id_rekap==2) {
        //     echo "2";
        // }elseif ($id_rekap==3) {
        //     echo "3";
        // }else{
        //     echo "lain";
        // }
        switch ($id_rekap) {
            case '1':
                $this->db->select('pembelian.*, pegawai.nama_pegawai, user.username, supplier.nama_supplier, detail_pembelian.id_barang, barang.nama_barang, satuan.nama_satuan, detail_pembelian.qty, detail_pembelian.harga, detail_pembelian.ppn_item, detail_pembelian.nominal_ppn_item, detail_pembelian.rafaksi, detail_pembelian.nominal_qty_rafaksi, detail_pembelian.sub_total_harga')
                ->from('pembelian')
                ->join('detail_pembelian','detail_pembelian.id_pembelian = pembelian.id_pembelian')
                ->join('user','user.id_user = pembelian.id_user')
                ->join('pegawai','user.id_pegawai = pegawai.id_pegawai')
                ->join('supplier','pembelian.id_supplier = supplier.id_supplier')
                ->join('barang','barang.id_barang = detail_pembelian.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('pembelian.tanggal_pembelian', 'DESC');
                if($id_supplier){
                    $this->db->where('pembelian.id_supplier', $id_supplier);
                }
                if($id_user){
                    $this->db->where('pembelian.id_user', $id_user);
                }
                if($status_pembelian){
                    $this->db->where('pembelian.status_pembelian', $status_pembelian);
                }
                if($periode_awal){
                    $this->db->where('pembelian.tanggal_pembelian >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('pembelian.tanggal_pembelian <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_pembelian'] = $query->result();
                $this->load->view('laporan/pembelian/laporan_pembelian_nota', $data);
                // echo "nota";
                break;
            case '2':
                $this->db->select('pembelian.tanggal_pembelian, pembelian.status_pembelian, pembelian.id_user, pegawai.nama_pegawai, user.username, barang.id_barang, barang.nama_barang, barang.id_satuan, satuan.nama_satuan, SUM(detail_pembelian.qty) as qty, detail_pembelian.harga, SUM(detail_pembelian.nominal_ppn_item) as nominal_ppn_item, SUM(detail_pembelian.rafaksi) rafaksi, SUM(detail_pembelian.nominal_qty_rafaksi) nominal_qty_rafaksi, SUM(detail_pembelian.sub_total_harga) sub_total_harga')
                ->from('detail_pembelian')
                ->join('pembelian','detail_pembelian.id_pembelian = pembelian.id_pembelian')
                ->join('user','user.id_user = pembelian.id_user')
                ->join('pegawai','user.id_pegawai = pegawai.id_pegawai')
                ->join('supplier','pembelian.id_supplier = supplier.id_supplier')
                ->join('barang','barang.id_barang = detail_pembelian.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('pembelian.tanggal_pembelian', 'DESC')
                // ->order_by('detail_pembelian.id_barang', 'DESC')
                ->group_by(array('pembelian.tanggal_pembelian', 'detail_pembelian.id_barang'));
                if($id_supplier){
                    $this->db->where('pembelian.id_supplier', $id_supplier);
                }
                if($id_user){
                    $this->db->where('pembelian.id_user', $id_user);
                }
                if($status_pembelian){
                    $this->db->where('pembelian.status_pembelian', $status_pembelian);
                }
                if($periode_awal){
                    $this->db->where('pembelian.tanggal_pembelian >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('pembelian.tanggal_pembelian <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_pembelian'] = $query->result();
                $this->load->view('laporan/pembelian/laporan_pembelian_harian', $data);
                // echo "harian";
                break;
            case '3':
                $this->db->select('LEFT(pembelian.tanggal_pembelian, 7) as bulan, pembelian.status_pembelian, pembelian.id_user, pegawai.nama_pegawai, user.username, barang.id_barang, barang.nama_barang, barang.id_satuan, satuan.nama_satuan, SUM(detail_pembelian.qty) as qty, detail_pembelian.harga, SUM(detail_pembelian.nominal_ppn_item) as nominal_ppn_item, SUM(detail_pembelian.rafaksi) rafaksi, SUM(detail_pembelian.nominal_qty_rafaksi) nominal_qty_rafaksi, SUM(detail_pembelian.sub_total_harga) sub_total_harga')
                ->from('detail_pembelian')
                ->join('pembelian','detail_pembelian.id_pembelian = pembelian.id_pembelian')
                ->join('user','user.id_user = pembelian.id_user')
                ->join('pegawai','user.id_pegawai = pegawai.id_pegawai')
                ->join('supplier','pembelian.id_supplier = supplier.id_supplier')
                ->join('barang','barang.id_barang = detail_pembelian.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('pembelian.tanggal_pembelian', 'DESC')
                // ->order_by('detail_pembelian.id_barang', 'DESC')
                ->group_by(array('LEFT(pembelian.tanggal_pembelian, 7)', 'detail_pembelian.id_barang'));
                if($id_supplier){
                    $this->db->where('pembelian.id_supplier', $id_supplier);
                }
                if($id_user){
                    $this->db->where('pembelian.id_user', $id_user);
                }
                if($status_pembelian){
                    $this->db->where('pembelian.status_pembelian', $status_pembelian);
                }
                if($periode_awal){
                    $this->db->where('pembelian.tanggal_pembelian >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('pembelian.tanggal_pembelian <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_pembelian'] = $query->result();
                $this->load->view('laporan/pembelian/laporan_pembelian_bulanan', $data);
                // echo "bulanan";
                break;
            default:
                # code...
                break;
        }
    }

    public function detail_pembelian($id_pembelian){
        ob_start();
            $data['data_pembelian'] = $this->laporan_m->laporan_pembelian($id_pembelian)->result();
            $data['data_detail_pembelian'] = $this->laporan_m->laporan_pembelian_detail($id_pembelian)->result();
            $this->load->view('laporan/pembelian/detail_pembelian', $data);
            $html = ob_get_contents();
        ob_end_clean();
        require_once('./public/plugin/pdf/html2pdf-4.4.0/html2pdf.class.php');
        $pdf = new HTML2PDF('L','F4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('Detail Pembelian.pdf', 'D');
    }

    public function pembelian_excel($id_pembelian){
        $data['data_pembelian'] = $this->laporan_m->laporan_pembelian($id_pembelian)->result();
        $data['data_detail_pembelian'] = $this->laporan_m->laporan_pembelian_detail($id_pembelian)->result();
        $this->load->view('laporan/pembelian/pembelian_excel', $data);
    }

	public function penjualan(){
        $data['customer'] = $this->customer_m->tampil_data()->result();
        $data['user'] = $this->user_m->tampil_data()->result();
		$data['tampil_penjualan'] = $this->laporan_m->tampil_penjualan()->result();
        $this->load->view('laporan/penjualan/index', $data);
	}

    public function filter_penjualan(){
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $id_customer = $this->input->post('id_customer');
        $id_user = $this->input->post('id_user');
        $status_penjualan = $this->input->post('status_penjualan');
        $id_rekap = $this->input->post('id_rekap');
        $this->db->select('penjualan.tanggal_penjualan, penjualan.id_penjualan, penjualan.id_customer, customer.nama_customer, penjualan.id_user, user.username, pegawai.nama_pegawai, detail_penjualan.id_barang, barang.nama_barang, detail_penjualan.qty, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga, penjualan.total_jual, penjualan.status_penjualan')
                ->from('penjualan')
                ->join('detail_penjualan','detail_penjualan.id_penjualan = penjualan.id_penjualan')
                ->join('customer','penjualan.id_customer = customer.id_customer')
                ->join('user','user.id_user = penjualan.id_user')
                ->join('pegawai','pegawai.id_pegawai = user.id_pegawai')
                ->join('barang','barang.id_barang = detail_penjualan.id_barang')
                ->order_by('penjualan.tanggal_penjualan', 'DESC');
        if($id_customer){
            $this->db->where('penjualan.id_customer', $id_customer);
        }
        if($id_user){
            $this->db->where('penjualan.id_user', $id_user);
        }
        if($status_penjualan){
            $this->db->where('penjualan.status_penjualan', $status_penjualan);
        }
        if($periode_awal){
            $this->db->where('penjualan.tanggal_penjualan >= ', date('Y-m-d', strtotime($periode_awal)));
        }
        if($periode_akhir){
            $this->db->where('penjualan.tanggal_penjualan <= ', date('Y-m-d', strtotime($periode_akhir)));
        }
        $query = $this->db->get();
        $data['periode_awal'] = $this->input->post('periode_awal');
        $data['periode_akhir'] = $this->input->post('periode_akhir');
        $data['id_customer'] = $this->input->post('id_customer');
        $data['id_user'] = $this->input->post('id_user');
        $data['status_penjualan'] = $this->input->post('status_penjualan');
        $data['customer'] = $this->customer_m->tampil_data()->result();
        $data['user'] = $this->user_m->tampil_data()->result();
        $data['tampil_penjualan'] = $query->result();
        $this->load->view('laporan/penjualan/index', $data);
    }

    public function cetak_penjualan(){
        $periode_awal = $this->input->get('periode_awal');
        $periode_akhir = $this->input->get('periode_akhir');
        $id_customer = $this->input->get('id_customer');
        $id_user = $this->input->get('id_user');
        $status_penjualan = $this->input->get('status_penjualan');
        $id_rekap = $this->input->get('id_rekap');
        switch ($id_rekap) {
            case '1':
                $this->db->select('penjualan.*, user.username, pegawai.nama_pegawai, customer.nama_customer, detail_penjualan.id_barang, barang.nama_barang, satuan.nama_satuan, detail_penjualan.qty, detail_penjualan.harga, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga')
                ->from('penjualan')
                ->join('detail_penjualan','detail_penjualan.id_penjualan = penjualan.id_penjualan')
                ->join('customer','penjualan.id_customer = customer.id_customer')
                ->join('user','user.id_user = penjualan.id_user')
                ->join('pegawai','pegawai.id_pegawai = user.id_pegawai')
                ->join('barang','barang.id_barang = detail_penjualan.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('penjualan.tanggal_penjualan', 'DESC');
                if($id_customer){
                    $this->db->where('penjualan.id_customer', $id_customer);
                }
                if($id_user){
                    $this->db->where('penjualan.id_user', $id_user);
                }
                if($status_penjualan){
                    $this->db->where('penjualan.status_penjualan', $status_penjualan);
                }
                if($periode_awal){
                    $this->db->where('penjualan.tanggal_penjualan >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('penjualan.tanggal_penjualan <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_penjualan'] = $query->result();
                $this->load->view('laporan/penjualan/laporan_penjualan_nota', $data);
                break;
            case '2':
                $this->db->select('penjualan.tanggal_penjualan, penjualan.status_penjualan, penjualan.id_user, user.username, pegawai.nama_pegawai, barang.id_barang, barang.nama_barang, barang.id_satuan, satuan.nama_satuan, SUM(detail_penjualan.qty) as qty, detail_penjualan.harga, SUM(detail_penjualan.nominal_ppn_item) as nominal_ppn_item, SUM(detail_penjualan.sub_total_harga) sub_total_harga')
                ->from('detail_penjualan')
                ->join('penjualan','detail_penjualan.id_penjualan = penjualan.id_penjualan')
                ->join('customer','penjualan.id_customer = customer.id_customer')
                ->join('user','user.id_user = penjualan.id_user')
                ->join('pegawai','pegawai.id_pegawai = user.id_pegawai')
                ->join('barang','barang.id_barang = detail_penjualan.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('penjualan.tanggal_penjualan', 'DESC')
                ->group_by(array('penjualan.tanggal_penjualan', 'detail_penjualan.id_barang'));
                if($id_customer){
                    $this->db->where('penjualan.id_customer', $id_customer);
                }
                if($id_user){
                    $this->db->where('penjualan.id_user', $id_user);
                }
                if($status_penjualan){
                    $this->db->where('penjualan.status_penjualan', $status_penjualan);
                }
                if($periode_awal){
                    $this->db->where('penjualan.tanggal_penjualan >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('penjualan.tanggal_penjualan <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_penjualan'] = $query->result();
                $this->load->view('laporan/penjualan/laporan_penjualan_harian', $data);
                break;
            case '3':
                $this->db->select('LEFT(penjualan.tanggal_penjualan, 7) as bulan, penjualan.status_penjualan, penjualan.id_user, user.username, pegawai.nama_pegawai, barang.id_barang, barang.nama_barang, barang.id_satuan, satuan.nama_satuan, SUM(detail_penjualan.qty) as qty, detail_penjualan.harga, SUM(detail_penjualan.nominal_ppn_item) as nominal_ppn_item, SUM(detail_penjualan.sub_total_harga) sub_total_harga')
                ->from('detail_penjualan')
                ->join('penjualan','detail_penjualan.id_penjualan = penjualan.id_penjualan')
                ->join('customer','penjualan.id_customer = customer.id_customer')
                ->join('user','user.id_user = penjualan.id_user')
                ->join('pegawai','pegawai.id_pegawai = user.id_pegawai')
                ->join('barang','barang.id_barang = detail_penjualan.id_barang')
                ->join('satuan','satuan.id_satuan = barang.id_satuan')
                ->order_by('penjualan.tanggal_penjualan', 'DESC')
                ->group_by(array('LEFT(penjualan.tanggal_penjualan, 7)', 'detail_penjualan.id_barang'));
                if($id_customer){
                    $this->db->where('penjualan.id_customer', $id_customer);
                }
                if($id_user){
                    $this->db->where('penjualan.id_user', $id_user);
                }
                if($status_penjualan){
                    $this->db->where('penjualan.status_penjualan', $status_penjualan);
                }
                if($periode_awal){
                    $this->db->where('penjualan.tanggal_penjualan >= ', date('Y-m-d', strtotime($periode_awal)));
                }
                if($periode_akhir){
                    $this->db->where('penjualan.tanggal_penjualan <= ', date('Y-m-d', strtotime($periode_akhir)));
                }
                $query = $this->db->get();
                $data['tampil_penjualan'] = $query->result();
                $this->load->view('laporan/penjualan/laporan_penjualan_bulanan', $data);
                break;
            default:
                # code...
                break;
        }
    }

    public function detail_penjualan($id_penjualan){
        ob_start();
            $data['data_penjualan'] = $this->laporan_m->laporan_penjualan($id_penjualan)->result();
            $data['data_detail_penjualan'] = $this->laporan_m->laporan_penjualan_detail($id_penjualan)->result();
            $this->load->view('laporan/penjualan/detail_penjualan', $data);
            $html = ob_get_contents();
        ob_end_clean();
        require_once('./public/plugin/pdf/html2pdf-4.4.0/html2pdf.class.php');
        $pdf = new HTML2PDF('L','F4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('Detail Penjualan.pdf', 'D');
    }

    public function penjualan_excel($id_penjualan){
        $data['data_penjualan'] = $this->laporan_m->laporan_penjualan($id_penjualan)->result();
        $data['data_detail_penjualan'] = $this->laporan_m->laporan_penjualan_detail($id_penjualan)->result();
        $this->load->view('laporan/penjualan/detail_penjualan', $data);
    }

    public function semua_penjualan(){
        $data['tampil_penjualan'] = $this->laporan_m->tampil_penjualan()->result();
        $this->load->view('laporan/penjualan/penjualan', $data);
    }

}