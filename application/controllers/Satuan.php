<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('satuan_m');
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['satuan'] = $this->satuan_m->tampil_data()->result();
        $this->load->view('master/satuan/index', $data);
	}

	public function tambah(){
        $this->load->view('master/satuan/tambah_form');
	}

    function tambah_aksi(){
        $id_satuan = $this->input->post('id_satuan');
        $nama_satuan = $this->input->post('nama_satuan');
        $alias = $this->input->post('alias');
        $data = array(
            'id_satuan' => $id_satuan,
            'nama_satuan' => $nama_satuan,
            'alias' => $alias
        );
        $this->master_m->input_data($data,'satuan');
        $this->message->set_success('satuan','Data Berhasil Ditambahkan !');
        redirect('satuan/index');
    }

    function edit($id_satuan){
        $where = array('id_satuan' => $id_satuan);
        $data['satuan'] = $this->master_m->edit_data($where,'satuan')->result();
        $this->load->view('master/satuan/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_satuan = $this->input->post('id_satuan');
        $nama_satuan = $this->input->post('nama_satuan');
        $alias = $this->input->post('alias');
        $data = array(
            'nama_satuan' => $nama_satuan,
            'alias' => $alias
        );
        $where = array(
            'id_satuan' => $id_satuan
        );
        $this->master_m->update_data($where,$data,'satuan');
        $this->message->set_success('satuan','Data Berhasil Diubah !');
        redirect('satuan/index');
    }

    function hapus($id_satuan){
        $where = array('id_satuan' => $id_satuan);
        $this->master_m->hapus_data($where,'satuan');
        $this->message->set_success('satuan','Data Berhasil Dihapus !');
        redirect('satuan/index');
    }

}
