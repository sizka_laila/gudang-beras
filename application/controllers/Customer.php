<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('customer_m');
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['customer'] = $this->customer_m->tampil_data()->result();
        $this->load->view('master/customer/index', $data);
	}

	public function tambah(){
        $data['kode'] = $this->customer_m->buat_kode();
        $this->load->view('master/customer/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_customer = $this->input->post('id_customer');
        $nama_customer = $this->input->post('nama_customer');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'id_customer' => $id_customer,
            'nama_customer' => $nama_customer,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email,
            'tanggal_terdaftar' => date('Y-m-d')
        );
        $this->master_m->input_data($data,'customer');
        $this->message->set_success('customer','Data Berhasil Ditambahkan !');
        redirect('customer/index');
    }

    function edit($id_customer){
        $where = array('id_customer' => $id_customer);
        $data['customer'] = $this->master_m->edit_data($where,'customer')->result();
        $this->load->view('master/customer/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_customer = $this->input->post('id_customer');
        $nama_customer = $this->input->post('nama_customer');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'nama_customer' => $nama_customer,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email
        );
        $where = array(
            'id_customer' => $id_customer
        );
        $this->master_m->update_data($where,$data,'customer');
        $this->message->set_success('customer','Data Berhasil Diubah !');
        redirect('customer/index');
    }

    function hapus($id_customer){
        $where = array('id_customer' => $id_customer);
        $this->master_m->hapus_data($where,'customer');
        $this->message->set_success('customer','Data Berhasil Dihapus !');
        redirect('customer/index');
    }

}
