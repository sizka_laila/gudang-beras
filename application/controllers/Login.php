<?php

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('login_m');
		$this->load->model('monitoring_m');
		$this->load->model('barang_m');
	}

	function index(){
		$this->load->view('login');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
		);
		$cek = $this->login_m->cek_login($where)->num_rows();
		if($cek > 0){
			$data_user = $this->login_m->cek_login($where)->result();
			foreach($data_user as $data){
				$data_session = array(
					'id_user' => $data->id_user,
					'id_pegawai' => $data->id_pegawai,
					'nama_pegawai' => $data->nama_pegawai,
					'tanggal_terdaftar' => $data->tanggal_terdaftar,
					'hak_akses' => $data->hak_akses,
					'username' => $data->username,
					'status' => $data->status,
					'status_login' => "login"
				);
				$this->session->set_userdata($data_session);
				if($data->status==0){
					$this->session->set_flashdata('gagal', '<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> Akun Anda masih non-aktif ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
					redirect(base_url('login'));
				}else{
					/*$monitoring = $this->monitoring_m->tampil_periode(date('Y-m'))->result();
					if(!$monitoring){
						$monitoring_periode = $this->monitoring_m->tampil_max_periode()->result();
						// print_r($monitoring_periode);
						if(!$monitoring_periode){
							$monitoring_barang = $this->barang_m->tampil_data()->result();
							$data_barang = array();
							foreach ($monitoring_barang as $data) {
								$data_barang[] = array(
									'periode' => date('Y-m'),
									'id_barang' => $data->id_barang,
									'stok_periode' => $data->qty
								);
							}
							$insert_barang = $this->db->insert_batch('monitoring', $data_barang);
						}else{
							$data_periode = array();
							foreach ($monitoring_periode as $data) {
								$data_periode[] = array(
									'periode' => date('Y-m'),
									'id_barang' => $data->id_barang,
									'stok_periode' => $data->stok_periode
								);
								// $insert_periode = $this->input_periode(date('Y-m'), $data->id_barang, $data->stok_periode, 'monitoring');
							}
							$insert_periode = $this->db->insert_batch('monitoring', $data_periode);
							// print_r($data_periode);
						}
					}*/
					// print_r($monitoring);
					redirect(base_url('home'));
				}
			}
		}else{
			$this->session->set_flashdata('gagal', '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Username dan password salah ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url('login'));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

}