<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('barang_m');
        $this->load->model('satuan_m');
        $this->load->model('monitoring_m');
        $this->load->library('message');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['barang'] = $this->barang_m->tampil_data()->result();
        $this->load->view('master/barang/index', $data);
	}

	public function tambah(){
        $data['satuan'] = $this->satuan_m->tampil_data()->result();
        $data['kode'] = $this->barang_m->buat_kode();
        $this->load->view('master/barang/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_barang = $this->input->post('id_barang');
        $nama_barang = $this->input->post('nama_barang');
        $qty = $this->input->post('qty');
        $id_satuan = $this->input->post('id_satuan');
        $jenis_barang = $this->input->post('jenis_barang');
        $harga = $this->input->post('harga');
        $ppn = $this->input->post('ppn');
        $nominal_ppn = $this->input->post('nominal_ppn');
        $harga_jual = $this->input->post('harga_jual');
        $ppn_jual = $this->input->post('ppn_jual');
        $nominal_ppn_jual = $this->input->post('nominal_ppn_jual');
        if($ppn<>1){
            $ppn = 0;
        }
        if($ppn_jual<>1){
            $ppn_jual = 0;
        }
        $data = array(
            'id_barang' => $id_barang,
            'nama_barang' => $nama_barang,
            'qty' => $qty,
            'id_satuan' => $id_satuan,
            'jenis_barang' => $jenis_barang,
            'harga' => number_unformat($harga),
            'ppn' => number_unformat($ppn),
            'nominal_ppn' => number_unformat($nominal_ppn),
            'harga_jual' => number_unformat($harga_jual),
            'ppn_jual' => number_unformat($ppn_jual),
            'nominal_ppn_jual' => number_unformat($nominal_ppn_jual)
        );
        $this->master_m->input_data($data,'barang');
        $monitoring_barang = $this->monitoring_m->tampil_data(date('Y-m'), $id_barang)->result();
        if(!$monitoring_barang){
            $data = array(
                'periode' => date('Y-m'),
                'id_barang' => $id_barang,
                'stok_periode' => $qty
            );
            $insert_barang = $this->monitoring_m->input_data($data, 'monitoring');
        }        
        $this->message->set_success('barang','Data Berhasil Ditambahkan !');
        redirect('barang/index');
    }

    function edit($id_barang){
        $where = array('id_barang' => $id_barang);
        $data['barang'] = $this->master_m->edit_data($where,'barang')->result();
        $this->load->view('master/barang/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_barang = $this->input->post('id_barang');
        $nama_barang = $this->input->post('nama_barang');
        $qty = $this->input->post('qty');
        $id_satuan = $this->input->post('id_satuan');
        $jenis_barang = $this->input->post('jenis_barang');
        $harga = $this->input->post('harga');
        $ppn = $this->input->post('ppn');
        $nominal_ppn = $this->input->post('nominal_ppn');
        $harga_jual = $this->input->post('harga_jual');
        $ppn_jual = $this->input->post('ppn_jual');
        $nominal_ppn_jual = $this->input->post('nominal_ppn_jual');
        if($ppn<>1){
            $ppn = 0;
        }
        if($ppn_jual<>1){
            $ppn_jual = 0;
        }
        $data = array(
            'nama_barang' => $nama_barang,
            'qty' => $qty,
            'id_satuan' => $id_satuan,
            'jenis_barang' => $jenis_barang,
            'harga' => number_unformat($harga),
            'ppn' => number_unformat($ppn),
            'nominal_ppn' => number_unformat($nominal_ppn),
            'harga_jual' => number_unformat($harga_jual),
            'ppn_jual' => number_unformat($ppn_jual),
            'nominal_ppn_jual' => number_unformat($nominal_ppn_jual)
        );
        $where = array(
            'id_barang' => $id_barang
        );
        $this->master_m->update_data($where,$data,'barang');
        $this->message->set_success('barang','Data Berhasil Diubah !');
        redirect('barang/index');
    }

    function hapus($id_barang){
        $where = array('id_barang' => $id_barang);
        $this->master_m->hapus_data($where,'barang');
        $this->message->set_success('barang','Data Berhasil Dihapus !');
        redirect('barang/index');
    }

}
