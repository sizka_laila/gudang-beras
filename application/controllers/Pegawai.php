<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('pegawai_m');
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['pegawai'] = $this->pegawai_m->tampil_data()->result();
        $this->load->view('master/pegawai/index', $data);
	}

	public function tambah(){
        $data['kode'] = $this->pegawai_m->buat_kode();
        $this->load->view('master/pegawai/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_pegawai = $this->input->post('id_pegawai');
        $nama_pegawai = $this->input->post('nama_pegawai');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'id_pegawai' => $id_pegawai,
            'nama_pegawai' => $nama_pegawai,
            'jenis_kelamin' => $jenis_kelamin,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email,
            'tanggal_terdaftar' => date('Y-m-d')
        );
        $this->master_m->input_data($data,'pegawai');
        $this->message->set_success('pegawai','Data Berhasil Ditambahkan !');
        redirect('pegawai/index');
    }

    function edit($id_pegawai){
        $where = array('id_pegawai' => $id_pegawai);
        $data['pegawai'] = $this->master_m->edit_data($where,'pegawai')->result();
        $this->load->view('master/pegawai/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_pegawai = $this->input->post('id_pegawai');
        $nama_pegawai = $this->input->post('nama_pegawai');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $alamat = $this->input->post('alamat');
        $telephone = $this->input->post('telephone');
        $email = $this->input->post('email');
        $data = array(
            'nama_pegawai' => $nama_pegawai,
            'jenis_kelamin' => $jenis_kelamin,
            'alamat' => $alamat,
            'telephone' => $telephone,
            'email' => $email
        );
        $where = array(
            'id_pegawai' => $id_pegawai
        );
        $this->master_m->update_data($where,$data,'pegawai');
        $this->message->set_success('pegawai','Data Berhasil Diubah !');
        redirect('pegawai/index');
    }

    function hapus($id_pegawai){
        $where = array('id_pegawai' => $id_pegawai);
        $this->master_m->hapus_data($where,'pegawai');
        $this->message->set_success('pegawai','Data Berhasil Dihapus !');
        redirect('pegawai/index');
    }

}
