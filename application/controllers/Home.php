<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('dashboard_m');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            redirect(base_url("login"));
        }
    }

	public function index(){
		foreach ($this->dashboard_m->grafik_penjualan()->result() as $key => $value) {
			$data['periode_jual'][] = $value->periode_jual;
			$data['total_jual'][] = $value->total_jual;
		}
		foreach ($this->dashboard_m->grafik_pembelian()->result() as $key => $value) {
			$data['periode_beli'][] = $value->periode_beli;
			$data['total_beli'][] = $value->total_beli;
		}
		$data['jumlah_pembelian'] = $this->dashboard_m->jumlah_pembelian()->result();
		$data['jumlah_penjualan'] = $this->dashboard_m->jumlah_penjualan()->result();
		$data['jumlah_customer'] = $this->dashboard_m->jumlah_customer()->result();
		$data['jumlah_supplier'] = $this->dashboard_m->jumlah_supplier()->result();
		// $data['grafik_penjualan'] = $this->dashboard_m->grafik_penjualan();
		$data['data_pembelian'] = $this->dashboard_m->data_pembelian()->result();
		$data['data_penjualan'] = $this->dashboard_m->data_penjualan()->result();
        $this->load->view('dashboard', $data);
	}
}
