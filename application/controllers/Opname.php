<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opname extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('message');
        $this->load->model('opname_m');
        $this->load->model('monitoring_m');
        $this->load->model('barang_m');
        $this->load->model('master_m');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $data['data_opname'] = $this->opname_m->tampil_data()->result();
        $this->load->view('stok/opname/index', $data);
	}

    public function tambah(){
        $data['barang'] = $this->barang_m->tampil_data()->result();
        $this->load->view('stok/opname/tambah_form', $data);
    }

    public function cari_barang($id_barang){
        $data = $this->barang_m->cari_data($id_barang)->row();
        echo json_encode($data);
    }

    public function tambah_aksi(){
        $this->db->trans_start();
        $post = $this->input->post();
        $record_stock_opname = array();
        foreach ($post['opname'] as $key => $opname) {
            if ($opname['stok_opname']) {
                $opname['tanggal'] = date('Y-m-d');
                $opname['id_barang'] = $key;
                $record_stock_opname[] = $opname;
                $this->opname_m->update_stok($opname['stok_opname'], 'barang', $key);
                /*$monitoring = $this->monitoring_m->tampil_data(date('Y-m'), $key)->row();
                if ($monitoring) {
                    $this->monitoring_m->update_data($monitoring->id, $opname['stok_opname'], 'monitoring');
                } else {
                    $data = array(
                        'periode' => date('Y-m'),
                        'id_barang' => $key,
                        'stok_periode' => $opname['stok_opname']
                    );
                    $this->monitoring_m->input_data($data, 'monitoring');
                }*/
            }
        }
        if ($record_stock_opname) {
            $input = $this->opname_m->input_batch($record_stock_opname, 'opname');
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $this->message->set_success('opname','Data Berhasil Ditambahkan !');
            redirect('opname/index');
        } else {
            $this->db->trans_rollback();
            redirect('opname/tambah');
        }

        // $data_opname = $this->opname_m->tampil_data(date('Y-m-d'), $key)->row();
        //     }
        // }
        // if ($data_opname) {
        //     $this->opname_m->update_data($data_opname->id_barang, $opname['stok_opname'], 'opname');
        // } else {
        //     if ($record_stock_opname) {
        //         $input = $this->opname_m->input_batch($record_stock_opname, 'opname');
        //     }
        // }
        // print_r($record_stock_opname);
        // exit;
        // $data = array();
        // $index = 0;
        // if($stok_opname==0){
        //     $stok_opname = 0;
        //     $selisih = 0;
        // }
        // foreach ($id_barang as $id) {
        //     array_push($data, array(
        //         'tanggal' => date('Y-m-d'),
        //         'id_barang' => $id,
        //         'stok_akhir' => $stok_akhir[$index],
        //         'stok_opname' => $stok_opname[$index],
        //         'selisih' => $selisih[$index],
        //         'keterangan' => $keterangan[$index],
        //         'keterangan_lain' => $keterangan_lain[$index]
        //     ));
        //     $index++;
        //     // $input = $this->opname_m->input_batch($data, 'opname');
        //     // if($input){
        //     //     $update = $this->opname_m->update_stok($stok_opname[$index], 'barang', $id);
        //     // }
        //     // var_dump($stok_opname[$index]);
        // }
        // var_dump($data);
        // // var_dump($stok_opname);
        // $a = $this->opname_m->update_stok(array_column($data, 'stok_opname'), 'barang', $id);
        // var_dump($a);

        // $input = $this->opname_m->input_batch($data, 'opname');
        // if($update){
        //     $update = $this->opname_m->update_stok(array_column($data, 'stok_opname'), 'barang', $id);
        //     // $update = $this->opname_m->update_batch($data, 'barang', 'id_barang');
        //     if($update){
        //         $this->message->set_success('opname','Data Berhasil Ditambahkan !');
        //         redirect('opname/index');
        //     }
        // }else{
        //     redirect('opname/tambah');
        // }
    }

    public function cari_data(){
        $periode = $this->input->post('periode');
        $id_barang = $this->input->post('id_barang');
        // print_r(date('Y-m'), strtotime($periode));
        // exit();
        if($id_barang){
            if($periode){
                $data['data_opname'] = $this->opname_m->tampil_filter(date('Y-m', strtotime($periode)), $id_barang)->result();
            }else{
                $data['data_opname'] = $this->opname_m->tampil_filter(date('Y-m'), $id_barang)->result();
            }
        }else{
            $data['data_opname'] = $this->opname_m->tampil_data()->result();
        }
        $this->load->view('stok/opname/index', $data);
    }

}