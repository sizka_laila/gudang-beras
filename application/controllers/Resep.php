<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resep extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $this->load->view('dashboard');
	}

}