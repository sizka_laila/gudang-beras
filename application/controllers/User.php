<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master_m');
        $this->load->model('user_m');
        $this->load->model('pegawai_m');
        $this->load->library('message');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
		$data['user'] = $this->user_m->tampil_data()->result();
        $this->load->view('master/user/index', $data);
	}

	public function tambah(){
        $data['pegawai'] = $this->pegawai_m->tampil_data()->result();
        $this->load->view('master/user/tambah_form', $data);
	}

    function tambah_aksi(){
        $id_pegawai = $this->input->post('id_pegawai');
        $hak_akses = $this->input->post('hak_akses');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $status = $this->input->post('status');
        if($status<1){
            $status = 0;
        }
        $data = array(
            'id_pegawai' => $id_pegawai,
            'hak_akses' => $hak_akses,
            'username' => $username,
            'password' => md5($password),
            'status' => $status,
            'tanggal_terdaftar' => date('Y-m-d')
        );
        $this->master_m->input_data($data,'user');
        $this->message->set_success('user','Data Berhasil Ditambahkan !');
        redirect('user/index');
    }

    function edit($id_user){
        $where = array('id_user' => $id_user);
        $data['user'] = $this->master_m->edit_data($where,'user')->result();
        $this->load->view('master/user/ubah_form',$data);
    }

    function ubah_aksi(){
        $id_user = $this->input->post('id_user');
        $id_pegawai = $this->input->post('id_pegawai');
        $hak_akses = $this->input->post('hak_akses');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $status = $this->input->post('status');        
        if($status<1){
            $status = 0;
        }
        $data = array(
            'id_pegawai' => $id_pegawai,
            'hak_akses' => $hak_akses,
            'username' => $username,
            'password' => md5($password),
            'status' => $status
        );
        $where = array(
            'id_user' => $id_user
        );
        $this->master_m->update_data($where,$data,'user');
        $this->message->set_success('user','Data Berhasil Diubah !');
        redirect('user/index');
    }

    function hapus($id_user){
        $where = array('id_user' => $id_user);
        $this->master_m->hapus_data($where,'user');
        $this->message->set_success('user','Data Berhasil Dihapus !');
        redirect('user/index');
    }

    function status($id_user){
        $where = array('id_user' => $id_user);
        $data['user'] = $this->master_m->edit_data($where,'user')->result();
        $this->load->view('master/user/ubah_form',$data);
    }

    function ubah_status(){
        $id_user = $_POST['idUser'];
        $status = $_POST['idStatus'];
        $data = array(
            'status' => $status
        );
        $this->user_m->ubah_status('user', $data, 'id_user = "'.$id_user.'"');
    }

    function profil($id_user){
        $id = $id_user;
        $data['user'] = $this->user_m->profil($id)->result();
        $this->load->view('master/user/profil',$data);
    }

    function ubah_profil(){
        $id_user = $this->input->post('id_user');
        // $id_pegawai = $this->input->post('id_pegawai');
        // $hak_akses = $this->input->post('hak_akses');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // $status = $this->input->post('status');
        // if($status<1){
        //     $status = 0;
        // }
        $data = array(
            // 'id_pegawai' => $id_pegawai,
            // 'hak_akses' => $hak_akses,
            'username' => $username,
            'password' => md5($password),
            // 'status' => $status
        );
        $where = array(
            'id_user' => $id_user
        );
        $this->master_m->update_data($where,$data,'user');
        $this->message->set_success('user','Data Berhasil Diubah !');
        // redirect('user/index');
        $data['user'] = $this->user_m->profil($id_user)->result();
        $this->load->view('master/user/profil',$data);
    }

}
