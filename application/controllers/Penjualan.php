<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('penjualan_m');
        $this->load->model('customer_m');
        $this->load->model('pegawai_m');
        $this->load->model('barang_m');
        $this->load->model('master_m');
        $this->load->model('monitoring_m');
        $this->load->model('laporan_m');
        $this->load->library('message');
        $this->load->library('pdf');
        $this->load->helper('date');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=2 OR $this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $data['penjualan'] = $this->penjualan_m->tampil_data()->result();
        $this->load->view('penjualan/index', $data);
	}

	public function tambah(){
        $data['customer'] = $this->customer_m->tampil_data()->result();
        $data['pegawai'] = $this->pegawai_m->tampil_data()->result();
        $data['barang'] = $this->barang_m->tampil_data()->result();
        $data['kode'] = $this->penjualan_m->buat_kode();
        $this->load->view('penjualan/tambah_form', $data);
	}

    public function cari_barang($id_barang){
        $data = $this->barang_m->cari_data($id_barang)->row();
        echo json_encode($data);
    }

    public function tambah_aksi(){
        $this->db->trans_start();
        $id_penjualan = $this->input->post('id_penjualan');
        $tanggal_penjualan = $this->input->post('tanggal_penjualan');
        $id_user = $this->input->post('id_user');
        $id_customer = $this->input->post('id_customer');
        $cara_jual = $this->input->post('cara_jual');
        $id_pengantar = $this->input->post('id_pengantar');
        $nama_pengambil = $this->input->post('nama_pengambil');
        $total_jual = $this->input->post('total_jual');
        $ppn_header = $this->input->post('ppn_header');
        $nominal_ppn_header = $this->input->post('nominal_ppn_header');
        $diskon = $this->input->post('diskon');
        $potongan = $this->input->post('potongan');
        $total_bayar = $this->input->post('total_bayar');
        $bayar = $this->input->post('bayar');
        $kembali = $this->input->post('kembali');
        $jenis_pembayaran = $this->input->post('jenis_pembayaran');
        $non_tunai = $this->input->post('non_tunai');
        $barang = $this->input->post('barang');;
        $data_penjualan = array(
            'id_penjualan' => $id_penjualan,
            'id_user' => $id_user,
            'tanggal_penjualan' => date('Y-m-d', strtotime($tanggal_penjualan)),
            'id_customer' => $id_customer,
            'cara_jual' => $cara_jual,
            'id_pengantar' => $id_pengantar,
            'nama_pengambil' => $nama_pengambil,
            'total_jual' => number_unformat($total_jual),
            'ppn_header' => number_unformat($ppn_header),
            'nominal_ppn_header' => number_unformat($nominal_ppn_header),
            'total_diskon' => number_unformat($diskon),
            'potongan' => number_unformat($potongan),
            'total_bayar' => number_unformat($total_bayar),
            'bayar' => number_unformat($bayar),
            'kembali' => number_unformat($kembali),
            'jenis_pembayaran' => $jenis_pembayaran,
            'bank' => $non_tunai,
            'status_penjualan' => 1
        );
        $insert_penjualan = $this->master_m->input_data($data_penjualan,'penjualan');
        if($insert_penjualan){
            foreach ($barang as $barang_penjualan) {
                $data_barang_penjualan = array(
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $barang_penjualan['id_barang'],
                    'qty' => $barang_penjualan['qty'],
                    'harga' => number_unformat($barang_penjualan['harga']),
                    'ppn_item' => $barang_penjualan['ppn_item'],
                    'nominal_ppn_item' => number_unformat($barang_penjualan['nominal_ppn_item']),
                    'sub_total_harga' => number_unformat($barang_penjualan['sub_total_harga'])
                );
                // $pembelian_data = array(
                //     'qty' => $barang_pembelian['qty']
                // );
                // $pembelian_where = array(
                //     'id_barang' => $barang_pembelian['id_barang']
                // );
                $this->master_m->input_data($data_barang_penjualan,'detail_penjualan');
                $this->penjualan_m->update_stok($barang_penjualan['qty'], 'barang', $barang_penjualan['id_barang']);
                /*$monitoring = $this->monitoring_m->tampil_data(date('Y-m'), $barang_penjualan['id_barang'])->row();
                if ($monitoring) {
                    $this->monitoring_m->update_monitoring_penjualan($monitoring->id, $barang_penjualan['qty'], 'monitoring');
                } else {
                    $data = array(
                        'periode' => date('Y-m'),
                        'id_barang' => $barang_penjualan['id_barang'],
                        'stok_periode' => $barang_penjualan['qty']
                    );
                    $this->monitoring_m->input_data($data, 'monitoring');
                }*/
            }
            // $this->message->set_success('penjualan','Data Berhasil Disimpan !');
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $this->message->set_success('penjualan','Data Berhasil Disimpan !');
            redirect('penjualan/index?print='.$id_penjualan);
        } else {
            $this->db->trans_rollback();
            redirect('penjualan/tambah');
        }
    }

    public function batal($id_penjualan){
        $batal_penjualan = $this->penjualan_m->batal_data($id_penjualan)->result();
        foreach ($batal_penjualan as $batal) {
            $this->penjualan_m->batal_penjualan($batal->qty, 'barang', $batal->id_barang);
        }
        $this->penjualan_m->batal_status($id_penjualan);
        $this->message->set_success('penjualan','Data Berhasil Dibatalkan !');
        redirect('penjualan/index');
    }

    public function detail($id_penjualan){
        $data['data_penjualan'] = $this->penjualan_m->data_penjualan($id_penjualan)->result();
        $data['data_detail_penjualan'] = $this->penjualan_m->data_detail_penjualan($id_penjualan)->result();
        $this->load->view('penjualan/detail', $data);
    }

    // public function nota($id_penjualan){
    //     ob_start();
    //         $data['data_penjualan'] = $this->laporan_m->laporan_penjualan($id_penjualan)->result();
    //         $data['data_detail_penjualan'] = $this->laporan_m->laporan_penjualan_detail($id_penjualan)->result();
    //         $this->load->view('penjualan/nota', $data);
    //         $html = ob_get_contents();
    //     ob_end_clean();
    //     require_once('./public/plugin/pdf/html2pdf-4.4.0/html2pdf.class.php');
    //     $pdf = new HTML2PDF('P','A4','en');
    //     $pdf->WriteHTML($html);
    //     $pdf->Output('Detail Penjualan.pdf', 'D');
    // }

    public function nota($id_penjualan){
        $data['data_penjualan'] = $this->laporan_m->laporan_penjualan($id_penjualan)->result();
        $data['data_detail_penjualan'] = $this->laporan_m->laporan_penjualan_detail($id_penjualan)->result();
        $data['sum_laporan_penjualan_detail'] = $this->laporan_m->sum_laporan_penjualan_detail($id_penjualan)->result();
        $customPaper = array(0,0,850,550);
        $this->pdf->setPaper($customPaper);
        $this->pdf->filename = 'Nota Penjualan.pdf';
        $this->pdf->load_view('penjualan/nota', $data);
    }

    public function surat_jalan($id_penjualan){
        $data['data_penjualan'] = $this->laporan_m->laporan_penjualan($id_penjualan)->result();
        $data['data_detail_penjualan'] = $this->laporan_m->laporan_penjualan_detail($id_penjualan)->result();
        $data['sum_laporan_penjualan_detail'] = $this->laporan_m->sum_laporan_penjualan_detail($id_penjualan)->result();
        $customPaper = array(0,0,850,550);
        $this->pdf->setPaper($customPaper);
        $this->pdf->filename = 'Surat Jalan.pdf';
        $this->pdf->load_view('penjualan/surat_jalan', $data);
    }
}