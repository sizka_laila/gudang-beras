<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('monitoring_m');
        $this->load->helper('slh');
        if($this->session->userdata('status_login') != "login"){
            if($this->session->userdata('hak_akses')!=3){
                redirect(base_url("login"));
            }
        }
    }

	public function index(){
        $data['monitoring'] = $this->monitoring_m->tampil()->result();
        $this->load->view('stok/monitoring/index', $data);
	}

    public function cari_data(){
        $start_date = date('Y-m-d', strtotime($this->input->post('start_date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end_date')));
        $data['monitoring'] = $this->monitoring_m->tampil($start_date, $end_date)->result();
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $this->load->view('stok/monitoring/index', $data);
    }

}