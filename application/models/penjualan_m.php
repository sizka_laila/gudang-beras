<?php

class Penjualan_m extends CI_Model{

	function tampil_data(){
		$query =  "SELECT penjualan.tanggal_penjualan, penjualan.id_penjualan, penjualan.id_customer, customer.nama_customer, penjualan.total_bayar, penjualan.status_penjualan FROM penjualan JOIN customer ON penjualan.id_customer = customer.id_customer WHERE penjualan.status_penjualan = 1 ORDER BY penjualan.tanggal_penjualan DESC, penjualan.created_at DESC";
		return $this->db->query($query);
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(penjualan.id_penjualan,3) as id_penjualan', FALSE);
		$this->db->where('LEFT(penjualan.id_penjualan,'.strlen('PJL0'.$tgl).')', 'PJL0'.$tgl);
		$this->db->order_by('id_penjualan','DESC');
		$this->db->limit(1);
		$query = $this->db->get('penjualan');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_penjualan) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "PJL"."0".$tgl.$batas;
		return $kodetampil;
	}

	function update_stok($penjualan_data, $table, $penjualan_where){
		$query = "UPDATE ".$table." SET qty = qty - ".$penjualan_data." WHERE id_barang = '".$penjualan_where."'";
		return $this->db->query($query);
	}

	function batal_data($id_penjualan){
		$query = "SELECT penjualan.id_penjualan, detail_penjualan.id_barang, detail_penjualan.qty FROM penjualan JOIN detail_penjualan ON penjualan.id_penjualan = detail_penjualan.id_penjualan WHERE penjualan.id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

	function batal_penjualan($qty, $table, $id){
		$query = "UPDATE ".$table." SET qty = qty + ".$qty." WHERE id_barang = '".$id."'";
		return $this->db->query($query);
	}

	function batal_status($id_penjualan){
		$query = "UPDATE penjualan SET status_penjualan = 2 WHERE id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

	function data_penjualan($id_penjualan){
		$query = "SELECT penjualan.id_penjualan, penjualan.tanggal_penjualan, penjualan.id_customer, customer.nama_customer, penjualan.cara_jual, penjualan.id_pengantar, pegawai.nama_pegawai, penjualan.nama_pengambil, penjualan.total_jual, penjualan.ppn_header, penjualan.nominal_ppn_header, penjualan.total_diskon, penjualan.potongan, penjualan.total_bayar, penjualan.bayar, penjualan.kembali, penjualan.jenis_pembayaran, penjualan.bank FROM penjualan JOIN customer ON customer.id_customer = penjualan.id_customer LEFT JOIN pegawai ON pegawai.id_pegawai = penjualan.id_pengantar WHERE penjualan.id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

	function data_detail_penjualan($id_penjualan){
		$query = "SELECT detail_penjualan.id_barang, barang.nama_barang, satuan.nama_satuan, detail_penjualan.qty, detail_penjualan.harga, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga FROM detail_penjualan JOIN barang ON barang.id_barang = detail_penjualan.id_barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_penjualan.id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

}