<?php 
 
class Dashboard_m extends CI_Model{

	function jumlah_pembelian(){
		$query = "SELECT count(id_pembelian) AS jumlah_pembelian FROM pembelian WHERE status_pembelian = 1";
		return $this->db->query($query);
	}

	function jumlah_penjualan(){
		$query = "SELECT count(id_penjualan) AS jumlah_penjualan FROM penjualan WHERE status_penjualan = 1";
		return $this->db->query($query);
	}

	function jumlah_customer(){
		$query = "SELECT count(id_customer) AS jumlah_customer FROM customer";
		return $this->db->query($query);
	}

	function jumlah_supplier(){
		$query = "SELECT count(id_supplier) AS jumlah_supplier FROM supplier";
		return $this->db->query($query);
	}

	function grafik_penjualan(){
		$query = "SELECT LEFT(penjualan.tanggal_penjualan, 7) periode_jual, SUM(penjualan.total_jual) total_jual FROM penjualan WHERE LEFT(penjualan.tanggal_penjualan, 4) = '".date('Y')."' AND penjualan.status_penjualan = 1 GROUP BY LEFT(penjualan.tanggal_penjualan, 7)";
		return $this->db->query($query);
	}

	function grafik_pembelian(){
		$query = "SELECT LEFT(pembelian.tanggal_pembelian, 7) periode_beli, SUM(pembelian.total_beli) total_beli FROM pembelian WHERE LEFT(pembelian.tanggal_pembelian, 4) = '".date('Y')."' AND pembelian.status_pembelian = 1 GROUP BY LEFT(pembelian.tanggal_pembelian, 7)";
		return $this->db->query($query);
	}

	function data_pembelian(){
		$query =  "SELECT pembelian.id_supplier, supplier.nama_supplier, SUM(pembelian.total_bayar) bayar, COUNT(pembelian.id_pembelian) jumlah FROM pembelian JOIN supplier ON pembelian.id_supplier = supplier.id_supplier WHERE pembelian.status_pembelian = 1 GROUP BY pembelian.id_supplier LIMIT 10";
		return $this->db->query($query);
	}

	function data_penjualan(){
		$query =  "SELECT penjualan.id_customer, customer.nama_customer, SUM(penjualan.total_bayar) bayar, COUNT(penjualan.id_penjualan) jumlah FROM penjualan JOIN customer ON penjualan.id_customer = customer.id_customer WHERE penjualan.status_penjualan = 1 GROUP BY penjualan.id_customer LIMIT 10";
		return $this->db->query($query);
	}

}