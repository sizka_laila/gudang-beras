<?php

class Customer_m extends CI_Model{

	function tampil_data(){
		return $this->db->order_by('nama_customer', 'ASC')->get('customer');
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(customer.id_customer,3) as id_customer', FALSE);
		$this->db->where('LEFT(customer.id_customer,'.strlen('CTM0'.$tgl).')', 'CTM0'.$tgl);
		$this->db->order_by('id_customer','DESC');
		$this->db->limit(1);
		$query = $this->db->get('customer');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_customer) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "CTM"."0".$tgl.$batas;
		return $kodetampil;
	}

}