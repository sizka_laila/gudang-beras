<?php

class Bahan_baku_m extends CI_Model{

	function tampil_data(){
		//return $this->db->get('bahan_baku');
		$query = "SELECT bahan_baku.id_bahan_baku, bahan_baku.tanggal_terdaftar, bahan_baku.barang_produksi, barang.nama_barang, bahan_baku.harga_produksi FROM bahan_baku JOIN barang ON barang.id_barang = bahan_baku.barang_produksi ORDER BY bahan_baku.created_at DESC";
		return  $this->db->query($query);
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(bahan_baku.id_bahan_baku,3) as id_bahan_baku', FALSE);
		$this->db->where('LEFT(bahan_baku.id_bahan_baku,'.strlen('RSP0'.$tgl).')', 'RSP0'.$tgl);
		$this->db->order_by('id_bahan_baku','DESC');
		$this->db->limit(1);
		$query = $this->db->get('bahan_baku');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_bahan_baku) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "RSP"."0".$tgl.$batas;
		return $kodetampil;
	}

	function edit_data_bahan_baku($id_bahan_baku){
		$query = "SELECT bahan_baku.id_bahan_baku, bahan_baku.tanggal_terdaftar, bahan_baku.barang_produksi, bahan_baku.harga_produksi, barang.nama_barang FROM bahan_baku JOIN barang ON barang.id_barang = bahan_baku.barang_produksi WHERE bahan_baku.id_bahan_baku = '".$id_bahan_baku."'";
		return  $this->db->query($query);
	}

	function edit_data_detail_bahan_baku($id_bahan_baku){
		$query = "SELECT detail_bahan_baku.id_bahan_baku, detail_bahan_baku.id_barang_bahan_baku, barang.nama_barang, satuan.nama_satuan, detail_bahan_baku.qty FROM detail_bahan_baku JOIN barang ON barang.id_barang = detail_bahan_baku.id_barang_bahan_baku JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_bahan_baku.id_bahan_baku = '".$id_bahan_baku."'";
		return  $this->db->query($query);
	}
}