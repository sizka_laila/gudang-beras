<?php

class Produksi_m extends CI_Model{

	function tampil_data(){
		$query = "SELECT produksi.id_produksi, produksi.tanggal_produksi, produksi.id_bahan_baku, produksi.id_barang_produksi, produksi.jumlah,produksi.harga_produksi, produksi.total_produksi, produksi.status_produksi, barang.nama_barang FROM produksi JOIN barang ON barang.id_barang = produksi.id_barang_produksi WHERE produksi.status_produksi = 1 ORDER BY produksi.tanggal_produksi DESC, produksi.created_at DESC";
		return $this->db->query($query);
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(produksi.id_produksi,3) as id_produksi', FALSE);
		$this->db->where('LEFT(produksi.id_produksi,'.strlen('PRD0'.$tgl).')', 'PRD0'.$tgl);
		$this->db->order_by('id_produksi','DESC');
		$this->db->limit(1);
		$query = $this->db->get('produksi');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_produksi) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "PRD"."0".$tgl.$batas;
		return $kodetampil;
	}

	function cari_data($id_barang){
		$query = "SELECT bahan_baku.id_bahan_baku, bahan_baku.tanggal_terdaftar, bahan_baku.barang_produksi, bahan_baku.harga_produksi, barang.id_barang, barang.nama_barang, barang.qty, barang.id_satuan, satuan.nama_satuan, barang.jenis_barang, barang.harga, barang.ppn, barang.nominal_ppn FROM bahan_baku JOIN barang ON barang.id_barang = bahan_baku.barang_produksi JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE barang.id_barang = '".$id_barang."'";
		return $this->db->query($query);
	}

	function data_bahan_baku($id_bahan_baku){
		$query = "SELECT bahan_baku.id_bahan_baku, bahan_baku.tanggal_terdaftar, bahan_baku.barang_produksi, bahan_baku.harga_produksi, barang.nama_barang, satuan.nama_satuan FROM bahan_baku JOIN barang ON barang.id_barang = bahan_baku.barang_produksi JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE bahan_baku.id_bahan_baku = '".$id_bahan_baku."'";
		return  $this->db->query($query);
	}

	function data_detail_bahan_baku($id_bahan_baku){
		$query = "SELECT detail_bahan_baku.id_bahan_baku, detail_bahan_baku.id_barang_bahan_baku, barang.nama_barang, satuan.nama_satuan, detail_bahan_baku.qty FROM detail_bahan_baku JOIN barang ON barang.id_barang = detail_bahan_baku.id_barang_bahan_baku JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_bahan_baku.id_bahan_baku = '".$id_bahan_baku."'";
		return  $this->db->query($query);
	}

	function update_stok_barang_produksi($produksi, $table, $produksi_where){
		$query = "UPDATE ".$table." SET qty = qty + ".$produksi." WHERE id_barang = '".$produksi_where."'";
		return $this->db->query($query);
	}

	function update_stok_bahan_baku($produksi, $table, $produksi_where){
		$query = "UPDATE ".$table." SET qty = qty - ".$produksi." WHERE id_barang = '".$produksi_where."'";
		return $this->db->query($query);
	}

	function batal_data($id_produksi){
		$query = "SELECT produksi.id_produksi, produksi.id_barang_produksi, produksi.jumlah, detail_produksi.id_barang_bahan_baku, detail_produksi.qty, detail_produksi.total FROM produksi JOIN detail_produksi ON detail_produksi.id_produksi = produksi.id_produksi WHERE produksi.id_produksi = '".$id_produksi."'";
		return $this->db->query($query);
	}

	function batal_status($id_produksi){
		$query = "UPDATE produksi SET status_produksi = 0 WHERE id_produksi = '".$id_produksi."'";
		return $this->db->query($query);
	}

	function batal_stok_barang_produksi($produksi, $table, $produksi_where){
		$query = "UPDATE ".$table." SET qty = qty - ".$produksi." WHERE id_barang = '".$produksi_where."'";
		return $this->db->query($query);
	}

	function batal_stok_bahan_baku($produksi, $table, $produksi_where){
		$query = "UPDATE ".$table." SET qty = qty + ".$produksi." WHERE id_barang = '".$produksi_where."'";
		return $this->db->query($query);
	}

	function data_produksi($id_produksi){
		$query = "SELECT produksi.id_produksi, produksi.tanggal_produksi, produksi.id_bahan_baku, produksi.id_barang_produksi, barang.nama_barang, satuan.nama_satuan, produksi.jumlah, produksi.harga_produksi, produksi.total_produksi FROM produksi JOIN barang ON barang.id_barang = produksi.id_barang_produksi JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE produksi.id_produksi = '".$id_produksi."'";
		return $this->db->query($query);
	}

	function data_detail_produksi($id_produksi){
		$query = "SELECT detail_produksi.id_produksi, detail_produksi.id_barang_bahan_baku, barang.nama_barang, satuan.nama_satuan, detail_produksi.qty, detail_produksi.total FROM detail_produksi JOIN barang ON barang.id_barang = detail_produksi.id_barang_bahan_baku JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_produksi.id_produksi = '".$id_produksi."'";
		return $this->db->query($query);
	}

}