<?php

class Pembelian_m extends CI_Model{

	function tampil_data(){
		$query =  "SELECT pembelian.tanggal_pembelian, pembelian.id_pembelian, pembelian.id_supplier, supplier.nama_supplier, pembelian.total_bayar, pembelian.status_pembelian FROM pembelian JOIN supplier ON pembelian.id_supplier = supplier.id_supplier WHERE pembelian.status_pembelian = 1 ORDER BY pembelian.tanggal_pembelian DESC, pembelian.created_at DESC";
		return $this->db->query($query);
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(pembelian.id_pembelian,3) as id_pembelian', FALSE);
		$this->db->where('LEFT(pembelian.id_pembelian,'.strlen('PBL0'.$tgl).')', 'PBL0'.$tgl);
		$this->db->order_by('id_pembelian','DESC');
		$this->db->limit(1);
		$query = $this->db->get('pembelian');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_pembelian) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "PBL"."0".$tgl.$batas;
		return $kodetampil;
	}

	function update_stok($pembelian_data, $table, $pembelian_where){
		// $barang = $this->db-query('SELECT * FROM barang WHERE id_barang = "'.$id_barang.'"')->result(); sk ku lali wkwkwkwk
		// foreach($barang as $data_barang){
		// 	$tot_qty = $data_barang
		// }
		$query = "UPDATE ".$table." SET qty = qty + ".$pembelian_data." WHERE id_barang = '".$pembelian_where."'";
		return $this->db->query($query);
	}

	function batal_data($id_pembelian){
		$query = "SELECT pembelian.id_pembelian, detail_pembelian.id_barang, detail_pembelian.qty, detail_pembelian.nominal_qty_rafaksi FROM pembelian JOIN detail_pembelian ON pembelian.id_pembelian = detail_pembelian.id_pembelian WHERE pembelian.id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

	function batal_pembelian($qty, $table, $id){
		$query = "UPDATE ".$table." SET qty = qty - ".$qty." WHERE id_barang = '".$id."'";
		return $this->db->query($query);
	}

	function batal_status($id_pembelian){
		$query = "UPDATE pembelian SET status_pembelian = 2 WHERE id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

	function data_pembelian($id_pembelian){
		$query = "SELECT pembelian.id_pembelian, pembelian.id_transaksi, pembelian.tanggal_pembelian, pembelian.id_supplier, supplier.nama_supplier, pembelian.cara_beli, pembelian.id_pengambil, pegawai.nama_pegawai, pembelian.nama_pengantar, pembelian.total_beli, pembelian.ppn_header, pembelian.nominal_ppn_header, pembelian.total_diskon, pembelian.potongan, pembelian.total_bayar, pembelian.jenis_pembayaran, pembelian.bank FROM pembelian JOIN supplier ON supplier.id_supplier = pembelian.id_supplier LEFT JOIN pegawai ON pegawai.id_pegawai = pembelian.id_pengambil WHERE pembelian.id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

	function data_detail_pembelian($id_pembelian){
		$query = "SELECT detail_pembelian.id_barang, barang.nama_barang, satuan.nama_satuan, detail_pembelian.qty, detail_pembelian.harga, detail_pembelian.ppn_item, detail_pembelian.nominal_ppn_item, detail_pembelian.rafaksi, detail_pembelian.qty_rafaksi, detail_pembelian.nominal_qty_rafaksi, detail_pembelian.harga_rafaksi, detail_pembelian.nominal_harga_rafaksi, detail_pembelian.sub_total_harga FROM detail_pembelian JOIN barang ON barang.id_barang = detail_pembelian.id_barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_pembelian.id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

}