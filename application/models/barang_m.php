<?php

class Barang_m extends CI_Model{

	function tampil_data(){
		$query = "SELECT barang.id_barang, barang.nama_barang, barang.qty, barang.id_satuan, satuan.nama_satuan, barang.jenis_barang, barang.harga, barang.ppn, barang.nominal_ppn, barang.harga_jual, barang.ppn_jual, barang.nominal_ppn_jual FROM barang JOIN satuan ON satuan.id_satuan = barang.id_satuan ORDER BY nama_barang ASC";
		return $this->db->query($query);
	}

	function cari_data($id_barang){
		$query = "SELECT barang.id_barang, barang.nama_barang, barang.qty, barang.id_satuan, satuan.nama_satuan, barang.jenis_barang, barang.harga, barang.ppn, barang.nominal_ppn, barang.harga_jual, barang.ppn_jual, barang.nominal_ppn_jual FROM barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE barang.id_barang ='$id_barang'";
		return $this->db->query($query);
	}

	function barang_produksi(){
		$query = "SELECT barang.id_barang, barang.nama_barang, barang.qty, barang.id_satuan, satuan.nama_satuan, barang.jenis_barang, barang.harga, barang.ppn, barang.nominal_ppn, barang.harga_jual, barang.ppn_jual, barang.nominal_ppn_jual FROM barang JOIN satuan ON satuan.id_satuan = barang.id_satuan";
		return $this->db->query($query);
	}

	function bahan_baku(){
		$query = "SELECT barang.id_barang, barang.nama_barang, barang.qty, barang.id_satuan, satuan.nama_satuan, barang.jenis_barang, barang.harga, barang.ppn, barang.nominal_ppn, barang.harga_jual, barang.ppn_jual, barang.nominal_ppn_jual FROM barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE barang.jenis_barang = 1";
		return $this->db->query($query);
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(barang.id_barang,3) as id_barang', FALSE);
		$this->db->where('LEFT(barang.id_barang,'.strlen('BRG0'.$tgl).')', 'BRG0'.$tgl);
		$this->db->order_by('id_barang','DESC');
		$this->db->limit(1);
		$query = $this->db->get('barang');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_barang) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "BRG"."0".$tgl.$batas;
		return $kodetampil;
	}

}