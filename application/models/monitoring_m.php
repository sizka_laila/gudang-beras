<?php

class Monitoring_m extends CI_Model{

	function tampil($start = null, $end = null) {
		if (!$start) {
			$start = date('Y-m-01');
		}
		if (!$end) {
			$end = date('Y-m-d');
		}
		$query = "SELECT
		    barang.id_barang,
		    barang.nama_barang,
		    COALESCE(monitoring.stok_periode, 0) AS stok_periode,
		    COALESCE(pembelian_awal.qty, 0) AS pembelian_awal,
		    COALESCE(produksi_awal.jumlah, 0) AS produksi_awal,
		    COALESCE(opname_awal.selisih, 0) AS opname_awal,
		    COALESCE(penjualan_awal.qty, 0) AS pejualan_awal,
		    COALESCE(produksi_bahan_awal.total,0) AS bahan_baku_awal,
		    COALESCE(pembelian_mutasi.qty, 0) AS pembelian,
		    COALESCE(produksi_mutasi.jumlah, 0) AS produksi,
		    COALESCE(opname_mutasi.selisih, 0) AS opname,
		    COALESCE(penjualan_mutasi.qty, 0) AS penjualan,
		    COALESCE(produksi_bahan_mutasi.total, 0) AS bahan_baku,
		    (COALESCE(monitoring.stok_periode, 0) + COALESCE(pembelian_awal.qty, 0) + COALESCE(produksi_awal.jumlah, 0) + COALESCE(opname_awal.selisih, 0) - COALESCE(penjualan_awal.qty, 0) - COALESCE(produksi_bahan_awal.total,0)) AS stok_awal,
		    (COALESCE(monitoring.stok_periode, 0) + COALESCE(pembelian_mutasi.qty, 0) + COALESCE(produksi_mutasi.jumlah, 0) + COALESCE(opname_mutasi.selisih, 0) - COALESCE(penjualan_mutasi.qty, 0) - COALESCE(produksi_bahan_mutasi.total,0)) AS stok_mutasi

		FROM barang
		LEFT JOIN (
		    SELECT * FROM monitoring
		    WHERE periode = (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		) monitoring ON monitoring.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang, SUM(qty) AS qty FROM detail_pembelian
		    JOIN pembelian ON pembelian.id_pembelian = detail_pembelian.id_pembelian
		    WHERE LEFT(pembelian.tanggal_pembelian,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		    AND pembelian.tanggal_pembelian < '".$start."'
		    GROUP BY id_barang
		) pembelian_awal ON pembelian_awal.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang_produksi, SUM(jumlah) AS jumlah FROM produksi
		    WHERE LEFT(tanggal_produksi,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		    AND tanggal_produksi < '".$start."'
		    GROUP BY id_barang_produksi
		) produksi_awal ON produksi_awal.id_barang_produksi = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang, SUM(selisih) AS selisih FROM opname
		    WHERE LEFT(tanggal,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		    AND tanggal < '".$start."'
		    GROUP BY id_barang
		) opname_awal ON opname_awal.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang, SUM(qty) AS qty FROM detail_penjualan
		    JOIN penjualan ON penjualan.id_penjualan = detail_penjualan.id_penjualan
		    WHERE LEFT(penjualan.tanggal_penjualan,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		    AND penjualan.tanggal_penjualan < '".$start."'
		    GROUP BY id_barang
		) penjualan_awal ON penjualan_awal.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang_bahan_baku, SUM(total) AS total FROM detail_produksi
		    JOIN produksi ON produksi .id_produksi  = detail_produksi .id_produksi
		    WHERE LEFT(produksi .tanggal_produksi ,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '".date('Y-m', strtotime($start))."')
		    AND produksi .tanggal_produksi < '".$start."'
		    GROUP BY id_barang_bahan_baku
		) produksi_bahan_awal ON produksi_bahan_awal.id_barang_bahan_baku = barang.id_barang

		LEFT JOIN (
		    SELECT id_barang, SUM(qty) AS qty FROM detail_pembelian
		    JOIN pembelian ON pembelian.id_pembelian = detail_pembelian.id_pembelian
		    WHERE pembelian.tanggal_pembelian >= '".$start."'
		    AND pembelian.tanggal_pembelian <= '".$end."'
		    GROUP BY id_barang
		) pembelian_mutasi ON pembelian_mutasi.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang_produksi, SUM(jumlah) AS jumlah FROM produksi
		    WHERE tanggal_produksi >= '".$start."'
		    AND tanggal_produksi <= '".$end."'
		    GROUP BY id_barang_produksi
		) produksi_mutasi ON produksi_awal.id_barang_produksi = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang, SUM(selisih) AS selisih FROM opname
		    WHERE tanggal >= '".$start."'
		    AND tanggal <= '".$end."'
		    GROUP BY id_barang
		) opname_mutasi ON opname_mutasi.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang, SUM(qty) AS qty FROM detail_penjualan
		    JOIN penjualan ON penjualan.id_penjualan = detail_penjualan.id_penjualan
		    WHERE penjualan.tanggal_penjualan >= '".$start."'
		    AND penjualan.tanggal_penjualan <= '".$end."'
		    GROUP BY id_barang
		) penjualan_mutasi ON penjualan_mutasi.id_barang = barang.id_barang
		LEFT JOIN (
		    SELECT id_barang_bahan_baku, SUM(total) AS total FROM detail_produksi
		    JOIN produksi ON produksi .id_produksi  = detail_produksi .id_produksi
		    WHERE produksi .tanggal_produksi >= '".$start."'
		    AND produksi .tanggal_produksi < '".$end."'
		    GROUP BY id_barang_bahan_baku
		) produksi_bahan_mutasi ON produksi_bahan_mutasi.id_barang_bahan_baku = barang.id_barang
		ORDER BY nama_barang ASC";
		return  $this->db->query($query);
	}

	function _tampil(){
		$query = "
			SELECT
			  IF(monitoring.periode, monitoring.periode, LEFT(CURRENT_TIMESTAMP, 7)) AS periode,
			  barang.id_barang,
			  barang.nama_barang,
			  COALESCE(monitoring.stok_periode, 0) AS stok_awal,
			  COALESCE(masuk.stok_masuk, 0) AS stok_masuk,
			  COALESCE(keluar.stok_keluar, 0) AS stok_keluar
			FROM
			  barang
			  LEFT JOIN
			    (SELECT
			      *
			    FROM
			      monitoring
			    WHERE periode < LEFT(CURRENT_TIMESTAMP, 7)
			    GROUP BY id_barang
			    ORDER BY periode DESC) monitoring
			    ON monitoring.id_barang = barang.id_barang
			  LEFT JOIN
			    (SELECT
			      id_barang,
			      SUM(qty) AS stok_masuk
			    FROM
			      (SELECT
			        pembelian.tanggal_pembelian AS tanggal,
			        detail_pembelian.id_barang,
			        IF(detail_pembelian.rafaksi = 0, detail_pembelian.qty, detail_pembelian.nominal_qty_rafaksi) AS qty
			      FROM
			        pembelian
			        JOIN detail_pembelian
			          ON detail_pembelian.id_pembelian = pembelian.id_pembelian
			      WHERE pembelian.status_pembelian = 1 AND LEFT(pembelian.tanggal_pembelian, 7) = LEFT(CURRENT_TIMESTAMP, 7)
			      UNION
			      SELECT
			        produksi.tanggal_produksi AS tanggal,
			        produksi.id_barang_produksi AS id_barang,
			        SUM(produksi.jumlah) AS qty
			      FROM
			        produksi
			      WHERE produksi.status_produksi = 1 AND LEFT(produksi.tanggal_produksi, 7) = LEFT(CURRENT_TIMESTAMP, 7)
			      GROUP BY id_barang
			      UNION
			      SELECT
			        opname.tanggal,
			        opname.id_barang,
			        opname.selisih AS qty
			      FROM
			        opname
			      WHERE opname.selisih > 0
			        AND LEFT(opname.tanggal, 7) = LEFT(CURRENT_TIMESTAMP, 7)) mutasi_masuk
			    GROUP BY id_barang) masuk
			    ON masuk.id_barang = barang.id_barang
			  LEFT JOIN
			    (SELECT
			      id_barang,
			      SUM(qty) AS stok_keluar
			    FROM
			      (SELECT
			        penjualan.tanggal_penjualan AS tanggal,
			        detail_penjualan.id_barang,
			        detail_penjualan.qty
			      FROM
			        penjualan
			        JOIN detail_penjualan
			          ON detail_penjualan.id_penjualan = penjualan.id_penjualan
			      WHERE penjualan.status_penjualan = 1 AND LEFT(penjualan.tanggal_penjualan, 7) = LEFT(CURRENT_TIMESTAMP, 7)
			      UNION
			      SELECT
			        produksi.tanggal_produksi AS tanggal,
			        detail_produksi.id_barang_bahan_baku AS id_barang,
			        SUM(detail_produksi.total) AS qty
			      FROM
			        produksi
			        JOIN detail_produksi
			          ON detail_produksi.id_produksi = produksi.id_produksi
			      WHERE produksi.status_produksi = 1 AND LEFT(produksi.tanggal_produksi, 7) = LEFT(CURRENT_TIMESTAMP, 7)
			      GROUP BY id_barang
			      UNION
			      SELECT
			        opname.tanggal,
			        opname.id_barang,
			        opname.selisih AS qty
			      FROM
			        opname
			      WHERE opname.selisih < 0
			        AND LEFT(opname.tanggal, 7) = LEFT(CURRENT_TIMESTAMP, 7)) mutasi_keluar
			    GROUP BY id_barang) keluar
			    ON keluar.id_barang = barang.id_barang
		";
		return  $this->db->query($query);
	}

	function tampil_filter($date, $tanggal_awal, $tanggal_akhir){
		$query = "
			SELECT
			  monitoring.periode,
			  barang.id_barang,
			  barang.nama_barang,
			  COALESCE(monitoring.stok_periode, 0) AS stok_awal,
			  COALESCE(masuk.stok_masuk, 0) AS stok_masuk,
			  COALESCE(keluar.stok_keluar, 0) AS stok_keluar
			FROM
			  barang
			  LEFT JOIN
			    (SELECT
			      *
			    FROM
			      monitoring
			    WHERE periode < '".$date."'
			    GROUP BY id_barang
			    ORDER BY periode DESC) monitoring
			    ON monitoring.id_barang = barang.id_barang
			  LEFT JOIN
			    (SELECT
			      id_barang,
			      SUM(qty) AS stok_masuk
			    FROM
			      (SELECT
			        pembelian.tanggal_pembelian AS tanggal,
			        detail_pembelian.id_barang,
			        IF(detail_pembelian.rafaksi = 0, detail_pembelian.qty, detail_pembelian.nominal_qty_rafaksi) AS qty
			      FROM
			        pembelian
			        JOIN detail_pembelian
			          ON detail_pembelian.id_pembelian = pembelian.id_pembelian
			      WHERE pembelian.status_pembelian = 1
			      	AND LEFT(pembelian.tanggal_pembelian, 7) >= '".$tanggal_awal."'
			      	AND LEFT(pembelian.tanggal_pembelian, 7) <= '".$tanggal_akhir."'
			      UNION
			      SELECT
			        produksi.tanggal_produksi AS tanggal,
			        produksi.id_barang_produksi AS id_barang,
			        SUM(produksi.jumlah) AS qty
			      FROM
			        produksi
			      WHERE produksi.status_produksi = 1
			      	AND LEFT(produksi.tanggal_produksi, 7) >= '".$tanggal_awal."'
			      	AND LEFT(produksi.tanggal_produksi, 7) <= '".$tanggal_akhir."'
			      GROUP BY id_barang
			      UNION
			      SELECT
			        opname.tanggal,
			        opname.id_barang,
			        opname.selisih AS qty
			      FROM
			        opname
			      WHERE opname.selisih > 0
			        AND LEFT(opname.tanggal, 7) >= '".$tanggal_awal."'
			        AND LEFT(opname.tanggal, 7) <= '".$tanggal_akhir."'
			      ) mutasi_masuk
			    GROUP BY id_barang) masuk
			    ON masuk.id_barang = barang.id_barang
			  LEFT JOIN
			    (SELECT
			      id_barang,
			      SUM(qty) AS stok_keluar
			    FROM
			      (SELECT
			        penjualan.tanggal_penjualan AS tanggal,
			        detail_penjualan.id_barang,
			        detail_penjualan.qty
			      FROM
			        penjualan
			        JOIN detail_penjualan
			          ON detail_penjualan.id_penjualan = penjualan.id_penjualan
			      WHERE penjualan.status_penjualan = 1
			      	AND LEFT(penjualan.tanggal_penjualan, 7) >= '".$tanggal_awal."'
			      	AND LEFT(penjualan.tanggal_penjualan, 7) <= '".$tanggal_akhir."'
			      UNION
			      SELECT
			        produksi.tanggal_produksi AS tanggal,
			        detail_produksi.id_barang_bahan_baku AS id_barang,
			        SUM(detail_produksi.total) AS qty
			      FROM
			        produksi
			        JOIN detail_produksi
			          ON detail_produksi.id_produksi = produksi.id_produksi
			      WHERE LEFT(produksi.tanggal_produksi, 7) >= '".$tanggal_awal."'
			      	AND LEFT(produksi.tanggal_produksi, 7) <= '".$tanggal_akhir."'
			      GROUP BY id_barang
			      UNION
			      SELECT
			        opname.tanggal,
			        opname.id_barang,
			        opname.selisih AS qty
			      FROM
			        opname
			      WHERE opname.selisih < 0
			        AND LEFT(opname.tanggal, 7) >= '".$tanggal_awal."'
			        AND LEFT(opname.tanggal, 7) <= '".$tanggal_akhir."'
			      ) mutasi_keluar
			    GROUP BY id_barang) keluar
			    ON keluar.id_barang = barang.id_barang
		";
		return  $this->db->query($query);
	}

	function input_data($data, $table){
		return $this->db->insert($table, $data);
	}

	function tampil_data($periode, $id){
		$query = "SELECT * FROM monitoring WHERE periode = '".$periode."' AND id_barang = '".$id."'";
		return  $this->db->query($query);
	}

	function tampil_periode($periode){
		$query = "SELECT * FROM monitoring WHERE periode = '".$periode."'";
		return  $this->db->query($query);
	}

	function tampil_max_periode(){
		$query = "SELECT * FROM monitoring WHERE periode = (SELECT MAX(periode) FROM monitoring)";
		return  $this->db->query($query);
	}

	function input_periode($periode, $id, $stok, $table){
		$query = "INSERT INTO '".$table."' (periode, id_barang, stok_periode) VALUES ('".$periode."', '".$id."', '".$stok."')";
		return $this->db->query($query);
	}

	function tampil_data_pembelian($periode, $id){
		$query = "SELECT * FROM monitoring WHERE periode < '".$periode."' AND id_barang = '".$id."' ORDER BY periode DESC limit 1";
		return  $this->db->query($query);
	}

	function update_data($where,$data,$table){
		$query = "UPDATE ".$table." SET stok_periode = ".$data." WHERE id = '".$where."'";
		return $this->db->query($query);
	}

	function update_monitoring($where,$date,$qty,$table){
		$query = "UPDATE ".$table." SET stok_periode = stok_periode + ".$qty." WHERE id = '".$where."' AND periode >= '".$date."'";
		return $this->db->query($query);
	}

	function update_monitoring_pembelian($where,$date,$qty,$table){
		$query = "UPDATE ".$table." SET stok_periode = stok_periode + ".$qty." WHERE id = '".$where."' AND periode > '".$date."'";
		return $this->db->query($query);
	}

	function update_monitoring_penjualan($where,$data,$table){
		$query = "UPDATE ".$table." SET stok_periode = stok_periode - ".$data." WHERE id = '".$where."'";
		return $this->db->query($query);
	}

	function update_monitoring_produksi_bahan_baku($where,$data,$table){
		$query = "UPDATE ".$table." SET stok_periode = stok_periode + ".$data." WHERE id = '".$where."'";
		return $this->db->query($query);
	}

	function update_monitoring_produksi_barang_produksi($where,$data,$table){
		$query = "UPDATE ".$table." SET stok_periode = stok_periode - ".$data." WHERE id = '".$where."'";
		return $this->db->query($query);
	}

}