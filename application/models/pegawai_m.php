<?php

class Pegawai_m extends CI_Model{

	function tampil_data(){
		return $this->db->order_by('nama_pegawai', 'ASC')->get('pegawai');
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(pegawai.id_pegawai,3) as id_pegawai', FALSE);
		$this->db->where('LEFT(pegawai.id_pegawai,'.strlen('PGW0'.$tgl).')', 'PGW0'.$tgl);
		$this->db->order_by('created_at','DESC');
		$this->db->limit(1);
		$query = $this->db->get('pegawai');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_pegawai) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "PGW"."0".$tgl.$batas;
		return $kodetampil;
	}

}