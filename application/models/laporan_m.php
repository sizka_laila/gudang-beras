<?php 
 
class Laporan_m extends CI_Model{

	function tampil_pembelian(){
		$query =  "SELECT pembelian.tanggal_pembelian, pembelian.id_pembelian, pegawai.nama_pegawai, pembelian.id_supplier, supplier.nama_supplier, detail_pembelian.id_barang, barang.nama_barang, detail_pembelian.qty, detail_pembelian.ppn_item, detail_pembelian.nominal_ppn_item, detail_pembelian.rafaksi, detail_pembelian.qty_rafaksi, detail_pembelian.nominal_qty_rafaksi, detail_pembelian.nominal_harga_rafaksi, detail_pembelian.sub_total_harga, pembelian.total_beli, pembelian.status_pembelian FROM pembelian JOIN detail_pembelian ON detail_pembelian.id_pembelian = pembelian.id_pembelian JOIN user ON user.id_user = pembelian.id_user JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai JOIN supplier ON pembelian.id_supplier = supplier.id_supplier JOIN barang ON barang.id_barang = detail_pembelian.id_barang ORDER BY pembelian.tanggal_pembelian DESC";
		return $this->db->query($query);
	}

	function laporan_pembelian($id_pembelian){
		$query = "SELECT pembelian.id_pembelian, pembelian.id_user, user.username, pegawai.nama_pegawai AS user, pembelian.id_transaksi, pembelian.tanggal_pembelian, pembelian.id_supplier, supplier.nama_supplier, pembelian.cara_beli, pembelian.id_pengambil, pengambil.nama_pegawai, pembelian.nama_pengantar, pembelian.total_beli, pembelian.ppn_header, pembelian.nominal_ppn_header, pembelian.total_diskon, pembelian.potongan, pembelian.total_bayar, pembelian.jenis_pembayaran, pembelian.bank FROM pembelian JOIN supplier ON supplier.id_supplier = pembelian.id_supplier LEFT JOIN pegawai AS pengambil ON pengambil.id_pegawai = pembelian.id_pengambil LEFT JOIN user ON user.id_user = pembelian.id_user LEFT JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai WHERE pembelian.id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

	function laporan_pembelian_detail($id_pembelian){
		$query = "SELECT detail_pembelian.id_barang, barang.nama_barang, satuan.nama_satuan, detail_pembelian.qty, detail_pembelian.harga, detail_pembelian.ppn_item, detail_pembelian.nominal_ppn_item, detail_pembelian.rafaksi, detail_pembelian.qty_rafaksi, detail_pembelian.nominal_qty_rafaksi, detail_pembelian.harga_rafaksi, detail_pembelian.nominal_harga_rafaksi, detail_pembelian.sub_total_harga FROM detail_pembelian JOIN barang ON barang.id_barang = detail_pembelian.id_barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_pembelian.id_pembelian = '".$id_pembelian."'";
		return $this->db->query($query);
	}

	function tampil_penjualan(){
		$query =  "SELECT penjualan.tanggal_penjualan, penjualan.id_penjualan, penjualan.id_customer, customer.nama_customer, pegawai.nama_pegawai, detail_penjualan.id_barang, barang.nama_barang, detail_penjualan.qty, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga, penjualan.total_jual, penjualan.status_penjualan FROM penjualan JOIN detail_penjualan ON detail_penjualan.id_penjualan = penjualan.id_penjualan JOIN customer ON penjualan.id_customer = customer.id_customer JOIN barang ON barang.id_barang = detail_penjualan.id_barang JOIN user ON user.id_user = penjualan.id_user JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai ORDER BY penjualan.tanggal_penjualan DESC";
		return $this->db->query($query);
	}

	function laporan_penjualan($id_penjualan){
		$query = "SELECT penjualan.id_penjualan, penjualan.id_user, user.username, pegawai.nama_pegawai AS user, penjualan.tanggal_penjualan, penjualan.id_customer, customer.nama_customer, customer.alamat, penjualan.cara_jual, penjualan.id_pengantar, pengambil.nama_pegawai, penjualan.nama_pengambil, penjualan.total_jual, penjualan.ppn_header, penjualan.nominal_ppn_header, penjualan.total_diskon, penjualan.potongan, penjualan.total_bayar, penjualan.bayar, penjualan.kembali, penjualan.jenis_pembayaran, penjualan.bank FROM penjualan JOIN customer ON customer.id_customer = penjualan.id_customer LEFT JOIN pegawai AS pengambil ON pengambil.id_pegawai = penjualan.id_pengantar LEFT JOIN user ON user.id_user = penjualan.id_user LEFT JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai WHERE penjualan.id_penjualan = '".$id_penjualan."'";
		return  $this->db->query($query);
	}

	function laporan_penjualan_detail($id_penjualan){
		$query = "SELECT detail_penjualan.id_barang, barang.nama_barang, satuan.nama_satuan, detail_penjualan.qty, detail_penjualan.harga, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga FROM detail_penjualan JOIN barang ON barang.id_barang = detail_penjualan.id_barang JOIN satuan ON satuan.id_satuan = barang.id_satuan WHERE detail_penjualan.id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

	function sum_laporan_penjualan_detail($id_penjualan){
		$query = "SELECT detail_penjualan.id_barang, detail_penjualan.qty, SUM(detail_penjualan.qty) AS jumlah_barang, detail_penjualan.harga, detail_penjualan.ppn_item, detail_penjualan.nominal_ppn_item, detail_penjualan.sub_total_harga FROM detail_penjualan WHERE detail_penjualan.id_penjualan = '".$id_penjualan."'";
		return $this->db->query($query);
	}

}