<?php 
 
class User_m extends CI_Model{

	function tampil_data(){
		$query = "SELECT user.id_user, user.id_pegawai, pegawai.nama_pegawai, pegawai.jenis_kelamin, pegawai.alamat, pegawai.telephone, pegawai.email, user.hak_akses, user.username, user.password, user.status, user.tanggal_terdaftar FROM user JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai";
		return $this->db->query($query);
	}

	function ubah_status($tabel,$data,$id){
		return $this->db->update($tabel,$data,$id);
	}

	function profil($id){
		$query = "SELECT user.id_user, user.id_pegawai, pegawai.nama_pegawai, pegawai.jenis_kelamin, pegawai.alamat, pegawai.telephone, pegawai.email, user.hak_akses, user.username, user.password, user.status, user.tanggal_terdaftar FROM user JOIN pegawai ON pegawai.id_pegawai = user.id_pegawai WHERE user.id_user =".$id;
		return $this->db->query($query);
	}

}