<?php 
 
class Master_m extends CI_Model{

	function input_data($data, $table){
		return $this->db->insert($table, $data);
	}
	
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_data($where,$data,$table){
		// $this->db->where($where);
		// $this->db->update($table,$data);
		return $this->db->update($table, $data, $where);
	}	

	function hapus_data($where,$table){
		// $this->db->where($where);
		// $this->db->delete($table);
		return $this->db->delete($table, $where);
	}

}