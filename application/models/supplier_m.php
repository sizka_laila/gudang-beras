<?php

class Supplier_m extends CI_Model{

	function tampil_data(){
		return $this->db->order_by('nama_supplier', 'ASC')->get('supplier');
	}

	public function buat_kode(){
		$tgl=date('dmY');
		$this->db->select('RIGHT(supplier.id_supplier,3) as id_supplier', FALSE);
		$this->db->where('LEFT(supplier.id_supplier,'.strlen('SPL0'.$tgl).')', 'SPL0'.$tgl);
		$this->db->order_by('id_supplier','DESC');
		$this->db->limit(1);
		$query = $this->db->get('supplier');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->id_supplier) + 1;
		}
		else{
			$kode = 1;
		}
		$batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
		$kodetampil = "SPL"."0".$tgl.$batas;
		return $kodetampil;
	}

}