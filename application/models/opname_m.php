<?php

class Opname_m extends CI_Model{

	function tampil_data(){
		$query = "SELECT opname.id, opname.tanggal, opname.id_barang, barang.nama_barang, opname.stok_akhir, opname.stok_opname, opname.selisih, opname.keterangan, opname.keterangan_lain FROM opname JOIN barang ON barang.id_barang = opname.id_barang WHERE opname.tanggal = curdate() ORDER BY opname.tanggal DESC, opname.created_at DESC";
		return  $this->db->query($query);
	}

	function tampil_filter($date, $id_barang){
		$query = "SELECT opname.id, opname.tanggal, opname.id_barang, barang.nama_barang, opname.stok_akhir, opname.stok_opname, opname.selisih, opname.keterangan, opname.keterangan_lain FROM opname JOIN barang ON barang.id_barang = opname.id_barang WHERE LEFT(opname.tanggal, 7) = '".$date."' AND opname.id_barang = '".$id_barang."' ORDER BY opname.tanggal DESC,  opname.created_at DESC";
		return  $this->db->query($query);
	}

	function tampil_opname($tanggal, $id){
		$query = "SELECT * FROM opname WHERE tanggal = '".$tanggal."' AND id_barang = '".$id."'";
		return  $this->db->query($query);
	}

	function input_batch($data, $table){
		return $this->db->insert_batch($table, $data);
	}

	function update_stok($qty, $table, $id){
		// return $this->db->update_batch($table, $data, $where);
		$query = "UPDATE ".$table." SET qty = ".$qty." WHERE id_barang = '".$id."'";
		return $this->db->query($query);
	}

	// function batal_pembelian($qty, $table, $id){
	// 	$query = "UPDATE ".$table." SET qty = qty - ".$qty." WHERE id_barang = '".$id."'";
	// 	return $this->db->query($query);
	// }

}