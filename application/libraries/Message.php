<?php
class Message{
	public function __construct(){		
		$this->_ci=& get_instance();
		//$this->_ci->load->library('session');		
	}

	public function set_success($name,$value){
		$succes_msg=$this->_ci->session->userdata('succes_msg');
		$succes_msg[$name][]=$value;
		$this->_ci->session->set_userdata('succes_msg',$succes_msg);
	}

	public function success($name,$open='',$close=''){				
		$succes_msg=$this->_ci->session->userdata('succes_msg');
		if(isset($succes_msg[$name])){
			if(is_array($succes_msg[$name])){
				if(count($succes_msg[$name])<>0){
					echo $open.'<div class="alert alert-success">';
					foreach ($succes_msg[$name] as $value) {
						if(!empty($value)){
							echo '<i class="fa fa-check-circle"></i> <b>Sukses !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><br />';
						}
					}
					echo '</div>'.$close;
				}
			}else{
				if(!empty($succes_msg[$name])){
					echo $open.'<div class="alert alert-success"><i class="fa fa-check-circle"></i> <b>Sukses !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'.$close;
				}
			}
		}
		unset($succes_msg[$name]);
		$this->_ci->session->set_userdata('succes_msg',$succes_msg);
	}

	public function set_error($name,$value){
		$error_msg=$this->_ci->session->userdata('error_msg');
		$error_msg[$name][]=$value;
		$this->_ci->session->set_userdata('error_msg',$error_msg);
	}

	public function error($name,$open='',$close=''){	
		$error_msg=$this->_ci->session->userdata('error_msg');	
		if(isset($error_msg[$name])){
			if(is_array($error_msg[$name])){
				if(count($error_msg[$name])<>0){
					echo $open.'<div class="alert alert-danger">';
					foreach ($error_msg[$name] as $value) {
						if(!empty($value)){
							echo '<i class="fa fa-exclamation-circle"></i> <b>Sukses !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><br />';
						}
					}
					echo '</div>'.$close;
				}
			}else{
				if(!empty($error_msg[$name])){
					echo $open.'<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <b>Sukses !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'.$close;
				}
			}		
		}	
		unset($error_msg[$name]);
		$this->_ci->session->set_userdata('error_msg',$error_msg);
	}

	public function set_warning($name,$value){
		$warning_msg=$this->_ci->session->userdata('warning_msg');
		$warning_msg[$name][]=$value;
		$this->_ci->session->set_userdata('warning_msg',$warning_msg);
	}
	
	public function warning($name,$open='',$close=''){				
		$warning_msg=$this->_ci->session->userdata('warning_msg');
		if(isset($warning_msg[$name])){
			if(is_array($warning_msg[$name])){
				if(count($warning_msg[$name])<>0){
					echo $open.'<div class="alert alert-warning">';
					foreach ($warning_msg[$name] as $value) {
						if(!empty($value)){
							echo '<i class="fa fa-check-circle"></i> <b>Peringatan !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><br />';
						}
					}
					echo '</div>'.$close;
				}
			}else{
				if(!empty($warning_msg[$name])){
					echo $open.'<div class="alert alert-warning"><i class="fa fa-check-circle"></i> <b>Peringatan !</b> '.$value.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'.$close;
				}
			}
		}
		unset($warning_msg[$name]);
		$this->_ci->session->set_userdata('warning_msg',$warning_msg);
	}

	public function show($name,$open='',$close=''){
		$this->success($name,$open,$close);
		$this->error($name,$open,$close);
	}
}