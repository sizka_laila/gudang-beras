<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UD. Mitra Tani</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugin/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/plugin/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <?php render('css') ?>
    <link rel="stylesheet" href="<?= base_url('public/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/css/skins/_all-skins.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/css/style.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-purple-light sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="<?= base_url('home/index') ?>" class="logo">
                <span class="logo-mini"><b>MT</b></span>
                <span class="logo-lg">UD. <b>Mitra</b> Tani</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= base_url('public/img/logo/user.png') ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $this->session->userdata('nama_pegawai'); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?= base_url('public/img/logo/user.png') ?>" class="img-circle" alt="User Image">
                                    <p>
                                        Hello <?= $this->session->userdata('nama_pegawai'); ?> !
                                    </p>
                                </li>
                                <li class="user-body">
                                    <div class="col-xs-12 text-center">
                                        <a href="<?= base_url('login/logout') ?>" class="btn btn-default btn-block btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= base_url('user/profil/') ?><?= $this->session->userdata('id_user'); ?>"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <?php
                        if($this->session->userdata('hak_akses')==1){
                    ?>
                    <li><a href="<?= base_url('home/index') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                    <?php
                        }else if($this->session->userdata('hak_akses')==2){
                    ?>
                    <li><a href="<?= base_url('home/index') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                    <li><a href="<?= base_url('pembelian/index') ?>"><i class="fa fa-cart-plus"></i> <span>Pembelian</span></a></li>
                    <li><a href="<?= base_url('produksi/index') ?>"><i class="fa fa-cubes"></i> <span>Produksi</span></a></li>
                    <li><a href="<?= base_url('penjualan/index') ?>"><i class="fa fa-money"></i> <span>Penjualan</span></a></li>
                    <?php
                        }else if($this->session->userdata('hak_akses')==3){
                    ?>
                    <li><a href="<?= base_url('home/index') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-database"></i> <span>Master Data</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= base_url('satuan/index') ?>"><i class="fa fa-circle-o"></i> Satuan</a></li>
                            <li><a href="<?= base_url('barang/index') ?>"><i class="fa fa-circle-o"></i> Barang</a></li>
                            <li><a href="<?= base_url('pegawai/index') ?>"><i class="fa fa-circle-o"></i> Pegawai</a></li>
                            <li><a href="<?= base_url('supplier/index') ?>"><i class="fa fa-circle-o"></i> Supplier</a></li>
                            <li><a href="<?= base_url('customer/index') ?>"><i class="fa fa-circle-o"></i> Customer</a></li>
                            <li><a href="<?= base_url('bahan_baku/index') ?>"><i class="fa fa-circle-o"></i> Resep</a></li>
                            <!-- <li><a href="<?= base_url('resep/index') ?>"><i class="fa fa-circle-o"></i> Resep</a></li> -->
                        </ul>
                    </li>
                    <li><a href="<?= base_url('pembelian/index') ?>"><i class="fa fa-cart-plus"></i> <span>Pembelian</span></a></li>
                    <li><a href="<?= base_url('produksi/index') ?>"><i class="fa fa-cubes"></i> <span>Produksi</span></a></li>
                    <li><a href="<?= base_url('penjualan/index') ?>"><i class="fa fa-money"></i> <span>Penjualan</span></a></li>
                    <li><a href="<?= base_url('user/profil/') ?><?= $this->session->userdata('id_user'); ?>"><i class="fa fa-user"></i> <span>Profil User</span></a></li>
                    <li><a href="<?= base_url('user/index') ?>"><i class="fa fa-users"></i> <span>User Management</span></a></li>
                    <li><a href="<?= base_url('opname/index') ?>"><i class="fa fa-sort-numeric-asc"></i> <span>Stok Opname</span></a></li>
                    <li><a href="<?= base_url('monitoring/index') ?>"><i class="fa fa-qrcode"></i> <span>Monitoring Stok</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= base_url('laporan/pembelian') ?>"><i class="fa fa-circle-o"></i> Laporan Pembelian</a></li>
                            <li><a href="<?= base_url('laporan/penjualan') ?>"><i class="fa fa-circle-o"></i> Laporan Penjualan</a></li>
                        </ul>
                    </li>
                    <?php
                        }else{
                            redirect(base_url('login/logout'));
                        }
                    ?>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <?php render('content') ?>
        </div>
        <footer class="main-footer">
            <!-- <div class="pull-right hidden-xs"><b><img src="<?= base_url('public/img/logo/haltec.png') ?>" class="
            img-responsive logo-footer" alt=""></b></div> -->
            <strong>Copyright &copy; 2019 <!-- <a href="javascript:;">HALTEC</a>. --></strong>
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>
    <script type="text/javascript" src="<?= base_url('public/js/jquery-3.3.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/js/popper1.14.7.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/plugin/bootstrap-4.3.1/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/plugin/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/plugin/fastclick/fastclick.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/js/app.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/js/demo.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/js/public.js') ?>"></script>
    <?php render('js') ?>
    <?php render('custom_js') ?>
</body>
</html>
