<?php
    header('Content-Type: application/vnd.ms-excel; charset=utf-8');
    header("Content-Disposition: attachment; filename=laporan_penjualan_harian.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false)
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Laporan Penjualan Harian</title>
</head>
<body>
    <h1 class="selling-title" style="font-size: 30px; font-weight: bold;">Data Penjualan Harian</h1>
    <table class="selling-detail-table" style="border: 1px solid #000; border-spacing: 0px;" width="1200px">
        <tr>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">No</td>
            <td style="border: 1px solid #000; padding: 5px">Tanggal</td>
            <td style="border: 1px solid #000; padding: 5px">ID Barang</td>
            <td style="border: 1px solid #000; padding: 5px">Barang</td>
            <td style="border: 1px solid #000; padding: 5px">Satuan</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">Qty</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Harga</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">PPN</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Total</td>
        </tr>
        <?php
            $no = 1;
            foreach ($tampil_penjualan as $data) {
        ?>
        <tr>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= $no++;?></td>
            <td style="border: 1px solid #000; padding: 5px"><?= date('d-m-Y', strtotime($data->tanggal_penjualan)) ?></td>
            <td style="border: 1px solid #000; padding: 5px"><?= $data->id_barang ?></td>
            <td style="border: 1px solid #000; padding: 5px"><?= $data->nama_barang ?></td>
            <td style="border: 1px solid #000; padding: 5px"><?= $data->nama_satuan ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= rupiah_nota($data->qty) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->harga) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->nominal_ppn_item) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->sub_total_harga) ?></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>