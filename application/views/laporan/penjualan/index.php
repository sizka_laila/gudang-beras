<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datepicker();
        $('.select2').select2();
        $('#datatable').DataTable();

        $('#btn-cetak').click(function() {
            var id_customer = $('#id_customer option:selected').val();
            var id_user = $('#id_user option:selected').val();
            var status_penjualan = $('#status_penjualan option:selected').val();
            var periode_awal = $('#periode_awal').val();
            var periode_akhir = $('#periode_akhir').val();
            var id_rekap = $('#id_rekap option:selected').val();
            document.location.href = '<?= base_url('laporan/cetak_penjualan') ?>?id_customer='+id_customer+'&id_user='+id_user+'&periode_awal='+periode_awal+'&periode_akhir='+periode_akhir+'&id_rekap='+id_rekap+'&status_penjualan='+status_penjualan;
            $('#CetakLaporan').modal('hide');
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Laporan Penjualan</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <div class="box-body">
            <form class="form-horizontal" action="<?= base_url('laporan/filter_penjualan') ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Periode</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" name="periode_awal" value="<?= (isset($periode_awal) ? $periode_awal:'') ?>" id="periode_awal" placeholder="Masukkan Tanggal Awal">
                            <div class="input-group-addon">s.d.</div>
                            <input type="text" class="form-control datepicker" name="periode_akhir" value="<?= (isset($periode_akhir) ? $periode_akhir:'') ?>" id="periode_akhir" placeholder="Masukkan Tanggal Akhir">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Customer</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_customer" id="id_customer" style="width: 100%;">
                            <option value="">- Pilih Customer -</option>
                            <?php foreach($customer as $select){ ?>
                                <option value="<?= $select->id_customer ?>" <?= (isset($id_customer) && $id_customer == $select->id_customer) ? 'selected' : '' ?>><?= $select->id_customer ?> - <?= $select->nama_customer ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">User</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_user" id="id_user" style="width: 100%;">
                            <option value="">- Pilih User -</option>
                            <?php foreach($user as $select){ ?>
                                <option value="<?= $select->id_user ?>" <?= (isset($id_user) && $id_user == $select->id_user) ? 'selected' : '' ?>><?= $select->nama_pegawai ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status Penjualan</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="status_penjualan" id="status_penjualan" style="width: 100%;">
                            <?php (isset($_POST['status_penjualan'])) ? $status_penjualan = $_POST['status_penjualan'] : $status_penjualan=''; ?>
                            <option value="">- Pilih Status Penjualan -</option>
                            <option value="1" <?php if($status_penjualan==1) echo 'selected'; ?>>Aktif</option>
                            <option value="2" <?php if($status_penjualan==2) echo 'selected'; ?>>Dibatalkan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari Data</button>
                        <!-- <button class="btn btn-default" type="button" id="btn-cetak" target="_blank"><i class="fa fa-download"></i> Cetak Data</button> -->
                        <a href="#CetakLaporan" class="btn btn-default" type="button" data-toggle="modal"><i class="fa fa-print"></i> Cetak Data</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box box-warning">
        <!-- <div class="box-header with-border">
            <a href="<?= base_url('laporan/semua_penjualan') ?>" class="btn btn-default"><i class="fa fa-download"></i> Cetak Semua Penjualan</a>
        </div> -->
        <div class="box-body">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th>Waktu</th>
                        <th>Kode Penjualan</th>
                        <th>Customer</th>
                        <th>User</th>
                        <th>Barang</th>
                        <th class="text-center">Qty</th>
                        <th class="text-right">Total Bayar</th>
                        <th class="text-center">Status Penjualan</th>
                        <!-- <th width="160" class="text-center">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        foreach($tampil_penjualan as $data){
                            if($data->status_penjualan==1){
                                $data->status_penjualan = 'Aktif';
                                $label_status_penjualan = 'label-success';
                                $disable = '';
                            }else{
                                $data->status_penjualan = 'Dibatalkan';
                                $label_status_penjualan = 'label-danger';
                                $disable = 'disabled';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= date("d-m-Y H:i:s", strtotime($data->tanggal_penjualan)) ?></td>
                        <td><?= $data->id_penjualan ?></td>
                        <td><?= $data->id_customer ?> - <?= $data->nama_customer ?></td>
                        <td><?= $data->nama_pegawai ?></td>
                        <td><?= $data->id_barang ?> - <?= $data->nama_barang ?></td>
                        <td class="text-center"><?= $data->qty ?></td>
                        <td class="text-right"><?= rupiah($data->sub_total_harga) ?></td>
                        <td class="text-center"><label class="label <?= $label_status_penjualan ?>"><?= $data->status_penjualan ?></label></td>
                        <!-- <td class="text-center">
                            <a href="<?= base_url('laporan/penjualan_excel/'.$data->id_penjualan) ?>" class="btn btn-success btn-sm"><i class="fa fa-file-text" target="_blank"></i> Cetak Excel</a>
                            <a href="<?= base_url('laporan/detail_penjualan/'.$data->id_penjualan) ?>" class="btn btn-default btn-sm"><i class="fa fa-download" target="_blank"></i> Cetak PDF</a>
                        </td> -->
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<div class="modal modal-default" id="CetakLaporan" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Pilihan Cetak Laporan</strong></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Rekap</label> -->
                        <div class="col-sm-12">
                            <select class="form-control" name="id_rekap" id="id_rekap" style="width: 100%;">
                                <option value="" disabled diselected>- Pilih Rekap -</option>
                                <option value="1">Nota</option>
                                <option value="2">Harian</option>
                                <option value="3">Bulanan</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" id="btn-cetak" target="_blank"><i class="fa fa-download"></i> Cetak Laporan</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>
