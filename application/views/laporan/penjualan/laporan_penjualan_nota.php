<?php
    header('Content-Type: application/vnd.ms-excel; charset=utf-8');
    header("Content-Disposition: attachment; filename=laporan_penjualan_nota.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false)
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Penjualan Nota</title>
</head>
<body>
    <h1 class="selling-title" style="font-size: 30px; font-weight: bold;">Data Penjualan Nota</h1>
    <table class="selling-detail-table" style="border: 1px solid #000; border-spacing: 0px;" width="1200px">
        <tr>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">No</td>
            <td style="border: 1px solid #000; padding: 5px">Tanggal</td>
            <td style="border: 1px solid #000; padding: 5px">Kode Penjualan</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">Status Penjualan</td>
            <td style="border: 1px solid #000; padding: 5px">Customer</td>
            <td style="border: 1px solid #000; padding: 5px">User</td>
            <td style="border: 1px solid #000; padding: 5px">Barang</td>
            <td style="border: 1px solid #000; padding: 5px">Satuan</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">Qty</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Harga</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;">PPN</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Nominal PPN</td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Sub Total</td>
        </tr>
        <?php
            $no = 1;
            $no_nota = '';
            foreach ($tampil_penjualan as $data) {
                if($data->ppn_item==0){
                    $data->ppn_item = '0';
                }else{
                    $data->ppn_item = '10%';
                }
                if($data->status_penjualan==1){
                    $data->status_penjualan = 'Aktif';
                }else{
                    $data->status_penjualan = 'Dibatalkan';
                }
        ?>
        <tr>
            <?php if ($no_nota != $data->id_penjualan) { ?>
                <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= $no;?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= date('d-m-Y', strtotime($data->tanggal_penjualan)) ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data->id_penjualan ?></td>
                <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= $data->status_penjualan ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data->id_customer ?> - <?= $data->nama_customer ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data->nama_pegawai ?></td>
                <?php 
                    $no_nota = $data->id_penjualan; 
                    $no++;
                ?>
            <?php } else { ?>
                <td style="border: 1px solid #000; padding: 5px"></td>
                <td style="border: 1px solid #000; padding: 5px"></td>
                <td style="border: 1px solid #000; padding: 5px"></td>
                <td style="border: 1px solid #000; padding: 5px"></td>
                <td style="border: 1px solid #000; padding: 5px"></td>
                <td style="border: 1px solid #000; padding: 5px"></td>
            <?php } ?>
            <td style="border: 1px solid #000; padding: 5px"><?= $data->nama_barang ?></td>
            <td style="border: 1px solid #000; padding: 5px"><?= $data->nama_satuan ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= rupiah_nota($data->qty) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->harga) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: center;"><?= $data->ppn_item ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->nominal_ppn_item) ?></td>
            <td style="border: 1px solid #000; padding: 5px; text-align: right;">Rp <?= rupiah_format($data->sub_total_harga) ?></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>