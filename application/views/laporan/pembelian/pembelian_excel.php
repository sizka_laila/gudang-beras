<?php
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment; filename=detail_pembelian.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Detail Pembelian</title>
</head>
<body>
    <h1 class="selling-title" style="font-size: 30px; font-weight: bold;">Detail Pembelian</h1>
    <br />
    <br />
    <br />
        <table class="selling-table" style="font-size: 17px; font-weight: bold;" width="1200px" border="0">
            <?php
                foreach($data_pembelian as $data){
                    if($data->cara_beli==1){
                        $cara_beli = 'Diantar';
                    }else{
                        $cara_beli = 'Diambil';
                    }
            ?>
            <tr>
                <td width="200px">Kode Pembelian</td>
                <td width="50px">:</td>
                <td width="500px"><?= $data->id_pembelian ?></td>
                <td width="200px">Customer</td>
                <td width="50px">:</td>
                <td width="500px"><?= $data->id_supplier ?> - <?= $data->nama_supplier ?></td>
            </tr>
            <tr>
                <td>Kode Transaksi</td>
                <td>:</td>
                <td><?= $data->id_transaksi ?></td>
                <td>Cara Jual</td>
                <td>:</td>
                <td><?= $cara_beli ?> ( <?php if($data->cara_beli==2){ echo $data->id_pengambil; echo ' - '; echo $data->nama_pegawai; }else{ echo $data->nama_pengantar; } ?> )</td>
            </tr>
            <tr>
                <td>Tanggal Pembelian</td>
                <td>:</td>
                <td><?= date("d-m-Y H:i:s", strtotime($data->tanggal_pembelian)) ?></td>
            </tr>
            <?php } ?>
        </table>
        <br />
        <br />
        <table class="selling-detail-table" style="border: 1px solid #000; border-spacing: 0px;" width="1200px">
            <tr>
                <td style="border: 1px solid #000; padding: 5px">No</td>
                <td style="border: 1px solid #000; padding: 5px">Barang</td>
                <td style="border: 1px solid #000; padding: 5px">Satuan</td>
                <td style="border: 1px solid #000; padding: 5px">Qty</td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Harga</td>
                <td style="border: 1px solid #000; padding: 5px">PPN</td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Nominal PPN</td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Sub Total</td>
            </tr>
            <?php
                $no = 1;
                foreach($data_detail_pembelian as $data_detail){
                    if($data_detail->ppn_item==0){
                        $data_detail->ppn_item = 'Tidak Ada';
                    }else{
                        $data_detail->ppn_item = 'Ada';
                    }
            ?>
            <tr>
                <td style="border: 1px solid #000; padding: 5px"><?= $no++;?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data_detail->id_barang ?> - <?= $data_detail->nama_barang ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data_detail->nama_satuan ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data_detail->qty ?></td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Rp <?= rupiah_format($data_detail->harga) ?></td>
                <td style="border: 1px solid #000; padding: 5px"><?= $data_detail->ppn_item ?></td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Rp <?= rupiah_format($data_detail->nominal_ppn_item) ?></td>
                <td style="border: 1px solid #000; padding: 5px" class="text-right">Rp <?= rupiah_format($data_detail->sub_total_harga) ?></td>
            </tr>
            <?php } ?>
        </table>
        <br />
        <br />
        <table class="selling-table" style="font-size: 17px; font-weight: bold;" width="1200px" border="0">
            <?php
                foreach($data_pembelian as $data){
                    if($data->ppn_header==1){
                        $ppn_header = 'Ada';
                    }else{
                        $ppn_header = 'Tidak Ada';
                    }

                    if($data->jenis_pembayaran==1){
                        $jenis_pembayaran = 'Tunai';
                    }else{
                        $jenis_pembayaran = 'Non Tunai';
                    }
            ?>
            <tr>
                <td width="200px">Total Jual</td>
                <td width="50px">:</td>
                <td width="500px">Rp <?= rupiah_format($data->total_beli) ?></td>
                <td width="200px">Potongan</td>
                <td width="50px">:</td>
                <td width="500px">Rp <?= rupiah_format($data->potongan) ?></td>
            </tr>
            <tr>
                <td>PPN</td>
                <td>:</td>
                <td><?= $ppn_header ?></td>
                <td>Jenis Pembayaran</td>
                <td>:</td>
                <td><?= $jenis_pembayaran ?> <?php if($data->jenis_pembayaran==2){ echo ' - '; echo $data->bank; } ?></td>
            </tr>
            <tr>
                <td>Nominal PPN</td>
                <td>:</td>
                <td><?= rupiah_format($data->nominal_ppn_header) ?></td>
                <td>Total Bayar</td>
                <td>:</td>
                <td rowspan="2" class="selling-number" style="font-weight: bold; font-size: 35px; padding: 0px;">Rp <?= rupiah_format($data->total_bayar) ?></td>
            </tr>
            <tr>
                <td>Diskon</td>
                <td>:</td>
                <td><?= $data->total_diskon ?> %</td>
            </tr>
            <?php } ?>
        </table>
</body>
</html>