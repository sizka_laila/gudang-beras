<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Surat Jalan</title>
    <style>
        body{
            /* width: 21.5cm; */
            /* height: 14cm; */
            /*margin: -1.2cm -1.2cm;*/
            margin: 0px;
            padding: 18px 17px;
            font-family: Arial;
            /* font-family: Calibri; */
        }
        .nota{
            /* border: 1px solid #000; */
            width: 100%;
            /* height: 80%; */
            /* height: 14cm; */
            /* background: #ccc; */
        }
        .top-nota{
            /* height: 15% !important; */
            /* background: #999; */
        }
        .middle-nota{
            /* height: 70% !important; */
            /* background: #333; */
            /* padding-top: 100px !important; */
        }
        .bottom-nota{
            /* height: 15% !important; */
            /* background: #ccc; */
        }
        .header-table{
            font-size: 22px;
            margin-bottom: 20px;
            width: 100%;
        }
        .header-table tr td{
            padding: 0px;
        }
        .header-table-brand{
            /* font-size: 20px; */
            /* font-weight: bold; */
            padding: 0px -50px 0px 30px !important;
            /* background-image: url('../../public/img/logo/logo-padi.png'); */
            /* background-repeat: no-repeat; */
            /* background-size: 57px 50px; */
        }
        .header-table-title{
            font-weight: bold;
            /* padding: 0px 20px !important; */
            font-size: 25px;
        }
        .header-table-information{
            /*background-color: #999;*/
            font-weight: bold;
            padding: 6px;
            border: 1px solid #000;
            font-size: 17px;
        }
        .header-table-bottom{
            font-size: 18px;
        }
        .header-table-bottom-left{
            width: 300px !important;
        }
        .header-table-bottom-right{
            width: 300px !important;
        }
        .selling-table{
            font-size: 21px;
            /* border: 1px solid #000; */
            border-spacing: 0px;
            /* font-family: Arial; */
            width: 100%;
        }
        .selling-table .selling-table-top th{
            border: 1px solid #000;
            padding: 2px 3px;
            font-size: 22px;
        }
        .selling-table tr td{
            /* border: 1px solid #000; */
            padding: 2px 3px;
        }
        .selling-table .selling-table-count th{
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
            padding: 4px 0px;
            font-size: 22px;
        }
        .selling-detail-table{
            /* font-size: 14px; */
        }
        .signature-table{
            font-size: 20px;
            border-spacing: 0px;
            margin-top: 20px;
            width: 100%;
        }
        .signature-table tr td{
            padding: 0px 6px;
        }
        .text-left{
            text-align: left;
        }
        .text-center{
            text-align: center;
        }
        .text-right{
            text-align: right;
        }
        .page-break{
            page-break-before: always;
        }
        @page{
            margin: 0px;
            padding: 0px;
        }
        @font-face{
            font-family: Arial;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }
        @media print{
            body{
                /* width: 21.5cm; */
                /* height: 14cm; */
                /*margin: -1.2cm -1cm;*/
                /*font-family: Arial;*/
                /* font-family: Courier; */
            }
        }
    </style>
    <script type="text/javascript">
        // window.onload = function(){
        //     window.print();
        // }
    </script>
</head>
<body>
    <table class="nota">
        <tr class="top-nota">
            <td height="150">
                <table class="header-table">
                    <!-- <tr class="header-table-top">
                        <td class="header-table-brand" rowspan="2"><img src="<?= $_SERVER["DOCUMENT_ROOT"].'/gudang-beras/public/img/logo/logo-padi.png';?>" width="70px" alt=""></td>
                        <td class="text-center">
                            <span class="header-table-title">UD. MITRA TANI</span><br />
                        </td>
                        <td>
                            <span class="header-table-information">SURAT JALAN</span><br />
                        </td>
                    </tr> -->
                    <tr>
                        <td><span class="header-table-title">UD. MITRA TANI</span><br /></td>
                        <td></td>
                        <td><span class="header-table-information">SURAT JALAN</span></td>
                    </tr>
                    <tr class="header-table-bottom">
                        <td class="header-table-bottom-left" valign="top">
                            <span>Desa Banjarsari, Kec.Trucuk, Kab.Bojonegoro</span><br />
                            <span>(0355) 885 279 / 0812 3583 6005</span><br /><br />
                        </td>
                        <!-- <td class="text-center address">
                            <span>Desa Banjarsari, Kec.Trucuk, Kab.Bojonegoro</span><br />
                            <span>(0355) 885 279 / 0812 3583 6005</span><br /><br /><br />
                        </td> -->
                        <?php foreach($data_penjualan as $data){ ?>
                        <td valign="top">
                            <span>Nomor : <?= $data->id_penjualan ?></span><br />
                            <span>Tujuan : <?= $data->alamat ?></span>
                        </td>
                        <?php } ?>
                        <?php foreach($data_penjualan as $data){ ?>
                        <td class="header-table-bottom-right" valign="top">
                            <span>Tanggal : <?= date("d-m-Y", strtotime($data->tanggal_penjualan)) ?></span><br />
                            <span>Nopol :</span><br />
                        </td>
                        <?php } ?>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="middle-nota" >
            <td valign="top" height="185">
                <table class="selling-table">
                    <tr class="selling-table-top">
                        <th width="1px" class="text-center">No</th>
                        <th class="text-left">Nama Barang</th>
                        <th width="150px" class="text-left">Satuan</th>
                        <th width="100px" class="text-center">Qty</th>
                    </tr>
                    <?php
                        $no = 1;
                        foreach($data_detail_penjualan as $data_detail){
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++; ?></td>
                        <td><?= $data_detail->nama_barang ?></td>
                        <td><?= $data_detail->nama_satuan ?></td>
                        <td class="text-center"><?= rupiah_nota($data_detail->qty) ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="selling-table">
                    <tr class="selling-table-count">
                        <?php foreach($sum_laporan_penjualan_detail as $data){ ?>
                            <th class="text-center" width="920px">Jumlah</th>
                            <th class="text-center"><?= rupiah_nota($data->jumlah_barang) ?></th>
                        <?php } ?>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="bottom-nota">
            <td height="120">
                <table class="signature-table">
                    <tr>
                        <td class="text-center">Admin,</td>
                        <td class="text-center">Kepala Gudang</td>
                        <td class="text-center">Sopir,</td>
                    </tr>
                    <tr>
                        <td style="color: #fff;">A</td>
                    </tr>
                    <tr>
                        <td style="color: #fff;">A</td>
                    </tr>
                    <tr>
                        <td class="text-center">(.............................................)</td>
                        <td class="text-center">(.............................................)</td>
                        <td class="text-center">(.............................................)</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>