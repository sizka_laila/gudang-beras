<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<!-- <link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>"> -->
<!-- <link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>"> -->
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script> -->
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker();
        $('.select2').select2();
        $('#pengantar').show();
        $('#pengambil').hide();
        $('#tunai').show();
        $('#non_tunai').hide();
        // $('#datatable').DataTable();
        $('#txtNominalPpn').val(0);
        $('#barang-potongan').number(true, 0, ',', '.');
        $('#barang-bayar').number(true, 0, ',', '.');
        // $('input[type="checkbox"].flat-red').iCheck({
        //     checkboxClass: 'icheckbox_flat-green'
        // });
        // $('input').on('ifChecked ifUnchecked', function(event){
        //     if(event.type == 'ifChecked'){
        //         var harga = $('#txtHarga').val();
        //         $('#txtNominalPpn').val(harga*10/100);
        //     }else{
        //         $('#txtNominalPpn').val(0);
        //     }
        // });
        $('#form-add-barang-qty, #form-add-barang-harga').keyup(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var nominal_ppn_item = qty*harga*(10/100);
            var nominal_ppn_item_output = 1*harga*(10/100);
            var sub_total_harga = qty*harga;
            // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
            // $('#form-add-barang-sub_total_harga').val(sub_total_harga);
            if($('#form-add-barang-ppn_item').is(':checked')){
                $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item_output).number(true, 0, ',', '.');
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
            }else{
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-nominal_ppn_item').val(0);
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga.toString()).number(true, 0, ',', '.');
            }
            SetTotalJual();
            SetTotalJualPpn();
        });
        $('#form-add-barang-ppn_item').change(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var nominal_ppn_item_output = 1*harga*(10/100);
            var nominal_ppn_item = qty*harga*(10/100);
            var sub_total_harga = qty*harga;
            // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
            if($(this).is(':checked')){
                $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item_output.toString()).number(true, 0, ',', '.');
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
            }else{
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-nominal_ppn_item').val(0);
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga.toString()).number(true, 0, ',', '.');
            }
            SetTotalJual();
            SetTotalJualPpn();
            SetPPNItem();
        });
        $('#barang-ppn_header').change(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var nominal_ppn_item = qty*harga*(10/100);
            var nominal_ppn_item_output = 1*harga*(10/100);
            var sub_total_harga = qty*harga;
            if($(this).is(':checked')){
                $('input[id*="barang-ppn_item"]').prop('checked', true).change();
                $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item_output.toString()).number(true, 0, ',', '.');
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
            }else{
                $('input[id*="barang-ppn_item"]').prop('checked', false).change();
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-nominal_ppn_item').val(0);
                $('#form-add-barang-sub_total_harga').val(sub_total_harga.toString()).number(true, 0, ',', '.');
                $('#form-add-barang-sub_total_harga-ppn').val(sub_total_harga.toString()).number(true, 0, ',', '.');
            }
            SetPPNHeader();
        });
        $('#barang-diskon, #barang-potongan').keyup(function(){
            SetTotalBayar();
        });
        $('#barang-bayar').keyup(function(){
            SetKembali();
        });
    });
    function BlockInputBarangHarga() {
        $('#form-add-barang-harga').select();
    }
    function BlockInputBarangHargaId(id_barang) {
        $('#barang-harga-'+id_barang).select();
    }
    function BlockInputBarangPotongan() {
        $('#barang-potongan').select();
    }
    function CaraJual(){
        var value = $('#CaraPenjualan').val();
        if(value=='1'){
            $('#pengantar').show();
            $('#pengambil').hide();
        }else{
            $('#pengantar').hide();
            $('#pengambil').show();
        }
    }
    function JenisPembayaran(){
        var value = $('#jenis_pembayaran').val();
        if(value=='1'){
            $('#tunai').show();
            $('#non_tunai').hide();
        }else if(value=='2'){
            $('#tunai').hide();
            $('#non_tunai').show();
        }else{
            $('#tunai').show();
            $('#non_tunai').hide();
        }
    }
    function ChangeSelectBarang(){
        var id_barang = $('#form-add-barang-id_barang').val();
        $.ajax({
            url : '<?= base_url() ?>penjualan/cari_barang/'+id_barang,
            type : 'post',
            dataType : 'json',
            success:function(response){
                $('#form-add-barang-id_barang').val(response['id_barang']);
                $('#form-add-barang-nama_barang').val(response['nama_barang']);
                $('#form-add-barang-id_satuan').val(response['id_satuan']);
                $('#form-add-barang-nama_satuan').val(response['nama_satuan']);
                $('#form-add-barang-harga').val(formatRupiah(response['harga_jual']));
                $('#form-add-barang-ppn_item').val(response['ppn_jual']);
                if(response['ppn_jual']==1){
                    $('#form-add-barang-ppn_item').prop('checked', true);
                }else{
                    $('#form-add-barang-ppn_item').prop('checked', false);
                }
                $('#form-add-barang-nominal_ppn_item').val(response['nominal_ppn_jual']).number(true, 0, ',', '.');
            }
        });
    }
    function SetTotalJualPpn(){
        var total_beli = 0;
        $.each($('.data_hitung_sub_total_harga_ppn'), function() {
            total_beli += toFloat(number_value($(this).val()));
        });
        $('#barang-total_jual-ppn').val(total_beli.toString()).number(true, 0, ',', '.');
        SetPPNHeader();
        SetTotalBayar();
    }
    function SetTotalJual(){
        var total_jual = 0;
        $.each($('.data_hitung_sub_total_harga'), function() {
            total_jual += toFloat(number_value($(this).val()));
        });
        $('#barang-total_jual').val(total_jual.toString()).number(true, 0, ',', '.');
        // $('#barang-ppn_header').click (function (){
        //     var thisCheck = $(this);
        //     if (thischeck.is (':checked')){
        //         SetPPNHeader();
        //     }else{
        //         SetPPNItem();
        //     }
        // });
        SetPPNHeader();
        SetTotalBayar();
    }
    function SetPPNItem(){
        var ppn_item = 0;
        $.each($('.data_hitung_nominal_ppn_item_data'), function() {
            ppn_item += toFloat(number_value($(this).val()));
        });
        $('#barang-total_ppn_item').val(ppn_item.toString()).number(true, 0, ',', '.');
    }
    function SetPPNHeader(){
        var ppn_header = 0;
        $.each($('.data_hitung_nominal_ppn_item_data'), function() {
            ppn_header += toFloat(number_value($(this).val()));
        });
        $('#barang-nominal_ppn_header').val(ppn_header.toString()).number(true, 0, ',', '.');
    }
    function SetTotalBayar(){
        var total_jual = toFloat(number_value($('#barang-total_jual').val()));
        var ppn_item = toFloat(number_value($('#barang-total_ppn_item').val()));
        var ppn_header = toFloat(number_value($('#barang-nominal_ppn_header').val()));
        var diskon = toFloat(number_value($('#barang-diskon').val()));
        var potongan = $('#barang-potongan').val();
        var harga_potongan = number_value(potongan);
        diskon = total_jual*(diskon/100);
        var total_bayar = total_jual-diskon-harga_potongan+ppn_header;
        $('#barang-total_bayar').val(total_bayar.toString()).number(true, 0, ',', '.');
    }
    function SetKembali(){
        var total_bayar = toFloat(number_value($('#barang-total_bayar').val()));
        var bayar = toFloat(number_value($('#barang-bayar').val()));
        if(bayar<total_bayar){
            var kembali = 0;
        }else{
            var kembali = bayar-total_bayar;
        }
        $('#barang-kembali').val(kembali.toString()).number(true, 0, ',', '.');
    }
    function addBarang(){
        var id_barang = $('#form-add-barang-id_barang').val();
        var nama_barang = $('#form-add-barang-nama_barang').val();
        var nama_satuan = $('#form-add-barang-nama_satuan').val();
        var qty = $('#form-add-barang-qty').val();
        var harga = $('#form-add-barang-harga').val();
        var ppn_item = $('#form-add-barang-ppn_item').is(':checked') ? 1 : 0;
        var nominal_ppn_item = $('#form-add-barang-nominal_ppn_item').val();
        var nominal_ppn_item_data = $('#form-add-barang-nominal_ppn_item_data').val();
        var sub_total_harga = $('#form-add-barang-sub_total_harga').val();
        var sub_total_harga_ppn = $('#form-add-barang-sub_total_harga-ppn').val();
        if (id_barang == '') {
            alert('ID Barang belum diisi !');
            return false;
        }
        if (nama_satuan == '') {
            alert('Satuan belum diisi !');
            return false;
        }
        if (qty == '') {
            alert('Qty belum diisi !');
            return false;
        }
        if (harga == '') {
            alert('Harga belum diisi !');
            return false;
        }
        if (nominal_ppn_item == '') {
            return 0;
        }
        if ($('tr[data-row-id="'+id_barang+'"]').length == 0) {
            var html_row = '<tr data-row-id="'+id_barang+'">';
                html_row += '<td><input type="hidden" class="form-control" name="barang['+id_barang+'][id_barang]" value="'+id_barang+'" id="barang-id_barang-'+id_barang+'" readonly><input type="text" class="form-control" name="barang['+id_barang+'][nama_barang]" value="'+nama_barang+'" id="barang-nama_barang-'+nama_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control" name="barang['+id_barang+'][nama_satuan]" value="'+nama_satuan+'" id="barang-nama_satuan-'+id_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="barang['+id_barang+'][qty]" value="'+qty+'" id="barang-qty-'+id_barang+'" onkeyup="SubTotal(\''+id_barang+'\')"></td>';
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right" name="barang['+id_barang+'][harga]" value="'+harga+'" id="barang-harga-'+id_barang+'" onkeyup="hargaBarang(\''+id_barang+'\')" onclick="BlockInputBarangHargaId(\''+id_barang+'\')"></div></td>';
                if (ppn_item == 1) {
                    html_row += '<td><label><input type="checkbox" class="flat-red" name="barang['+id_barang+'][ppn_item]" value="'+ppn_item+'" id="barang-ppn_item-'+id_barang+'" onchange="hargaPPN(\''+id_barang+'\')" checked> 10 %</label></td>';
                } else {
                    html_row += '<td><label><input type="checkbox" class="flat-red" name="barang['+id_barang+'][ppn_item]" value="'+ppn_item+'" id="barang-ppn_item-'+id_barang+'" onchange="hargaPPN(\''+id_barang+'\')"> 10 %</label></td>';
                }
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right" name="barang['+id_barang+'][nominal_ppn_item]" value="'+nominal_ppn_item+'" id="barang-nominal_ppn_item-'+id_barang+'" readonly><input type="hidden" class="form-control text-right data_hitung_nominal_ppn_item_data" name="barang['+id_barang+'][nominal_ppn_item_data]" value="'+nominal_ppn_item_data+'" id="barang-nominal_ppn_item_data-'+id_barang+'" readonly></div></td>';
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="hidden" class="form-control text-right data_hitung_sub_total_harga" name="barang['+id_barang+'][sub_total_harga]" value="'+sub_total_harga+'" id="barang-sub_total_harga-'+id_barang+'" readonly><input type="text" class="form-control text-right data_hitung_sub_total_harga_ppn" name="barang['+id_barang+'][sub_total_harga_ppn]" value="'+sub_total_harga_ppn+'" id="barang-sub_total_harga-ppn-'+id_barang+'" readonly></div></td>';
                html_row += '<td><button type="button" class="btn btn-danger btn-sm" onclick="removeBarang(\''+id_barang+'\')"><i class="fa fa-trash"></i></button></td>';
            html_row += '</tr>';
            $('#form-add-barang').before(html_row);
            $('#barang-nominal_ppn_item-'+id_barang).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-'+id_barang).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).number(true, 0, ',', '.');
            $('#barang-harga-'+id_barang).keyup(function() {
                $(this).val($(this).val()).number(true, 0, ',', '.');
            });
            $('#form-add-barang-id_barang').val('').change();
            $('#form-add-barang-nama_barang').val('');
            $('#form-add-barang-nama_satuan').val('');
            $('#form-add-barang-qty').val('');
            $('#form-add-barang-harga').val('');
            $('#form-add-barang-ppn_item').prop('checked', false);
            $('#form-add-barang-nominal_ppn_item').val(0);
            $('#form-add-barang-nominal_ppn_item_data').val(0);
            $('#form-add-barang-sub_total_harga').val(0);
            $('#form-add-barang-sub_total_harga-ppn').val(0);
            // $('input[type="checkbox"].flat-red').iCheck({
            //     checkboxClass: 'icheckbox_flat-green'
            // });
            SetTotalJual();
            SetTotalJualPpn();
            SetPPNHeader();
            SetTotalBayar();
        } else {
            alert('Data barang sudah ada !');
        }
    }
    function hargaPPN(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var nominal_ppn_item = qty*harga*(10/100);
        var nominal_ppn_item_output = 1*harga*(10/100);
        var sub_total_harga = qty*harga;
        // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 0, ',', '.');
            $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
            var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
        }
        SetTotalJual();
        SetTotalJualPpn();
    }
    function SubTotal(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var nominal_ppn_item = qty*harga*(10/100);
        var nominal_ppn_item_output = 1*harga*(10/100);
        var sub_total_harga = qty*harga;
        // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
        // $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga);
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 0, ',', '.');
            $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
            var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
        }
        SetTotalJual();
        SetTotalJualPpn();
    }
    function hargaBarang(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var nominal_ppn_item = qty*harga*(10/100);
        var nominal_ppn_item_output = 1*harga*(10/100);
        var sub_total_harga = qty*harga;
        // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
        // $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga);
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 0, ',', '.');
            $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 0, ',', '.');
            var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga_end.toString()).number(true, 0, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
            $('#barang-sub_total_harga-ppn-'+id_barang).val(sub_total_harga.toString()).number(true, 0, ',', '.');
        }
        SetTotalJual();
        SetTotalJualPpn();
    }
    function removeBarang(id_barang) {
        $('#barang tbody tr[data-row-id="'+id_barang+'"]').remove();
        SetTotalJual();
        SetTotalJualPpn();
    }
    // $('#form-add-barang-nominal_ppn_item, #barang-potongan, #form-add-barang-harga, #form-add-barang-nominal_ppn_item, #form-add-barang-sub_total_harga, #barang-total_jual, #total_ppn_item, #nominal_ppn_header, #barang-diskon, #barang-total_bayar, #barang-bayar').keyup(function() {
    //     $(this).val(formatRupiah($(this).val()));
    // });
    $('#form-add-barang-harga').keyup(function() {
        $(this).val(formatRupiah($(this).val()));
    });
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    // function hargaPPN(val){
    //     if($('input[type="checkbox"]').iCheck('checked')){
    //         var harga = $('#txtHarga').val();
    //         $('#txtNominalPpn').val(harga*10/100);
    //     }else{
    //         $('#txtNominalPpn').val(0);
    //     }        
    // }
    function simpan(){
        var id_customer = $('#barang-id_customer').val();
        var cara_jual = $('#CaraPenjualan').val();
        var total_jual = $('#barang-total_jual').val();
        var total_bayar = $('#barang-total_bayar').val();
        // var bayar = $('#barang-bayar').val();
        var jenis_pembayaran = $('#jenis_pembayaran').val();
        // var id_barang = $('#barang-id_barang-'+id_barang).val();
        // var bayar = $('#barang-bayar').val();
        // var kembali = $('#barang-kembali').val();
        if (id_customer == '' || cara_jual == '' || total_jual == '' || total_bayar == '' || jenis_pembayaran == '') {
            alert('Masih ada data yang belum diisi ! Silahkan cek kembali !');
            return false;
        }else{
            $('#form-penjualan').submit();
            // if(bayar < total_bayar){
            //     alert('Nominal bayar kurang !');
            // }else{
            //     // alert('simpan');
            //     $('#form-penjualan').submit();
            // }
        }
    }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Penjualan</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" id="form-penjualan" action="<?= base_url('penjualan/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Penjualan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_penjualan" value="<?= $kode; ?>" placeholder="Masukkan Kode Penjualan" required readonly>
                        <input type="hidden" value="<?= $this->session->userdata('id_user'); ?>" name="id_user">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Penjualan</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" name="tanggal_penjualan" placeholder="Masukkan Tanggal Penjualan" required>
                        </div>
                        <!-- <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" value="<?= date('d-m-Y H:i:s') ?>" name="tanggal_penjualan" placeholder="Masukkan Tanggal Penjualan" required>
                        </div> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Customer</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_customer" id="barang-id_customer" style="width: 100%;" required>
                            <option value="">- Pilih Customer -</option>
                            <?php foreach($customer as $select){ ?>
                                <option value="<?= $select->id_customer ?>"><?= $select->id_customer ?> - <?= $select->nama_customer ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Cara Jual</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" style="width: 100%;" name="cara_jual" id="CaraPenjualan" onchange="CaraJual()" required>
                            <option value="" disabled diselected>- Pilih Cara Jual -</option>
                            <option value="1">Diantar</option>
                            <option value="2">Diambil</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <span id="pengantar">
                            <select class="form-control select2" name="id_pengantar" style="width: 100%;">
                                <option value="">- Pilih Pengantar -</option>
                                <?php foreach($pegawai as $select){ ?>
                                    <option value="<?= $select->id_pegawai ?>"><?= $select->id_pegawai ?> - <?= $select->nama_pegawai ?></option>
                                <?php } ?>
                            </select>
                        </span>
                        <input type="text" class="form-control" name="nama_pengambil" id="pengambil" placeholder="Masukkan Nama Pengambil">
                    </div>
                </div>
            </div>
            <!-- <div class="box-footer">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Barang</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_barang" style="width: 100%;">
                            <option value="" disabled diselected>- Pilih Barang -</option>
                            <?php foreach($barang as $select){ ?>
                                <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Barang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_barang" placeholder="Masukkan Nama Barang" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Qty</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="qty" placeholder="Masukkan Qty" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Satuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="satuan" placeholder="Masukkan Satuan" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga" id="txtHarga" placeholder="Masukkan Harga" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN Item</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn" id="txtPpn" value="1"> 10 %
                        </label>
                    </div>
                    <div class="col-sm-9" id="txtNominal">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn" id="txtNominalPpn" placeholder="Nominal PPN Item" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus-square"></i> Masukkan ke List</button>
                    </div>
                </div>
            </div> -->
            <div class="box-footer">
                <table class="table table-striped table-bordered" id="barang">
                    <thead class="thead-dark">
                        <tr>
                            <th>Barang</th>
                            <th width="130px">Satuan</th>
                            <th width="80px" class="text-center">Qty</th>
                            <th width="150px" class="text-right">Harga</th>
                            <th colspan="2" class="text-center">PPN</th>
                            <th width="170px" class="text-center">Sub Total</th>
                            <th width="1" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="form-add-barang">
                            <td>
                                <select class="form-control select2" id="form-add-barang-id_barang" onchange="ChangeSelectBarang()" style="width: 100%;">
                                    <option value="">- Pilih Barang -</option>
                                    <?php foreach($barang as $select){ ?>
                                        <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="nama_barang" id="form-add-barang-nama_barang" placeholder="Masukkan Nama Barang">
                            </td>
                            <td><input type="text" class="form-control" name="nama_satuan" id="form-add-barang-nama_satuan" placeholder="Satuan" required readonly></td>
                            <td><input type="text" class="form-control text-center" name="qty" id="form-add-barang-qty" placeholder="Qty"></td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="harga" onclick="BlockInputBarangHarga()" id="form-add-barang-harga" placeholder="Harga">
                                </div>
                            </td>
                            <td width="70px">
                                <label><input type="checkbox" class="flat-red" name="ppn" id="form-add-barang-ppn_item" value="1"> 10 %</label>
                            </td>
                            <td width="150px">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="nominal_ppn" id="form-add-barang-nominal_ppn_item" placeholder="Nominal PPN Item" required readonly>
                                    <input type="hidden" class="form-control text-right" id="form-add-barang-nominal_ppn_item_data" placeholder="Nominal PPN Item" required readonly>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="hidden" class="form-control text-right" name="sub_total_harga" id="form-add-barang-sub_total_harga" placeholder="Sub Total" required readonly>
                                    <input type="text" class="form-control text-right" name="sub_total_harga_ppn" id="form-add-barang-sub_total_harga-ppn" placeholder="Sub Total" required readonly>
                                </div>
                            </td>
                            <td><button class="btn btn-sm btn-primary" type="button" name="add_barang" onclick="addBarang()"><i class="fa fa-plus"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Jual</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="total_jual" id="barang-total_jual" placeholder="Masukkan Total Jual" required readonly>
                            <input type="hidden" class="form-control text-right" name="total_jual_ppn" id="barang-total_jual-ppn" placeholder="Masukkan Total Jual PPN" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Diskon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control text-right" name="diskon" id="barang-diskon" placeholder="Masukkan Diskon">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Potongan</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="potongan" id="barang-potongan" onclick="BlockInputBarangPotongan()" placeholder="Masukkan potongan">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn_header" id="barang-ppn_header" value="1"> 10 %
                        </label>
                    </div>
                    <div class="col-sm-9" id="txtNominal">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn_header" id="barang-nominal_ppn_header" placeholder="Nominal PPN" readonly>
                        </div>
                        <input type="hidden" class="form-control text-right" name="total_ppn_item" id="barang-total_ppn_item" placeholder="Nominal PPN">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Bayar</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control price text-right" value="" name="total_bayar" id="barang-total_bayar" placeholder="0" required readonly>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Bayar</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="bayar" id="barang-bayar" placeholder="Masukkan Bayar" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kembali</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="kembali" id="barang-kembali" placeholder="Masukkan Kembali" readonly>
                        </div>
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Pembayaran</label>
                    <div class="col-sm-5">
                        <select class="form-control" style="width: 100%;" name="jenis_pembayaran" id="jenis_pembayaran" onchange="JenisPembayaran()" required>
                            <!-- <option value="">- Pilih Jenis Pembayaran -</option> -->
                            <option value="1">Tunai</option>
                            <option value="2">Non Tunai</option>
                            <option value="3">Piutang</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="tunai" placeholder="Masukkan Bank" readonly>
                        <input type="text" class="form-control" name="non_tunai" id="non_tunai" placeholder="Masukkan Bank">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="button" onclick="simpan()"><i class="fa fa-check"></i> Simpan Data</button>
                        <a href="<?= base_url('penjualan/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>