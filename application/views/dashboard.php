<?php section('css') ?>
<!-- <link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>"> -->
<?php endsection() ?>

<?php section('js') ?>
<!-- <script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script> -->
<!-- <script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script> -->
<script type="text/javascript" src="<?= base_url('public/plugin/chartjs/Chart.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        // $('#datatable').DataTable();
        var ctx = document.getElementById("barChart").getContext('2d');
		var barChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: <?= json_encode($periode_jual); ?>,
				datasets: [{
					label: 'Total Penjualan',
					data: <?= json_encode($total_jual); ?>,
					backgroundColor: 'rgb(255, 208, 137)',
					borderColor: 'rgb(239, 143, 0)',
				borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});

		var speedCanvas = document.getElementById("speedChart");
		var speedData = {
		  	labels: <?= json_encode($periode_beli); ?>,
		  	datasets: [{
			    label: "Total Pembelian",
			    data: <?= json_encode($total_beli); ?>,
			    backgroundColor: 'rgb(173, 212, 255)',
				borderColor: 'rgb(0, 78, 196)',
		  	}]
		};
		var chartOptions = {
		  	legend: {
			    display: true,
			    position: 'top',
			    labels: {
			      	boxWidth: 80,
			      	fontColor: 'black'
			    }
		  	}
		};
		var lineChart = new Chart(speedCanvas, {
		  	type: 'line',
		  	data: speedData,
		  	options: chartOptions
		});
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?php foreach($jumlah_pembelian as $data){ echo $data->jumlah_pembelian; } ?></h3>
					<p>Jumlah Pembelian</p>
				</div>
				<div class="icon">
					<i class="fa fa-cart-plus"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?php foreach($jumlah_penjualan as $data){ echo $data->jumlah_penjualan; } ?></h3>
					<p>Jumlah Penjualan</p>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><?php foreach($jumlah_customer as $data){ echo $data->jumlah_customer; } ?></h3>
					<p>Jumlah Customer</p>
				</div>
				<div class="icon">
					<i class="fa fa-users"></i>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><?php foreach($jumlah_supplier as $data){ echo $data->jumlah_supplier; } ?></h3>
					<p>Jumlah Supplier</p>
				</div>
				<div class="icon">
					<i class="fa fa-user"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="box box-warning">
        		<div class="box-body">
        			<div class="chart">
						<canvas id="speedChart" width="510" height="250"></canvas>
					</div>
        		</div>
        	</div>
		</div>
		<div class="col-sm-6">
			<div class="box box-warning">
        		<div class="box-body">
					<div class="chart">
                    	<canvas id="barChart" style="height: 250px; width: 510px;" width="510" height="250"></canvas>
	                </div>
	            </div>
        	</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="box box-warning">
				<div class="box-header with-border">
		            <span class="dashboard-title">Data Pembelian</span>
		        </div>
		        <div class="box-body">
		        	<table class="table table-striped table-bordered">
			            <thead class="thead-dark">
			                <tr>
			                    <th width="1" class="text-center">No</th>
			                    <th>Supplier</th>
			                    <th width="150" class="text-right">Total Bayar</th>
			                    <th class="text-center">Total Pembelian</th>
			                </tr>
			            </thead>
			            <tbody>
			            	<?php 
		                        $no = 1;
		                        foreach($data_pembelian as $data){
		                    ?>
			                <tr>
			                    <td class="text-center"><?= $no++ ?></td>
			                    <td><?= $data->id_supplier ?> - <?= $data->nama_supplier ?></td>
			                    <td class="text-right"><?= rupiah($data->bayar) ?></td>
			                    <td class="text-center"><?= $data->jumlah ?></td>
			                </tr>
			            	<?php } ?>
			            </tbody>
			        </table>
		        </div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="box box-warning">
				<div class="box-header with-border">
		            <span class="dashboard-title">Data Penjualan</span>
		        </div>
		        <div class="box-body">
		        	<table class="table table-striped table-bordered">
			            <thead class="thead-dark">
			                <tr>
			                    <th width="1" class="text-center">No</th>
			                    <th>Customer</th>
			                    <th width="150" class="text-right">Total Bayar</th>
			                    <th class="text-center">Total Penjualan</th>
			                </tr>
			            </thead>
			            <tbody>
			            	<?php 
		                        $no = 1;
		                        foreach($data_penjualan as $data){
		                    ?>
			                <tr>
			                    <td class="text-center"><?= $no++ ?></td>
			                    <td><?= $data->id_customer ?> - <?= $data->nama_customer ?></td>
			                    <td class="text-right"><?= rupiah($data->bayar) ?></td>
			                    <td class="text-center"><?= $data->jumlah ?></td>
			                </tr>
			            	<?php } ?>
			            </tbody>
			        </table>
		        </div>
			</div>
		</div>
	</div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>
