<!-- PEMBELIAN TANGGAL 17-12-2019 -->

<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<!-- <link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>"> -->
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script> -->
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker();
        $('.select2').select2();
        // $('#datatable').DataTable();
        $('#pengantar').show();
        $('#pengambil').hide();
        $('#form-add-barang-nominal_ppn_item').val(0);
        $('#form-add-barang-sub_total_harga').val(0);
        /*$('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });*/
        $('#form-add-barang-ppn_item').change(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var rafaksi = $('#form-add-barang-rafaksi').val();
            if(rafaksi==null||rafaksi==""){
                var qty_rafaksi = 0;
                var nominal_qty_rafaksi = 0;
                var harga_rafaksi = 0;
                var nominal_harga_rafaksi = 0;
                var sub_total_harga = qty*harga;
            }else{
                var qty_rafaksi = qty*(rafaksi/100);
                var nominal_qty_rafaksi = qty-qty_rafaksi;
                var harga_rafaksi = harga*(rafaksi/100);
                var nominal_harga_rafaksi = harga-harga_rafaksi;
                var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
            }
            // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
            $('#form-add-barang-qty_rafaksi').val(qty_rafaksi.toString());
            $('#form-add-barang-nominal_qty_rafaksi').val(nominal_qty_rafaksi.toString());
            $('#form-add-barang-harga_rafaksi').val(harga_rafaksi.toString()).number(true, 2, ',', '.');
            $('#form-add-barang-nominal_harga_rafaksi').val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
            if($(this).is(':checked')){
                if(rafaksi==null||rafaksi==""){
                    var nominal_ppn_item_output = 1*harga*(10/100);
                    var nominal_ppn_item = qty*harga*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
                    // $.number(nominal_ppn_item_rafaksi.toString(), 2, ',', '.');
                    // $.number(nominal_ppn_item.toString(), 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                }else{
                    var nominal_ppn_item_output = 1*nominal_harga_rafaksi*(10/100);
                    var nominal_ppn_item = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
                    // $.number(nominal_ppn_item_rafaksi.toString(), 2, ',', '.');
                    // $.number(nominal_ppn_item.toString(), 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                }
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }else{
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-nominal_ppn_item').val(0);
                var sub_total_harga_end = sub_total_harga;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }
            SetTotalBeli();
        });
        /*$('input[id*="barang-qty"]').keyup(function(){
            var qty = toFloat(number_value($('#form-add-barang-qty')).val());
            var harga = toFloat(number_value($('#form-add-barang-harga')).val());
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item')).val());
            var nominal_ppn_item = qty*harga*(10/100);
            var sub_total_harga = qty*harga;
            // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
            // $('#form-add-barang-sub_total_harga').val(sub_total_harga);
            if($('#form-add-barang-ppn_item').is(':checked')){
                $('#form-add-barang-ppn_item').val(formatRupiah(nominal_ppn_item));
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
                $('#form-add-barang-sub_total_harga').val(formatRupiah(sub_total_harga_end));
            }else{
                $('#form-add-barang-ppn_item').val(0);
                $('#form-add-barang-sub_total_harga').val(formatRupiah(sub_total_harga));
            }
            SetTotalBeli();
        });*/
        $('#form-add-barang-qty, #form-add-barang-harga, #form-add-barang-rafaksi').keyup(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var rafaksi = toFloat(number_value($('#form-add-barang-rafaksi').val()));
            if(rafaksi==null||rafaksi==""){
                var qty_rafaksi = 0;
                var nominal_qty_rafaksi = 0;
                var harga_rafaksi = 0;
                var nominal_harga_rafaksi = 0;
                var sub_total_harga = qty*harga;
            }else{
                var qty_rafaksi = qty*(rafaksi/100);
                var nominal_qty_rafaksi = qty-qty_rafaksi;
                var harga_rafaksi = harga*(rafaksi/100);
                var nominal_harga_rafaksi = harga-harga_rafaksi;
                var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
            }
            // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
            // $('#form-add-barang-sub_total_harga').val(sub_total_harga);
            $('#form-add-barang-qty_rafaksi').val(qty_rafaksi.toString());
            $('#form-add-barang-nominal_qty_rafaksi').val(nominal_qty_rafaksi.toString());
            $('#form-add-barang-harga_rafaksi').val(harga_rafaksi.toString()).number(true, 2, ',', '.');
            $('#form-add-barang-nominal_harga_rafaksi').val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
            if($('#form-add-barang-ppn_item').is(':checked')){
                if(rafaksi==null||rafaksi==""){
                    var nominal_ppn_item_output = 1*harga*(10/100);
                    var nominal_ppn_item = qty*harga*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
                    // $.number(nominal_ppn_item_rafaksi.toString(), 2, ',', '.');
                    // $.number(nominal_ppn_item.toString(), 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                }else{
                    var nominal_ppn_item_output = 1*nominal_harga_rafaksi*(10/100);
                    var nominal_ppn_item = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
                    // $.number(nominal_ppn_item_rafaksi.toString(), 2, ',', '.');
                    // $.number(nominal_ppn_item.toString(), 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                }
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }else{
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-ppn_item').val(0);
                var sub_total_harga_end = sub_total_harga;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }
            SetTotalBeli();
        });

        $('#barang-ppn_header').change(function(){
            var qty = $('#form-add-barang-qty').val();
            var harga = toFloat(number_value($('#form-add-barang-harga').val()));
            var nominal_ppn = toFloat(number_value($('#form-add-barang-nominal_ppn_item').val()));
            var nominal_ppn_data = toFloat(number_value($('#form-add-barang-nominal_ppn_item_data').val()));
            var rafaksi = toFloat(number_value($('#form-add-barang-rafaksi').val()));
            // var nominal_ppn_item = qty*harga*(10/100);
            // var nominal_ppn_item_output = 1*harga*(10/100);
            // var sub_total_harga = qty*harga;
            if(rafaksi==null||rafaksi==""){
                var qty_rafaksi = 0;
                var nominal_qty_rafaksi = 0;
                var harga_rafaksi = 0;
                var nominal_harga_rafaksi = 0;
                var sub_total_harga = qty*harga;
            }else{
                var qty_rafaksi = qty*(rafaksi/100);
                var nominal_qty_rafaksi = qty-qty_rafaksi;
                var harga_rafaksi = harga*(rafaksi/100);
                var nominal_harga_rafaksi = harga-harga_rafaksi;
                var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
            }
            $('#form-add-barang-qty_rafaksi').val(qty_rafaksi.toString());
            $('#form-add-barang-nominal_qty_rafaksi').val(nominal_qty_rafaksi.toString());
            $('#form-add-barang-harga_rafaksi').val(harga_rafaksi.toString()).number(true, 2, ',', '.');
            $('#form-add-barang-nominal_harga_rafaksi').val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
            if($(this).is(':checked')){
                $('input[id*="barang-ppn_item"]').prop('checked', true).change();
                if(rafaksi==null||rafaksi==""){
                    var nominal_ppn_item_output = 1*harga*(10/100);
                    var nominal_ppn_item = qty*harga*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
                }else{
                    var nominal_ppn_item_rafaksi_output = 1*nominal_harga_rafaksi*(10/100);
                    var nominal_ppn_item_rafaksi = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                    var sub_total_harga_end = sub_total_harga+nominal_ppn_item_rafaksi-nominal_harga_rafaksi;
                    $('#form-add-barang-nominal_ppn_item_data').val(nominal_ppn_item_rafaksi.toString()).number(true, 2, ',', '.');
                    $('#form-add-barang-nominal_ppn_item').val(nominal_ppn_item_rafaksi_output.toString()).number(true, 2, ',', '.');
                }
                // var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }else{
                $('input[id*="barang-ppn_item"]').prop('checked', false).change();
                $('#form-add-barang-nominal_ppn_item_data').val(0);
                $('#form-add-barang-nominal_ppn_item').val(0);
                var sub_total_harga_end = sub_total_harga;
                $('#form-add-barang-sub_total_harga').val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
            }
            SetPPNHeader();
        });

        $('#barang-diskon, #barang-potongan').keyup(function(){
            SetTotalBayar();
        });
    });
    function CaraBeli(){
        var value = $('#CaraPembelian').val();
        if(value=='1'){
            $('#pengantar').show();
            $('#pengambil').hide();
        }else{
            $('#pengantar').hide();
            $('#pengambil').show();
        }
    }
    function JenisPembayaran(){
        var value = $('#jenis_pembayaran').val();
        if(value=='1'){
            $('#non_tunai').attr('readonly', true);
        }else{
            $('#non_tunai').attr('readonly', false);
        }
    }
    function hargaBarang(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var rafaksi = toFloat(number_value($('#barang-rafaksi-'+id_barang).val()));
        // var nominal_ppn_item_output = harga*(10/100);
        // var nominal_ppn_item = qty*nominal_ppn_item_output;
        if(rafaksi==null||rafaksi==""){
            var qty_rafaksi = 0;
            var nominal_qty_rafaksi = 0;
            var harga_rafaksi = 0;
            var nominal_harga_rafaksi = 0;
            var sub_total_harga = qty*harga;
        }else{
            var qty_rafaksi = qty*(rafaksi/100);
            var nominal_qty_rafaksi = qty-qty_rafaksi;
            var harga_rafaksi = harga*(rafaksi/100);
            var nominal_harga_rafaksi = harga-harga_rafaksi;
            var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
        }
        $('#barang-qty_rafaksi-'+id_barang).val(qty_rafaksi.toString());
        $('#barang-nominal_qty_rafaksi-'+id_barang).val(nominal_qty_rafaksi.toString());
        $('#barang-harga_rafaksi-'+id_barang).val(harga_rafaksi.toString()).number(true, 2, ',', '.');
        $('#barang-nominal_harga_rafaksi-'+id_barang).val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            // $('#barang-nominal_ppn_item-'+id_barang).val(formatRupiah(nominal_ppn_item_output.toString()));
            // $('#barang-nominal_ppn_item_data-'+id_barang).val(formatRupiah(nominal_ppn_item.toString()));
            // var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
            if(rafaksi==null||rafaksi==""){
                var nominal_ppn_item_output = 1*harga*(10/100);
                var nominal_ppn_item = qty*harga*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
            }else{
                var nominal_ppn_item_rafaksi_output = 1*nominal_harga_rafaksi*(10/100);
                var nominal_ppn_item_rafaksi = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item_rafaksi-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item_rafaksi.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_rafaksi_output.toString()).number(true, 2, ',', '.');
            }
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            var sub_total_harga_end = sub_total_harga;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }
        SetTotalBeli();
    }
    function hargaPPN(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var rafaksi = toFloat(number_value($('#barang-rafaksi-'+id_barang).val()));
        // var nominal_ppn_item_output = harga*(10/100);
        // var nominal_ppn_item = qty*nominal_ppn_item_output;
        if(rafaksi==null||rafaksi==""){
            var qty_rafaksi = 0;
            var nominal_qty_rafaksi = 0;
            var harga_rafaksi = 0;
            var nominal_harga_rafaksi = 0;
            var sub_total_harga = qty*harga;
        }else{
            var qty_rafaksi = qty*(rafaksi/100);
            var nominal_qty_rafaksi = qty-qty_rafaksi;
            var harga_rafaksi = harga*(rafaksi/100);
            var nominal_harga_rafaksi = harga-harga_rafaksi;
            var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
        }
        $('#barang-qty_rafaksi-'+id_barang).val(qty_rafaksi.toString());
        $('#barang-nominal_qty_rafaksi-'+id_barang).val(nominal_qty_rafaksi.toString());
        $('#barang-harga_rafaksi-'+id_barang).val(harga_rafaksi.toString()).number(true, 2, ',', '.');
        $('#barang-nominal_harga_rafaksi-'+id_barang).val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
        // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            if(rafaksi==null||rafaksi==""){
                var nominal_ppn_item_output = 1*harga*(10/100);
                var nominal_ppn_item = qty*harga*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
            }else{
                var nominal_ppn_item_rafaksi_output = 1*nominal_harga_rafaksi*(10/100);
                var nominal_ppn_item_rafaksi = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item_rafaksi-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item_rafaksi.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_rafaksi_output.toString()).number(true, 2, ',', '.');
            }
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            var sub_total_harga_end = sub_total_harga;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }
        SetTotalBeli();
    }
    function hargaRafaksi(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var rafaksi = toFloat(number_value($('#barang-rafaksi-'+id_barang).val()));
        // var nominal_ppn_item_output = harga*(10/100);
        // var nominal_ppn_item = qty*nominal_ppn_item_output;
        if(rafaksi==null||rafaksi==""){
            var qty_rafaksi = 0;
            var nominal_qty_rafaksi = 0;
            var harga_rafaksi = 0;
            var nominal_harga_rafaksi = 0;
            var sub_total_harga = qty*harga;
        }else{
            var qty_rafaksi = qty*(rafaksi/100);
            var nominal_qty_rafaksi = qty-qty_rafaksi;
            var harga_rafaksi = harga*(rafaksi/100);
            var nominal_harga_rafaksi = harga-harga_rafaksi;
            var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
        }
        $('#barang-qty_rafaksi-'+id_barang).val(qty_rafaksi.toString());
        $('#barang-nominal_qty_rafaksi-'+id_barang).val(nominal_qty_rafaksi.toString());
        $('#barang-harga_rafaksi-'+id_barang).val(harga_rafaksi.toString()).number(true, 2, ',', '.');
        $('#barang-nominal_harga_rafaksi-'+id_barang).val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            if(rafaksi==null||rafaksi==""){
                var nominal_ppn_item_output = 1*harga*(10/100);
                var nominal_ppn_item = qty*harga*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
            }else{
                var nominal_ppn_item_rafaksi_output = 1*nominal_harga_rafaksi*(10/100);
                var nominal_ppn_item_rafaksi = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item_rafaksi-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item_rafaksi.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_rafaksi_output.toString()).number(true, 2, ',', '.');
            }
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            var sub_total_harga_end = sub_total_harga;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }
        SetTotalBeli();
    }
    function SubTotal(id_barang){
        var qty = $('#barang-qty-'+id_barang).val();
        var harga = toFloat(number_value($('#barang-harga-'+id_barang).val()));
        var nominal_ppn = toFloat(number_value($('#barang-nominal_ppn_item-'+id_barang).val()));
        var nominal_ppn_data = toFloat(number_value($('#barang-nominal_ppn_item_data-'+id_barang).val()));
        var rafaksi = toFloat(number_value($('#barang-rafaksi-'+id_barang).val()));
        // var nominal_ppn_item_output = harga*(10/100);
        // var nominal_ppn_item = qty*nominal_ppn_item_output;
        if(rafaksi==null||rafaksi==""){
            var qty_rafaksi = 0;
            var nominal_qty_rafaksi = 0;
            var harga_rafaksi = 0;
            var nominal_harga_rafaksi = 0;
            var sub_total_harga = qty*harga;
        }else{
            var qty_rafaksi = qty*(rafaksi/100);
            var nominal_qty_rafaksi = qty-qty_rafaksi;
            var harga_rafaksi = harga*(rafaksi/100);
            var nominal_harga_rafaksi = harga-harga_rafaksi;
            var sub_total_harga = nominal_qty_rafaksi*nominal_harga_rafaksi;
        }
        $('#barang-qty_rafaksi-'+id_barang).val(qty_rafaksi.toString());
        $('#barang-nominal_qty_rafaksi-'+id_barang).val(nominal_qty_rafaksi.toString());
        $('#barang-harga_rafaksi-'+id_barang).val(harga_rafaksi.toString()).number(true, 2, ',', '.');
        $('#barang-nominal_harga_rafaksi-'+id_barang).val(nominal_harga_rafaksi.toString()).number(true, 2, ',', '.');
        // var sub_total_harga_uncheck = (qty*harga)+nominal_ppn;
        // $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga);
        if($('#barang-ppn_item-'+id_barang).is(':checked')){
            // $('#barang-nominal_ppn_item-'+id_barang).val(formatRupiah(nominal_ppn_item_output.toString()));
            // $('#barang-nominal_ppn_item_data-'+id_barang).val(formatRupiah(nominal_ppn_item.toString()));
            // var sub_total_harga_end = sub_total_harga+nominal_ppn_item;
            if(rafaksi==null||rafaksi==""){
                var nominal_ppn_item_output = 1*harga*(10/100);
                var nominal_ppn_item = qty*harga*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_output.toString()).number(true, 2, ',', '.');
            }else{
                var nominal_ppn_item_rafaksi_output = 1*nominal_harga_rafaksi*(10/100);
                var nominal_ppn_item_rafaksi = nominal_qty_rafaksi*nominal_harga_rafaksi*(10/100);
                var sub_total_harga_end = sub_total_harga+nominal_ppn_item_rafaksi-nominal_harga_rafaksi;
                $('#barang-nominal_ppn_item_data-'+id_barang).val(nominal_ppn_item_rafaksi.toString()).number(true, 2, ',', '.');
                $('#barang-nominal_ppn_item-'+id_barang).val(nominal_ppn_item_rafaksi_output.toString()).number(true, 2, ',', '.');
            }
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }else{
            $('#barang-nominal_ppn_item_data-'+id_barang).val(0);
            $('#barang-nominal_ppn_item-'+id_barang).val(0);
            var sub_total_harga_end = sub_total_harga;
            $('#barang-sub_total_harga-'+id_barang).val(sub_total_harga_end.toString()).number(true, 2, ',', '.');
        }
        SetTotalBeli();
    }
    function SetTotalBeli(){
        var total_beli = 0;
        $.each($('.data_hitung_sub_total_harga'), function() {
            total_beli += toFloat(number_value($(this).val()));
        });
        $('#barang-total_beli').val(total_beli.toString()).number(true, 2, ',', '.');
        SetPPNHeader();
        SetTotalBayar();
    }
    
    function SetPPNHeader(){
        var ppn_header = 0;
        $.each($('.data_hitung_nominal_ppn_item_data'), function() {
            ppn_header += toFloat(number_value($(this).val()));
        });
        $('#barang-nominal_ppn_header').val(ppn_header.toString()).number(true, 2, ',', '.');
    }
    
    function SetTotalBayar(){
        var total_beli = toFloat(number_value($('#barang-total_beli').val()));
        var ppn_header = toFloat(number_value($('#barang-nominal_ppn_header').val()));
        var diskon = toFloat(number_value($('#barang-diskon').val()));
        var potongan = $('#barang-potongan').val();
        var harga_potongan = number_value(potongan);
        diskon = total_beli*(diskon/100);
        var total_bayar = total_beli-diskon-harga_potongan;
        $('#barang-total_bayar').val(total_bayar.toString()).number(true, 2, ',', '.');
    }
    function ChangeSelectBarang(){
        var id_barang = $('#form-add-barang-id_barang').val();
        $.ajax({
            url : '<?= base_url() ?>pembelian/cari_barang/'+id_barang,
            type : 'post',
            dataType : 'json',
            success:function(response){
                $('#form-add-barang-id_barang').val(response['id_barang']);
                $('#form-add-barang-nama_barang').val(response['nama_barang']);
                $('#form-add-barang-id_satuan').val(response['id_satuan']);
                $('#form-add-barang-nama_satuan').val(response['nama_satuan']);
                $('#form-add-barang-harga').val(formatRupiah(response['harga']));
                $('#form-add-barang-ppn_item').val(response['ppn']);
                if(response['ppn']==1){
                    $('#form-add-barang-ppn_item').prop('checked', true);
                }else{
                    $('#form-add-barang-ppn_item').prop('checked', false);
                }
                $('#form-add-barang-nominal_ppn_item').val(formatRupiah(response['nominal_ppn']));
            }
        });
    }
    function addBarang(){
        var id_barang = $('#form-add-barang-id_barang').val();
        var nama_barang = $('#form-add-barang-nama_barang').val();
        var nama_satuan = $('#form-add-barang-nama_satuan').val();
        var qty = $('#form-add-barang-qty').val();
        var harga = $('#form-add-barang-harga').val();
        var ppn_item = $('#form-add-barang-ppn_item').is(':checked') ? 1 : 0;
        var nominal_ppn_item = $('#form-add-barang-nominal_ppn_item').val();
        var nominal_ppn_item_data = $('#form-add-barang-nominal_ppn_item_data').val();
        var rafaksi = $('#form-add-barang-rafaksi').val();
        var qty_rafaksi = $('#form-add-barang-qty_rafaksi').val();
        var nominal_qty_rafaksi = $('#form-add-barang-nominal_qty_rafaksi').val();
        var harga_rafaksi = $('#form-add-barang-harga_rafaksi').val();
        var nominal_harga_rafaksi = $('#form-add-barang-nominal_harga_rafaksi').val();
        var sub_total_harga = $('#form-add-barang-sub_total_harga').val();
        if (id_barang == '') {
            alert('ID Barang belum diisi !');
            return false;
        }
        if (nama_satuan == '') {
            alert('Satuan belum diisi !');
            return false;
        }
        if (qty == '') {
            alert('Qty belum diisi !');
            return false;
        }
        if (harga == '') {
            alert('Harga belum diisi !');
            return false;
        }
        if (nominal_ppn_item == '') {
            return 0;
        }
        if ($('tr[data-row-id="'+id_barang+'"]').length == 0) {
            var html_row = '<tr data-row-id="'+id_barang+'">';
                html_row += '<td><input type="hidden" class="form-control" name="barang['+id_barang+'][id_barang]" value="'+id_barang+'" id="barang-id_barang-'+id_barang+'" readonly><input type="text" class="form-control" name="barang['+id_barang+'][nama_barang]" value="'+nama_barang+'" id="barang-nama_barang-'+nama_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control" name="barang['+id_barang+'][nama_satuan]" value="'+nama_satuan+'" id="barang-nama_satuan-'+id_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="barang['+id_barang+'][qty]" value="'+qty+'" id="barang-qty-'+id_barang+'" onkeyup="SubTotal(\''+id_barang+'\')"></td>';
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right" name="barang['+id_barang+'][harga]" value="'+harga+'" id="barang-harga-'+id_barang+'" onkeyup="hargaBarang(\''+id_barang+'\')"></div></td>';
                html_row += '<td><div class="input-group"><input type="text" class="form-control text-center" name="barang['+id_barang+'][rafaksi]" value="'+rafaksi+'" id="barang-rafaksi-'+id_barang+'" onkeyup="hargaRafaksi(\''+id_barang+'\')" placeholder="Rafaksi"><span class="input-group-addon">%</span><input type="text" class="form-control text-center" name="barang['+id_barang+'][nominal_qty_rafaksi]" value="'+nominal_qty_rafaksi+'" id="barang-nominal_qty_rafaksi-'+id_barang+'" readonly></div><input type="hidden" name="barang['+id_barang+'][qty_rafaksi]" value="'+qty_rafaksi+'" id="barang-qty_rafaksi-'+id_barang+'"><input type="hidden" name="barang['+id_barang+'][harga_rafaksi]" value="'+harga_rafaksi+'" id="barang-harga_rafaksi-'+id_barang+'"></td>';
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right" name="barang['+id_barang+'][nominal_harga_rafaksi]" value="'+nominal_harga_rafaksi+'" id="barang-nominal_harga_rafaksi-'+id_barang+'" placeholder="Harga Rafaksi" readonly></div></td>'
                if (ppn_item == 1) {
                    html_row += '<td><label><input type="checkbox" class="flat-red" name="barang['+id_barang+'][ppn_item]" value="'+ppn_item+'" id="barang-ppn_item-'+id_barang+'" onchange="hargaPPN(\''+id_barang+'\')" checked> 10 %</label></td>';
                } else {
                    html_row += '<td><label><input type="checkbox" class="flat-red" name="barang['+id_barang+'][ppn_item]" value="'+ppn_item+'" id="barang-ppn_item-'+id_barang+'" onchange="hargaPPN(\''+id_barang+'\')"> 10 %</label></td>';
                }
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right" name="barang['+id_barang+'][nominal_ppn_item]" value="'+nominal_ppn_item+'" id="barang-nominal_ppn_item-'+id_barang+'" readonly><input type="hidden" class="form-control text-right data_hitung_nominal_ppn_item_data" name="barang['+id_barang+'][nominal_ppn_item_data]" value="'+nominal_ppn_item_data+'" id="barang-nominal_ppn_item_data-'+id_barang+'" readonly></div></td>';
                html_row += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control text-right data_hitung_sub_total_harga" name="barang['+id_barang+'][sub_total_harga]" value="'+sub_total_harga+'" id="barang-sub_total_harga-'+id_barang+'" readonly></div></td>';
                html_row += '<td><button type="button" class="btn btn-danger btn-sm" onclick="removeBarang(\''+id_barang+'\')"><i class="fa fa-trash"></i></button></td>';
            html_row += '</tr>';
            $('#form-add-barang').before(html_row);
            $('#barang-harga-'+id_barang).keyup(function() {
                $(this).val($(this).val()).number(true, 2, ',', '.');
            });
            $('#form-add-barang-id_barang').val('').change();
            $('#form-add-barang-nama_barang').val('');
            $('#form-add-barang-nama_satuan').val('');
            $('#form-add-barang-qty').val('');
            $('#form-add-barang-harga').val('');
            $('#form-add-barang-ppn_item').prop('checked', false);
            $('#form-add-barang-nominal_ppn_item').val(0);
            $('#form-add-barang-nominal_ppn_item_data').val(0);
            $('#form-add-barang-rafaksi').val('');
            // $('#form-add-barang-qty_rafaksi').val(0);
            $('#form-add-barang-nominal_qty_rafaksi').val(0);
            // $('#form-add-barang-harga_rafaksi').val(0);
            $('#form-add-barang-nominal_harga_rafaksi').val(0);
            $('#form-add-barang-sub_total_harga').val(0);
            // $('input[type="checkbox"].flat-red').iCheck({
            //     checkboxClass: 'icheckbox_flat-green'
            // });
            SetTotalBeli();
            SetPPNHeader();
            SetTotalBayar();
        } else {
            alert('Data barang sudah ada !');
        }
    }
    function removeBarang(id_barang) {
        $('#barang tbody tr[data-row-id="'+id_barang+'"]').remove();
    }

    // var rupiah = document.getElementById('barang-potongan');
    // // form-add-barang-harga, form-add-barang-nominal_ppn_item, form-add-barang-sub_total_harga, barang-total_beli, nominal_ppn_header, barang-diskon, barang-total_bayar
    // rupiah.addEventListener('keyup', function(e){
    //     rupiah.value = formatRupiah(this.value);
    // });
    //'#form-add-barang-harga', '#form-add-barang-nominal_ppn_item', '#form-add-barang-sub_total_harga', '#barang-total_beli', '#nominal_ppn_header', '#barang-diskon', '#barang-total_bayar'

    $('#barang-potongan, #form-add-barang-harga, #form-add-barang-nominal_ppn_item, #form-add-barang-harga_rafaksi, #form-add-barang-nominal_harga_rafaksi, #form-add-barang-sub_total_harga, #barang-total_beli, #nominal_ppn_header, #barang-diskon, #barang-total_bayar').keyup(function() {
        $(this).val(formatRupiah($(this).val()));
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Pembelian</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('pembelian/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Pembelian</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_pembelian" value="<?= $kode; ?>" placeholder="Masukkan Kode Pembelian" required readonly>
                        <input type="hidden" value="<?= $this->session->userdata('id_user'); ?>" name="id_user">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Transaksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_transaksi" placeholder="Masukkan Kode Transaksi">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Pembelian</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" name="tanggal_pembelian" placeholder="Masukkan Tanggal Pembelian" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Supplier</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_supplier" style="width: 100%;" required>
                            <option value="">- Pilih Supplier -</option>
                            <?php foreach($supplier as $select){ ?>
                                <option value="<?= $select->id_supplier ?>"><?= $select->id_supplier ?> - <?= $select->nama_supplier ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Cara Beli</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" style="width: 100%;" name="cara_beli" id="CaraPembelian" onchange="CaraBeli()" required>
                            <option value="" disabled diselected>- Pilih Cara Beli -</option>
                            <option value="1">Diantar</option>
                            <option value="2">Diambil</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_pengantar" id="pengantar" placeholder="Masukkan Nama Pengantar">
                        <span id="pengambil">
                            <select class="form-control select2" name="id_pengambil" style="width: 100%;">
                                <option value="">- Pilih Pengambil -</option>
                                <?php foreach($pegawai as $select){ ?>
                                    <option value="<?= $select->id_pegawai ?>"><?= $select->id_pegawai ?> - <?= $select->nama_pegawai ?></option>
                                <?php } ?>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <p><h4 class="blue">Daftar Barang</h4></p>
                <table class="table table-striped table-bordered" id="barang">
                    <thead class="thead-dark">
                        <tr>
                            <th>Barang</th>
                            <th width="100px">Satuan</th>
                            <th width="70px" class="text-center">Qty</th>
                            <th width="140px" class="text-right">Harga</th>
                            <th colspan="2" class="text-center">Rafaksi</th>
                            <th colspan="2" class="text-center">PPN</th>
                            <th width="150px" class="text-center">Sub Total</th>
                            <th width="1" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="form-add-barang">
                            <td>
                                <select class="form-control select2" id="form-add-barang-id_barang" onchange="ChangeSelectBarang()" style="width: 100%;">
                                    <option value="">- Pilih Barang -</option>
                                    <?php foreach($barang as $select){ ?>
                                        <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="nama_barang" id="form-add-barang-nama_barang" placeholder="Masukkan Nama Barang">
                            </td>
                            <td><input type="text" class="form-control" name="nama_satuan" id="form-add-barang-nama_satuan" placeholder="Satuan" required readonly></td>
                            <td><input type="text" class="form-control text-center" name="qty" id="form-add-barang-qty" placeholder="Qty"></td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="harga" id="form-add-barang-harga" placeholder="Harga">
                                </div>
                            </td>
                            <td width="160px">
                                <div class="input-group">
                                    <input type="text" class="form-control text-center" name="rafaksi" id="form-add-barang-rafaksi" placeholder="Rafaksi">
                                    <span class="input-group-addon">%</span>
                                    <input type="text" class="form-control text-center" name="nominal_qty_rafaksi" id="form-add-barang-nominal_qty_rafaksi" placeholder="Qty Rafaksi" readonly>
                                </div>
                                <input type="hidden" name="qty_rafaksi" id="form-add-barang-qty_rafaksi">
                                <!-- <input type="hidden" name="nominal_qty_rafaksi" id="form-add-barang-nominal_qty_rafaksi"> -->
                                <input type="hidden" name="harga_rafaksi" id="form-add-barang-harga_rafaksi">
                                <!-- <input type="hidden" name="nominal_harga_rafaksi" id="form-add-barang-nominal_harga_rafaksi"> -->
                            </td>
                            <td width="130px">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="nominal_harga_rafaksi" id="form-add-barang-nominal_harga_rafaksi" placeholder="Harga Rafaksi" readonly>
                                </div>
                            </td>
                            <td width="70px">
                                <label><input type="checkbox" class="flat-red" name="ppn" id="form-add-barang-ppn_item" value="1"> 10 %</label>
                            </td>
                            <td width="130px">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="nominal_ppn" id="form-add-barang-nominal_ppn_item" placeholder="Nominal PPN Item" required readonly>
                                    <input type="hidden" class="form-control text-right" id="form-add-barang-nominal_ppn_item_data" placeholder="Nominal PPN Item" required readonly>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="sub_total_harga" id="form-add-barang-sub_total_harga" placeholder="Sub Total Harga" required readonly>
                                </div>
                            </td>
                            <td><button class="btn btn-sm btn-primary" type="button" name="add_barang" onclick="addBarang()"><i class="fa fa-plus"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Beli</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="total_beli" id="barang-total_beli" placeholder="Masukkan Total Beli" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn_header" id="barang-ppn_header" value="1"> 10 %
                        </label>
                    </div>
                    <div class="col-sm-9" id="txtNominal">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn_header" id="barang-nominal_ppn_header" placeholder="Nominal PPN" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Diskon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control text-right" name="diskon" id="barang-diskon" placeholder="Masukkan Diskon">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Potongan</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="potongan" id="barang-potongan" placeholder="Masukkan Potongan">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Bayar</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control price text-right" value="" name="total_bayar" id="barang-total_bayar" placeholder="0" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Pembayaran</label>
                    <div class="col-sm-5">
                        <select class="form-control" style="width: 100%;" name="jenis_pembayaran" id="jenis_pembayaran" onchange="JenisPembayaran()" required>
                            <option value="">- Pilih Jenis Pembayaran -</option>
                            <option value="1">Tunai</option>
                            <option value="2">Non Tunai</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="non_tunai" id="non_tunai" placeholder="Masukkan Bank">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data</button>
                        <!-- <input type="submit" value="simpan" placeholder=""> -->
                        <a href="<?= base_url('pembelian/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
            <!-- <input type="text" name="total_bayar" value="90000" placeholder="">
            <input type="submit" value="simpan" placeholder=""> -->
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>