<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nota Pembelian</title>
    <style>
        body{
            width: 5cm;
            margin: 1mm 1mm 1mm 1mm;
            font-family: Courier;
        }
        .selling-title{
            font-size: 13px;
        }
        .selling-table{
            font-size: 11px;
        }
        .selling-detail-table{
            font-size: 10px;
        }
        @media print{
            body{
                width: 5cm;
                margin: 1mm 1mm 1mm 1mm;
                font-family: Courier;
            }
            .selling-detail-table{
                font-size: 10px;
            }
        }
    </style>
</head>
<body>
    <h1 class="selling-title" style="font-weight: bold;"><u>UD. Mitra Tani</u></h1>
    <table class="selling-table" style="padding: 0px; margin: 0px;" border="0">
        <?php
            foreach($data_pembelian as $data){
                if($data->cara_beli==1){
                    $cara_beli = 'Diantar';
                }else{
                    $cara_beli = 'Diambil';
                }
        ?>
        <tr>
            <td colspan="3"><?= $data->id_pembelian ?> | <?= date("d-m-Y", strtotime($data->tanggal_pembelian)) ?> | Kasir : <?= $data->username ?></td>
        </tr>
        <!-- <tr>
            <td>Kode Pembelian</td>
            <td>:</td>
            <td><?= $data->id_pembelian ?></td>
        </tr> -->
        <!-- <tr>
            <td>Kode Transaksi</td>
            <td>:</td>
            <td><?= $data->id_transaksi ?></td>
            <td>Cara Jual</td>
            <td>:</td>
            <td><?= $cara_beli ?> ( <?php if($data->cara_beli==2){ echo $data->id_pengambil; echo ' - '; echo $data->nama_pegawai; }else{ echo $data->nama_pengantar; } ?> )</td>
        </tr> -->
        <!-- <tr>
            <td>Tanggal Pembelian</td>
            <td>:</td>
            <td><?= date("d-m-Y", strtotime($data->tanggal_pembelian)) ?></td>
        </tr> -->
        <!-- <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?= $data->nama_supplier ?></td>
        </tr> -->
        <!-- <tr>
            <td>Kasir</td>
            <td>:</td>
            <td><?= $data->username ?></td>
        </tr> -->
        <?php } ?>
    </table>
    <span style="font-size: 5px;">----------------------------------------------------------------</span>
    <table class="selling-detail-table" style="border: 0px solid #000; border-spacing: 0px; padding: 0px; margin: 0px;">
        <tr>
            <!-- <td style="border: 1px solid #000; padding: 1px 3px;">No</td> -->
            <td style="border: 0px solid #000; padding: 1px 3px;">Barang</td>
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: center;">Qty</td>
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;">Harga</td>
            <!-- <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;">PPN</td> -->
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;">Sub Total</td>
        </tr>
        <?php
            // $no = 1;
            foreach($data_detail_pembelian as $data_detail){
                if($data_detail->ppn_item==0){
                    $data_detail->ppn_item = 'Tidak Ada';
                }else{
                    $data_detail->ppn_item = 'Ada';
                }
        ?>
        <tr>
            <!-- <td style="border: 1px solid #000; padding: 1px 3px;"><?= $no++;?></td> -->
            <td style="border: 0px solid #000; padding: 1px 3px;"><?= $data_detail->nama_barang ?></td>
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: center;"><?= $data_detail->qty ?> <!-- <?= $data_detail->nama_satuan ?> --></td>
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;"><?= rupiah_nota($data_detail->harga) ?></td>
            <!-- <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;"><?= rupiah_nota($data_detail->nominal_ppn_item) ?></td> -->
            <td style="border: 0px solid #000; padding: 1px 3px; text-align: right;"><?= rupiah_nota($data_detail->sub_total_harga) ?></td>
        </tr>
        <?php } ?>
    </table>
    <span style="font-size: 5px;">----------------------------------------------------------------</span>
    <table class="selling-table" style="padding: 0px; margin: 0px;" border="0">
        <?php
            foreach($data_pembelian as $data){
                if($data->ppn_header==1){
                    $ppn_header = 'Ada';
                }else{
                    $ppn_header = 'Tidak Ada';
                }

                if($data->jenis_pembayaran==1){
                    $jenis_pembayaran = 'Tunai';
                }else{
                    $jenis_pembayaran = 'Non Tunai';
                }
        ?>
        <tr>
            <td>Total Jual</td>
            <td>:</td>
            <td><?= rupiah_nota($data->total_beli) ?></td>
        </tr>
        <tr>
            <td>PPN</td>
            <td>:</td>
            <td><?= rupiah_nota($data->nominal_ppn_header) ?></td>
            <!-- <td>PPN</td>
            <td>:</td>
            <td><?= $ppn_header ?></td> -->
            <!-- <td>Jenis Pembayaran</td>
            <td>:</td>
            <td><?= $jenis_pembayaran ?> <?php if($data->jenis_pembayaran==2){ echo ' - '; echo $data->bank; } ?></td> -->
        </tr>
        <tr>
            <td>Potongan</td>
            <td>:</td>
            <td><?= rupiah_nota($data->potongan) ?></td>
        </tr>
        <tr>
            <td>Diskon</td>
            <td>:</td>
            <td><?= $data->total_diskon ?> %</td>
        </tr>
        <tr>
            <td>Total Bayar</td>
            <td>:</td>
            <td><?= rupiah_nota($data->total_bayar) ?></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>