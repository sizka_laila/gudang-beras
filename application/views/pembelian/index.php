<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    function confirmDialog(url) {
        $('#btn-yes').attr('href',url);
    }
    $(document).ready(function () {
        $('#datatable').DataTable();
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Pembelian</h1>
</section>
<section class="content">
    <?= $this->message->show('pembelian') ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <a href="<?= base_url('pembelian/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th width="60">Tanggal</th>
                        <th>Kode Pembelian</th>
                        <th>Supplier</th>
                        <th class="text-right">Total Bayar</th>
                        <th class="text-center">Status Pembelian</th>
                        <th width="200" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        foreach($pembelian as $data){
                            if($data->status_pembelian==1){
                                $data->status_pembelian = 'Aktif';
                                $label_status_pembelian = 'label-success';
                                $disable = '';
                            }else{
                                $data->status_pembelian = 'Dibatalkan';
                                $label_status_pembelian = 'label-danger';
                                $disable = 'disabled';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= mediumdate($data->tanggal_pembelian) ?></td>
                        <td><?= $data->id_pembelian ?></td>
                        <td><?= $data->id_supplier ?> - <?= $data->nama_supplier ?></td>
                        <td class="text-right"><?= rupiah($data->total_bayar) ?></td>
                        <td class="text-center"><label class="label <?= $label_status_pembelian ?>"><?= $data->status_pembelian ?></label></td>
                        <td class="text-center">
                            <a href="<?= base_url('pembelian/nota/'.$data->id_pembelian) ?>" class="btn btn-default btn-sm"><i class="fa fa-download"></i> Nota</a>
                            <a href="<?= base_url('pembelian/detail/'.$data->id_pembelian) ?>" class="btn btn-info btn-sm"><i class="fa fa-file-text"></i> Detail</a>
                            <a href="#HapusData" class="btn btn-danger btn-sm" onclick="confirmDialog('<?= base_url('pembelian/batal/'.$data->id_pembelian) ?>')" data-toggle="modal" <?= $disable ?>><i class="fa fa-flag"></i> Batalkan</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<div class="modal modal-default" id="HapusData" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Batalkan Data Pembelian</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>membatalkan</b> data pembelian ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" id="btn-yes"><i class="fa fa-check"></i> Ya</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>
