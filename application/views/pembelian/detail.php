<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        // $('#pengantar').show();
        // $('#pengambil').hide();
    });
    // function CaraBeli(){
    //     var value = $('#CaraPembelian').val();
    //     if(value=='1'){
    //         $('#pengantar').show();
    //         $('#pengambil').hide();
    //     }else{
    //         $('#pengantar').hide();
    //         $('#pengambil').show();
    //     }
    // }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Pembelian</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal">
            <?php foreach($data_pembelian as $data){ ?>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Pembelian</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_pembelian" value="<?= $data->id_pembelian ?>" placeholder="Masukkan Kode Pembelian" required disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Transaksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_transaksi" value="<?= $data->id_transaksi ?>" placeholder="Masukkan Kode Transaksi" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Pembelian</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" name="tanggal_pembelian" value="<?= shortdate($data->tanggal_pembelian) ?>" placeholder="Masukkan Tanggal Pembelian" required disabled>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Supplier</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_supplier" style="width: 100%;" required disabled>
                            <option value="">- Pilih Supplier -</option>
                            <?php
                                $SelectSupplier = $data->id_supplier;
                                foreach($this->db->get('supplier')->result() as $supplier){
                            ?>
                                <option value="<?= $supplier->id_supplier ?>"
                                    <?php
                                        if($SelectSupplier==$supplier->id_supplier){
                                            echo "selected";
                                        }
                                    ?>
                                ><?= $supplier->id_supplier ?> - <?= $supplier->nama_supplier ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Cara Beli</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" style="width: 100%;" name="cara_beli" id="CaraPembelian" onchange="CaraBeli()" required disabled>
                            <option value="" disabled diselected>- Pilih Cara Beli -</option>
                            <option value="1"
                                <?php
                                    if($data->cara_beli==1){
                                        echo "selected";
                                    }
                                ?>
                            >Diantar</option>
                            <option value="2"
                                <?php
                                    if($data->cara_beli==2){
                                        echo "selected";
                                    }
                                ?>
                            >Diambil</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <!-- <span id="pengantar">
                            <select class="form-control select2" name="id_pengantar" style="width: 100%;" disabled>
                                <option value="">- Pilih Pengantar -</option>
                                <?php
                                    $SelectPegawai = $data->id_pengantar;
                                    foreach($this->db->get('pegawai')->result() as $pegawai){
                                ?>
                                    <option value="<?= $pegawai->id_pegawai ?>"
                                        <?php
                                            if($SelectPegawai==$pegawai->id_pegawai){
                                                echo "selected";
                                            }
                                        ?>
                                    ><?= $pegawai->id_pegawai ?> - <?= $pegawai->nama_pegawai ?></option>
                                <?php } ?>
                            </select>
                        </span> -->
                        <input type="text" class="form-control" name="nama_pengambil" value="<?php if($data->cara_beli==2){ echo $pegawai->id_pegawai; echo ' - '; echo $data->nama_pegawai; }else{ echo $data->nama_pengantar; } ?>" placeholder="Masukkan Nama Pengantar" disabled>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="box-footer">
                <p><h4 class="blue">Daftar Barang</h4></p>
                <table class="table table-striped table-bordered" id="barang">
                    <thead class="thead-dark">
                        <tr>
                            <th>Barang</th>
                            <th width="100px">Satuan</th>
                            <th width="80px" class="text-center">Qty</th>
                            <th width="160px" class="text-right">Harga</th>
                            <th colspan="2" class="text-center">Rafaksi</th>
                            <th colspan="2" class="text-center">PPN</th>
                            <th width="170px" class="text-center">Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data_detail_pembelian as $data_detail){ ?>
                        <tr id="form-add-barang">
                            <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                    <option value="">- Pilih Barang -</option>
                                    <?php
                                        $SelectBarang = $data_detail->id_barang;
                                        foreach($this->db->get('barang')->result() as $barang){
                                    ?>
                                        <option value="<?= $barang->id_barang ?>"
                                            <?php
                                            if($SelectBarang==$barang->id_barang){
                                                echo "selected";
                                            }
                                        ?>
                                        ><!-- <?= $barang->id_barang ?> -  --><?= $barang->nama_barang ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="nama_barang" id="form-add-barang-nama_barang" placeholder="Masukkan Nama Barang" disabled>
                            </td>
                            <td><input type="text" class="form-control" name="nama_satuan" value="<?= $data_detail->nama_satuan ?>" placeholder="Satuan" required disabled></td>
                            <td><input type="text" class="form-control text-center" name="qty" id="form-add-barang-qty" value="<?= $data_detail->qty ?>" placeholder="Qty" disabled></td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="harga" id="form-add-barang-harga" value="<?= rupiah_format($data_detail->harga) ?>" placeholder="Harga" disabled>
                                </div>
                            </td>
                            <td width="160px">
                                <div class="input-group">
                                    <input type="text" class="form-control text-center" name="rafaksi" id="form-add-barang-rafaksi" value="<?= $data_detail->rafaksi ?>" placeholder="Rafaksi" disabled>
                                    <span class="input-group-addon">%</span>
                                    <input type="text" class="form-control text-center" name="nominal_qty_rafaksi" id="form-add-barang-nominal_qty_rafaksi" value="<?= $data_detail->nominal_qty_rafaksi ?>" placeholder="Qty Rafaksi" disabled>
                                </div>
                            </td>
                            <td width="130px">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="nominal_harga_rafaksi" id="form-add-barang-nominal_harga_rafaksi" value="<?= rupiah_format($data_detail->nominal_harga_rafaksi) ?>" placeholder="Harga Rafaksi" disabled>
                                </div>
                            </td>
                            <td width="70px">
                                <label><input type="checkbox" class="flat-red" name="ppn" id="form-add-barang-ppn_item" value="1" disabled
                                    <?php
                                        if($data_detail->ppn_item==1){
                                            echo "checked";
                                        }
                                    ?>
                                > 10 %</label>
                            </td>
                            <td width="130px">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="nominal_ppn" id="form-add-barang-nominal_ppn_item" value="<?= rupiah_format($data_detail->nominal_ppn_item) ?>" placeholder="Nominal PPN Item" required disabled>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control text-right" name="sub_total_harga" id="form-add-barang-sub_total_harga" value="<?= rupiah_format($data_detail->sub_total_harga) ?>" placeholder="Sub Total Harga" required disabled>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <?php foreach($data_pembelian as $data){ ?>
            <div class="box-footer">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Beli</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="total_beli" value="<?= rupiah_format($data->total_beli) ?>" placeholder="Masukkan Total Beli" required disabled>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Diskon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control text-right" name="diskon" value="<?= $data->total_diskon ?>" placeholder="Masukkan Diskon" disabled>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn_header" value="1" disabled
                                <?php
                                    if($data->ppn_header==1){
                                        echo "checked";
                                    }
                                ?>
                            > 10 %
                        </label>
                    </div>
                    <div class="col-sm-9" id="txtNominal">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn_header" value="<?= rupiah_format($data->nominal_ppn_header) ?>" placeholder="Nominal PPN" disabled>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Potongan</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="potongan" value="<?= rupiah_format($data->potongan) ?>" placeholder="Masukkan Potongan" disabled>
                        </div>
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Bayar</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control price text-right" name="total_bayar" value="<?= rupiah_format($data->total_bayar) ?>" placeholder="Masukkan Total Bayar" required disabled>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Pembayaran</label>
                    <div class="col-sm-5">
                        <select class="form-control" style="width: 100%;" name="jenis_pembayaran" required disabled>
                            <option value="">- Pilih Jenis Pembayaran -</option>
                            <option value="1"
                                <?php
                                    if($data->jenis_pembayaran==1){
                                        echo "selected";
                                    }
                                ?>
                            >Tunai</option>
                            <option value="2"
                                <?php
                                    if($data->jenis_pembayaran==2){
                                        echo "selected";
                                    }
                                ?>
                            >Non Tunai</option>
                            <option value="3"
                                <?php
                                    if($data->jenis_pembayaran==3){
                                        echo "selected";
                                    }
                                ?>
                            >Hutang</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <?php
                            if($data->jenis_pembayaran==1){
                                $bank = '';
                            }else if($data->jenis_pembayaran==2){
                                $bank = $data->bank;
                            }else{
                                $bank = '';
                            }
                        ?>
                        <input type="text" class="form-control" name="non_tunai" value="<?= $bank ?>" placeholder="Masukkan Bank" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="<?= base_url('pembelian/index') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>