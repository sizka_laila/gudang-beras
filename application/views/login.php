<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UD. Mitra Tani | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugin/bootstrap-4.3.1/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/plugin/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <link rel="stylesheet" href="<?= base_url('public/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/square/blue.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/css/style.css') ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-box-body">
            <center><div class="login-logo-img"><img class="login-img img-responsive" src="<?= base_url('public/img/logo/user.png') ?>" alt=""></div></center>
            <p class="login-box-msg">
                <div class="login-logo">
                    <a href="<?=base_url()?>">UD.<b class="login-logo-bold"> Mitra</b> Tani</a>
                </div>
            </p>
            <?= $this->session->flashdata('gagal'); ?>
            <form action="<?= base_url('login/aksi_login'); ?>" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-unlock-alt"></i> Sign In</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row mt-5">
            <div class="col-sm-12 mt-5 text-center">
                <span class="copyright">Copyright &copy; 2019</span>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?= base_url('public/js/jquery-3.3.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/plugin/bootstrap-4.3.1/js/bootstrap.min.js') ?>"></script>
</body>
</html>