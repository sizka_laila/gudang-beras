<?php section('css') ?>
<?php endsection() ?>

<?php section('js') ?>
<?php endsection() ?>

<?php section('custom_js') ?>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Supplier</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <?php foreach($supplier as $data){ ?>
        <form class="form-horizontal" action="<?= base_url('supplier/ubah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Supplier</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_supplier" value="<?= $data->id_supplier ?>" placeholder="Masukkan ID Supplier" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Supplier</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_supplier" value="<?= $data->nama_supplier ?>" placeholder="Masukkan Nama Supplier" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat" required><?= $data->alamat ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="telephone" value="<?= $data->telephone ?>" placeholder="Masukkan Telephone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" value="<?= $data->email ?>" placeholder="Masukkan E-mail" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('supplier/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>