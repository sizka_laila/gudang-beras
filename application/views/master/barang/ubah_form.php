<?php section('css') ?>
<!-- <link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>"> -->
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script> -->
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        // $('input[type="checkbox"].flat-red').iCheck({
        //     checkboxClass: 'icheckbox_flat-green'
        // });
        $('#txtHargaBeli').number(true, 0, ',', '.');
        $('#txtHargaJual').number(true, 0, ',', '.');
        $('#txtNominalPpnBeli').number(true, 0, ',', '.');
        $('#txtNominalPpnJual').number(true, 0, ',', '.');
        $('#txtHargaBeli').keyup(function(){
            if($('#txtPpnBeli').is(':checked')){
                var harga = toFloat(number_value($('#txtHargaBeli').val()));
                var nominal_ppn = harga*10/100;
                $('#txtNominalPpnBeli').val(nominal_ppn.toString()).number(true, 0, ',', '.');
            }else{
                $('#txtNominalPpnBeli').val(0);
            }
        });
        $('#txtPpnBeli').change(function(event){
            if($(this).is(':checked')){
                var harga = toFloat(number_value($('#txtHargaBeli').val()));
                var nominal_ppn = harga*10/100;
                $('#txtNominalPpnBeli').val(nominal_ppn.toString()).number(true, 0, ',', '.');
            }else{
                $('#txtNominalPpnBeli').val(0);
            }
        });
        $('#txtHargaJual').keyup(function(){
            if($('#txtPpnJual').is(':checked')){
                var harga = toFloat(number_value($('#txtHargaJual').val()));
                var nominal_ppn = harga*10/100;
                $('#txtNominalPpnJual').val(nominal_ppn.toString()).number(true, 0, ',', '.');
            }else{
                $('#txtNominalPpnJual').val(0);
            }
        });
        $('#txtPpnJual').change(function(event){
            if($(this).is(':checked')){
                var harga = toFloat(number_value($('#txtHargaJual').val()));
                var nominal_ppn = harga*10/100;
                $('#txtNominalPpnJual').val(nominal_ppn.toString()).number(true, 0, ',', '.');
            }else{
                $('#txtNominalPpnJual').val(0);
            }
        });
    });

    function BlockInputHargaBeli() {
        $('#txtHargaBeli').select();
    }
    function BlockInputHargaJual() {
        $('#txtHargaJual').select();
    }
    function hargaPPNBeli(val){
        if($('#txtPpnBeli').prop('checked')){
            var harga = toFloat(number_value($('#txtHargaBeli').val()));
            var nominal_ppn = harga*10/100;
            $('#txtNominalPpnBeli').val(nominal_ppn.toString()).number(true, 0, ',', '.');
        }else{
            $('#txtNominalPpnBeli').val(0);
        }
    }
    function hargaPPNJual(val){
        if($('#txtPpnJual').prop('checked')){
            var harga = toFloat(number_value($('#txtHargaJual').val()));
            var nominal_ppn = harga*10/100;
            $('#txtNominalPpnJual').val(nominal_ppn.toString()).number(true, 0, ',', '.');
        }else{
            $('#txtNominalPpnJual').val(0);
        }
    }
    // $('#txtHargaBeli, #txtNominalPpnBeli, #txtHargaJual, #txtNominalPpnJual').keyup(function() {
    //     $(this).val(formatRupiah($(this).val()));
    // });
    // function formatRupiah(angka, prefix){
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //     split           = number_string.split(','),
    //     sisa            = split[0].length % 3,
    //     rupiah          = split[0].substr(0, sisa),
    //     ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
    //     if(ribuan){
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    // }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Barang</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <?php foreach($barang as $data){ ?>
        <form class="form-horizontal" action="<?= base_url('barang/ubah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Barang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_barang" value="<?= $data->id_barang ?>" placeholder="Masukkan ID Barang" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Barang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_barang" value="<?= $data->nama_barang ?>" placeholder="Masukkan Nama Barang" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Qty</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="qty" value="<?= $data->qty ?>" placeholder="Masukkan Qty" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Satuan</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_satuan" style="width: 100%;">
                            <option value="" disabled diselected>- Pilih Satuan -</option>
                            <?php
                                $SelectSatuan = $data->id_satuan;
                                foreach($this->db->get('satuan')->result() as $satuan){
                            ?>
                                <option value="<?= $satuan->id_satuan ?>"
                                    <?php
                                        if($SelectSatuan==$satuan->id_satuan){
                                            echo "selected";
                                        }
                                    ?>
                                ><?= $satuan->nama_satuan ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Barang</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="jenis_barang" style="width: 100%;">
                            <option value="" disabled diselected>- Pilih Jenis Barang -</option>
                            <option value="1"
                                <?php
                                    if($data->jenis_barang==1){
                                        echo "selected";
                                    }
                                ?>
                            >Bahan Baku</option>
                            <option value="2"
                                <?php
                                    if($data->jenis_barang==2){
                                        echo "selected";
                                    }
                                ?>
                            >Barang Jadi</option>
                            <option value="3"
                                <?php
                                    if($data->jenis_barang==3){
                                        echo "selected";
                                    }
                                ?>
                            >Barang Sisa</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Beli</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga" id="txtHargaBeli" onclick="BlockInputHargaBeli()" value="<?= $data->harga ?>" placeholder="Masukkan Harga Beli" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN Beli</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn" id="txtPpnBeli" value="1" onchange="hargaPPNBeli(this.value)"
                                <?php
                                    if($data->ppn==1){
                                        echo "checked";
                                    }
                                ?>
                            > 10 %
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn" id="txtNominalPpnBeli" value="<?= $data->nominal_ppn ?>" placeholder="Nominal PPN Beli" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Jual</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga_jual" id="txtHargaJual" onclick="BlockInputHargaJual()" value="<?= $data->harga_jual ?>" placeholder="Masukkan Harga Jual" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PPN Jual</label>
                    <div class="col-sm-1">
                        <label>
                            <input type="checkbox" class="flat-red" name="ppn_jual" id="txtPpnJual" value="1" onchange="hargaPPNJual(this.value)"
                                <?php
                                    if($data->ppn_jual==1){
                                        echo "checked";
                                    }
                                ?>
                            > 10 %
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="nominal_ppn_jual" id="txtNominalPpnJual" value="<?= $data->nominal_ppn_jual ?>" placeholder="Nominal PPN Jual" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('barang/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>