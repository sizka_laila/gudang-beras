<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    function confirmDialog(url) {
        $('#btn-yes').attr('href',url);
    }
    $(document).ready(function () {
        $('#datatable').DataTable();
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Barang</h1>
</section>
<section class="content">
    <?= $this->message->show('barang') ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <a href="<?= base_url('barang/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th>ID Barang</th>
                        <th>Nama Barang</th>
                        <th width="1" class="text-center">Qty</th>
                        <th>Satuan</th>
                        <th>Jenis Barang</th>
                        <th class="text-right">Harga Beli</th>
                        <th width="1" class="text-center">PPN Beli</th>
                        <th class="text-right">Harga Jual</th>
                        <th width="1" class="text-center">PPN Jual</th>
                        <th width="120" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        foreach($barang as $data){
                            if($data->ppn==1){
                                $data->ppn = 'Ada';
                                $label_ppn = 'label-success';
                                $disable = '';
                            }else{
                                $data->ppn = 'Tidak ada';
                                $label_ppn = 'label-danger';
                            }

                            if($data->ppn_jual==1){
                                $data->ppn_jual = 'Ada';
                                $label_ppn_jual = 'label-success';
                                $disable = '';
                            }else{
                                $data->ppn_jual = 'Tidak ada';
                                $label_ppn_jual = 'label-danger';
                            }

                            if($data->jenis_barang==1){
                                $data->jenis_barang = 'Bahan Baku';
                            }elseif($data->jenis_barang==2){
                                $data->jenis_barang = 'Barang Jadi';
                            }else{
                                $data->jenis_barang = 'Barang Sisa';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= $data->id_barang ?></td>
                        <td><?= $data->nama_barang ?></td>
                        <td class="text-center"><?= $data->qty ?></td>
                        <td><?= $data->nama_satuan ?></td>
                        <td><?= $data->jenis_barang ?></td>
                        <td class="text-right"><?= rupiah($data->harga) ?></td>
                        <td class="text-center"><label class="label <?= $label_ppn ?>"><?= $data->ppn ?></label></td>
                        <td class="text-right"><?= rupiah($data->harga_jual) ?></td>
                        <td class="text-center"><label class="label <?= $label_ppn_jual ?>"><?= $data->ppn_jual ?></label></td>
                        <td class="text-center">
                            <a href="<?= base_url('barang/edit/'.$data->id_barang) ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#HapusData" class="btn btn-danger btn-sm" onclick="confirmDialog('<?= base_url('barang/hapus/'.$data->id_barang) ?>')" data-toggle="modal"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<div class="modal modal-default" id="HapusData" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Hapus Data</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>menghapus</b> data ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" id="btn-yes"><i class="fa fa-check"></i> Ya</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>
