<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $(".select2").select2();
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>User Management</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('user/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Pegawai</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_pegawai" style="width: 100%;">
                            <option value="" disabled diselected>- Pilih Pegawai -</option>
                            <?php foreach($pegawai as $select){ ?>
                                <option value="<?= $select->id_pegawai ?>"><?= $select->id_pegawai ?> - <?= $select->nama_pegawai ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Hak Akses</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="hak_akses" required>
                            <option value="" disabled diselected>- Pilih -</option>
                            <option value="1">Bagian Operasional</option>
                            <option value="2">Bagian Keuangan</option>
                            <option value="3">General Manager</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" placeholder="Masukkan Username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Masukkan Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <label>
                            <input type="checkbox" class="flat-red" name="status" value="1"> Aktifkan User
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('user/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>