<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
            if (password != confirmPassword) {
                var outputConfirm = document.getElementById("forConfirmPassword");
                outputConfirm.innerHTML = "Konfirmasi Password yang Anda masukkan tidak sesuai dengan Password !";
                // alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Profil User</h1>
</section>
<section class="content">
    <?= $this->message->show('user') ?>
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <?php
            foreach($user as $data){
                if($data->hak_akses==1){
                    $data->hak_akses = 'Bagian Operasional';
                }else if($data->hak_akses==2){
                    $data->hak_akses = 'Bagian Keuangan';
                }else{
                    $data->hak_akses = 'General Manager';
                }

                if($data->status==1){
                    $status = 'btn-info';
                    $label_status = 'Aktif';
                }else{
                    $status = 'btn-danger';
                    $label_status = 'Non-aktif';
                }
        ?>
        <form class="form-horizontal" action="<?= base_url('user/ubah_profil') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Pegawai</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="id_user" value="<?= $data->id_user ?>">
                        <input type="text" class="form-control" name="id_pegawai" value="<?= $data->id_pegawai ?>" placeholder="Masukkan ID Pegawai" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Pegawai</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_pegawai" value="<?= $data->nama_pegawai ?>" placeholder="Masukkan Nama Pegawai" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="jenis_kelamin" required readonly>
                            <option value="" disabled diselected>- Pilih -</option>
                            <option value="1"
                                <?php
                                    if($data->jenis_kelamin==1){
                                        echo "selected";
                                    }
                                ?>
                            >Laki-laki</option>
                            <option value="2"
                                <?php
                                    if($data->jenis_kelamin==2){
                                        echo "selected";
                                    }
                                ?>
                            >Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat" required readonly><?= $data->alamat ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="telephone" value="<?= $data->telephone ?>" placeholder="Masukkan Telephone" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" value="<?= $data->email ?>" placeholder="Masukkan E-mail" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Hak Akses</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="hak_akses" value="<?= $data->hak_akses ?>" placeholder="Masukkan Username" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" value="<?= $data->username ?>" placeholder="Masukkan Username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="txtPassword" placeholder="Masukkan Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Konfirmasi Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="txtConfirmPassword" placeholder="Masukkan Konfirmasi Password" required>
                        <small id="forConfirmPassword" class="red"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="status" value="<?= $data->status ?>">
                        <span class="label <?= $status; ?>"><?= $label_status ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit" id="btnSubmit"><i class="fa fa-check"></i> Simpan Profil</button>
                        <!-- <a href="<?= base_url('user/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a> -->
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>