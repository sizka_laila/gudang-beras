<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    function confirmStatus(id,id_user) {
        $('#UbahStatus').modal('show');
        $('#AktifStatus').attr('idStatus', id);
        $('#AktifStatus').attr('idUser', id_user);
    }
    function confirmDialog(url) {
        $('#btn-yes').attr('href',url);
    }
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('#AktifStatus').click(function(){
            var idStatus = $('#AktifStatus').attr('idStatus');
            var idUser = $('#AktifStatus').attr('idUser');
            if(idStatus==0){
                idStatus = 1
            }else{
                idStatus = 0
            }
            $.ajax({
                type : 'post',
                url : '<?= base_url('user/ubah_status/') ?>',
                data : {
                    idStatus : idStatus,
                    idUser : idUser
                },
                success : function(){
                    if(idStatus==0){
                        $('#UbahStatus').modal('hide');
                        $('#btn-status-'+idUser).removeClass('btn-info').addClass('btn-danger');
                        $('#btn-status-'+idUser).html('Non-aktif');
                    }else{
                        $('#UbahStatus').modal('hide');
                        $('#btn-status-'+idUser).removeClass('btn-danger').addClass('btn-info');
                        $('#btn-status-'+idUser).html('Aktif');
                    }
                }
            });
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>User Management</h1>
</section>
<section class="content">
    <?= $this->message->show('user') ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <a href="<?= base_url('user/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th>ID Pegawai</th>
                        <th>Nama Pegawai</th>
                        <th class="text-center">Hak Akses</th>
                        <th>Username</th>
                        <th width="1" class="text-center">Status</th>
                        <th width="120" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        foreach($user as $data){
                            if($data->hak_akses==1){
                                $hak_akses = 'label-primary';
                                $data->hak_akses = 'Bagian Operasional';
                            }else if($data->hak_akses==2){
                                $hak_akses = 'label-success';
                                $data->hak_akses = 'Bagian Keuangan';
                            }else{
                                $hak_akses = 'label-warning';
                                $data->hak_akses = 'General Manager';
                            }

                            if($data->status==1){
                                $status = 'btn-info';
                                $label_status = 'Aktif';
                            }else{
                                $status = 'btn-danger';
                                $label_status = 'Non-aktif';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= $data->id_pegawai ?></td>
                        <td><?= $data->nama_pegawai ?></td>
                        <td class="text-center"><span class="label <?= $hak_akses; ?>"><?= $data->hak_akses ?></span></td>
                        <td><?= $data->username ?></td>
                        <td class="text-center"><button type="button" class="btn <?= $status; ?> btn-xs" id="btn-status-<?= $data->id_user ?>" onclick="confirmStatus('<?= $data->status ?>','<?= $data->id_user ?>')"><?= $label_status ?></button></td>
                        <td class="text-center">
                            <a href="<?= base_url('user/edit/'.$data->id_user) ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#HapusData" class="btn btn-danger btn-sm" onclick="confirmDialog('<?= base_url('user/hapus/'.$data->id_user) ?>')" data-toggle="modal"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<div class="modal modal-default" id="UbahStatus" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Ubah Status</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>mengubah status</b> data ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" id="AktifStatus"><i class="fa fa-check"></i> Ya</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default" id="HapusData" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Hapus Data</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>menghapus</b> data ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" id="btn-yes"><i class="fa fa-check"></i> Ya</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>