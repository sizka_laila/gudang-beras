<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/iCheck/all.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/iCheck/icheck.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $(".select2").select2();
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>User Management</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <?php foreach($user as $data){ ?>
        <form class="form-horizontal" action="<?= base_url('user/ubah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Pegawai</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="id_user" value="<?= $data->id_user ?>">
                        <select class="form-control select2" name="id_pegawai" style="width: 100%;">
                            <option value="" disabled diselected>- Pilih Pegawai -</option>
                            <?php
                                $SelectPegawai = $data->id_pegawai;
                                foreach($this->db->get('pegawai')->result() as $pegawai){
                            ?>
                                <option value="<?= $pegawai->id_pegawai ?>"
                                    <?php
                                        if($SelectPegawai==$pegawai->id_pegawai){
                                            echo "selected";
                                        }
                                    ?>
                                ><?= $pegawai->id_pegawai ?> - <?= $pegawai->nama_pegawai ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Hak Akses</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="hak_akses" required>
                            <option value="" disabled diselected>- Pilih -</option>
                            <option value="1"
                                <?php
                                    if($data->hak_akses==1){
                                        echo "selected";
                                    }
                                ?>
                            >Bagian Operasional</option>
                            <option value="2"
                                <?php
                                    if($data->hak_akses==2){
                                        echo "selected";
                                    }
                                ?>
                            >Bagian Keuangan</option>
                            <option value="3"
                                <?php
                                    if($data->hak_akses==3){
                                        echo "selected";
                                    }
                                ?>
                            >General Manager</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" value="<?= $data->username ?>" placeholder="Masukkan Username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Masukkan Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <label>
                            <input type="checkbox" class="flat-red" name="status" value="1"
                                <?php
                                    if($data->status==1){
                                        echo "checked";
                                    }
                                ?>
                            > Aktifkan User
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('user/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>