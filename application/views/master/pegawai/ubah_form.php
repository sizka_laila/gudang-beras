<?php section('css') ?>
<?php endsection() ?>

<?php section('js') ?>
<?php endsection() ?>

<?php section('custom_js') ?>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Pegawai</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <?php foreach($pegawai as $data){ ?>
        <form class="form-horizontal" action="<?= base_url('pegawai/ubah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Pegawai</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_pegawai" value="<?= $data->id_pegawai ?>" placeholder="Masukkan ID Pegawai" required readonly>
                        <!-- <input type="text" class="form-control" name="id_pegawai" value="<?= $data->id_pegawai ?>" placeholder="Masukkan ID Pegawai" required disabled> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Pegawai</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_pegawai" value="<?= $data->nama_pegawai ?>" placeholder="Masukkan Nama Pegawai" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="jenis_kelamin" required>
                            <option value="" disabled diselected>- Pilih -</option>
                            <option value="1"
                                <?php
                                    if($data->jenis_kelamin==1){
                                        echo "selected";
                                    }
                                ?>
                            >Laki-laki</option>
                            <option value="2"
                                <?php
                                    if($data->jenis_kelamin==2){
                                        echo "selected";
                                    }
                                ?>
                            >Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat" required><?= $data->alamat ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="telephone" value="<?= $data->telephone ?>" placeholder="Masukkan Telephone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" value="<?= $data->email ?>" placeholder="Masukkan E-mail" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('pegawai/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>