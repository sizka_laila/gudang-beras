<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('#harga_produksi').number(true, 0, ',', '.');
        // $('#harga_produksi').keyup(function() {
        //     $(this).val(formatRupiah($(this).val()));
        // });
    });
    function BlockInputHargaProduksi() {
        $('#harga_produksi').select();
    }
    // function ChangeSelectBarang(){
    //     var selObj = document.getElementById("SelectBarang");
    //     var selValue = selObj.options[selObj.selectedIndex].value;
    //     var selText = selObj.options[selObj.selectedIndex].text;
    //     document.getElementById("nama_barang").value = selValue;
    // }
    // function formatRupiah(angka, prefix){
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //     split           = number_string.split(','),
    //     sisa            = split[0].length % 3,
    //     rupiah          = split[0].substr(0, sisa),
    //     ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
    //     if(ribuan){
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    // }
    function ChangeSelectBarang(){
        // var selObj = document.getElementById("SelectBarang");
        // var id_barang = selObj.options[selObj.selectedIndex].value;
        var id_barang = $('#form-add-resep-id_barang').val();
        $.ajax({
            url : '<?= base_url() ?>/bahan_baku/cari_barang/'+id_barang,
            type : 'post',
            dataType : 'json',
            success:function(response){
                $('#form-add-resep-id_barang').val(response['id_barang']);
                $('#form-add-resep-nama_barang').val(response['nama_barang']);
                $('#form-add-resep-id_satuan').val(response['id_satuan']);
                $('#form-add-resep-nama_satuan').val(response['nama_satuan']);
            }
        });
    }
    function addResep(){
        var id_barang = $('#form-add-resep-id_barang').val();
        var nama_barang = $('#form-add-resep-nama_barang').val();
        var nama_satuan = $('#form-add-resep-nama_satuan').val();
        var qty = $('#form-add-resep-qty').val();
        if (id_barang == '') {
            alert('ID Barang belum diisi !');
            return false;
        }
        if (nama_satuan == '') {
            alert('Satuan belum diisi !');
            return false;
        }
        if (qty == '') {
            alert('Qty belum diisi !');
            return false;
        }
        if ($('tr[data-row-id="'+id_barang+'"]').length == 0) {
            var html_row = '<tr data-row-id="'+id_barang+'">';
                html_row += '<td><input type="hidden" class="form-control" name="resep['+id_barang+'][id_barang]" value="'+id_barang+'" id="resep-id_barang-'+id_barang+'" readonly><input type="text" class="form-control" name="resep['+id_barang+'][nama_barang]" value="'+nama_barang+'" id="resep-nama_barang-'+nama_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="resep['+id_barang+'][nama_satuan]" value="'+nama_satuan+'" id="resep-nama_satuan-'+id_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="resep['+id_barang+'][qty]" value="'+qty+'" id="resep-qty-'+id_barang+'"></td>';
                html_row += '<td><button type="button" class="btn btn-danger btn-sm" onclick="removeResep(\''+id_barang+'\')"><i class="fa fa-trash"></i></button></td>';
            html_row += '</tr>';
            $('#form-add-resep').before(html_row);
            $('#form-add-resep-id_barang').val('').change();
            $('#form-add-resep-nama_barang').val('');
            $('#form-add-resep-nama_satuan').val('');
            $('#form-add-resep-qty').val('');
        } else {
            alert('Data barang sudah ada !');
        }
    }
    function removeResep(id_barang) {
        $('#reseps tbody tr[data-row-id="'+id_barang+'"]').remove();
    }
    // function addResep(){
    //     $.ajax({
    //         url : '<?= base_url() ?>/bahan_baku/addresep',
    //         type : 'post',
    //         dataType : 'json',
    //         data : 'id_barang='+$('input[name="id_barang"]').val(),
    //         beforeSend:function(){            
    //             $('button[name="add_resep"]').prop('disabled',true);
    //             $('button[name="add_resep"] i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
    //         },
    //         complete:function(){
    //             $('button[name="add_resep"]').prop('disabled',false);
    //             $('button[name="add_resep"] i').replaceWith('<i class="fa fa-plus-circle"></i>');
    //         },
    //         success:function(response){
    //             if(response['error']){
    //                 alert(response['error']);
    //             }
    //             if(response['message']){
    //                 alert(response['message']);
    //             }
    //             clearFormResep();
    //             getResep();
    //         }
    //     });
    // }
    // function getResep(){
    //     $.ajax({
    //         url : '<?= base_url() ?>/bahan_baku/getresep',
    //         type : 'post',
    //         dataType : 'json',
    //         success:function(response){
    //             var html='';
    //             if(response['reseps']){
    //                 $.each(response['reseps'],function(row,data){
    //                     html +='<tr id="resep-row-'+row+'" class="text-left">';
    //                         html+='<td class="text-left">'+data['id_barang']+'</td>';
    //                         html+='<td width="5%" class="text-center"><button type="button" name="remove_resep" onclick="removeResep('+row+')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button></td>';
    //                     html +='</tr>';
    //                 });
    //             }
    //             $('#reseps tbody').html(html);
    //         }
    //     });
    // }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Resep</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('bahan_baku/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Resep</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_bahan_baku" value="<?= $kode; ?>" placeholder="Masukkan Resep" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pilih Barang Produksi</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="barang_produksi" style="width: 100%;" required>
                            <option value="">- Pilih Barang Yang Diproduksi -</option>
                            <?php foreach($barang_produksi as $select){ ?>
                                <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <!-- <div class="col-sm-3">
                        <a href="<?= base_url('barang/tambah') ?>" class="btn btn-default" type="submit"><i class="fa fa-plus"></i> Tambah Data Barang Produksi Lain</a>
                    </div> -->
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga_produksi" onclick="BlockInputHargaProduksi()" id="harga_produksi" placeholder="Masukkan Harga Produksi" required>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Bahan Baku</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="id_barang" id="id_barang" placeholder="Masukkan Kode Bahan Baku" required readonly>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Masukkan Nama Bahan Baku" required readonly>
                    </div>
                </div> -->
            </div>
            <div class="box-footer">
                <p><h4 class="blue">Daftar Bahan Baku</h4></p>
                <table class="table table-striped table-bordered" id="reseps">
                    <thead>
                        <tr>
                            <th>Bahan Baku</th>
                            <th width="300px" class="text-center">Satuan</th>
                            <th width="200px" class="text-center">Qty</th>
                            <th width="1" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <?php if($this->session->flashdata('data_resep')){ ?>
                            <?php foreach($this->session->flashdata('data_resep') as $resep){ ?>
                                <tr data-row-id="<?= $resep['id_barang'] ?>">
                                    <td>
                                        <select class="form-control select2" id="resep-id_barang-'.$bahan_baku['id_barang'].'" name="resep['.$resep['id_barang'].'][id_barang]" style="width: 100%;" disabled>
                                            <option value="">- Pilih Bahan Baku -</option>
                                            <?php foreach($bahan_baku as $select){ ?>
                                                <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="resep['.$resep['nama_satuan'].'][nama_satuan]" id="resep-nama_satuan-'.$bahan_baku['nama_satuan'].'" placeholder="Masukkan Satuan" required readonly></td>
                                    <td><input type="text" class="form-control text-center" name="resep['.$resep['qty'].'][qty]" id="resep-qty-'.$bahan_baku['qty'].'" placeholder="Masukkan Qty" required></td>
                                    <td><button class="btn btn-sm btn-danger" type="button" onclick="removeResep(<?= $resep['id_barang'] ?>)"><i class="fa fa-trash"></i></button></td>
                                </tr>
                            <?php } ?>
                        <?php } ?> -->
                        <tr id="form-add-resep">
                            <td>
                                <select class="form-control select2" id="form-add-resep-id_barang" onchange="ChangeSelectBarang()" style="width: 100%;">
                                    <option value="">- Pilih Bahan Baku -</option>
                                    <?php foreach($bahan_baku as $select){ ?>
                                        <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="nama_barang" id="form-add-resep-nama_barang" placeholder="Masukkan Nama Barang">
                            </td>
                            <td><input type="text" class="form-control text-center" name="nama_satuan" id="form-add-resep-nama_satuan" placeholder="Masukkan Satuan" required readonly></td>
                            <td><input type="text" class="form-control text-center" name="qty" id="form-add-resep-qty" placeholder="Masukkan Qty"></td>
                            <td><button class="btn btn-sm btn-primary" type="button" name="add_resep" onclick="addResep()"><i class="fa fa-plus"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-12">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data</button>
                        <a href="<?= base_url('bahan_baku/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>