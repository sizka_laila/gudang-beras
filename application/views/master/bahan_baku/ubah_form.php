<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $(".select2").select2();
        $('#harga_produksi').number(true, 0, ',', '.');
        // $('#harga_produksi').keyup(function() {
        //     $(this).val(formatRupiah($(this).val()));
        // });
    });
    function BlockInputHargaProduksi() {
        $('#harga_produksi').select();
    }
    function ChangeSelectBarang(){
        var id_barang = $('#form-add-resep-id_barang_bahan_baku').val();
        $.ajax({
            url : '<?= base_url() ?>bahan_baku/cari_barang/'+id_barang,
            type : 'post',
            dataType : 'json',
            success:function(response){
                $('#form-add-resep-id_barang_bahan_baku').val(response['id_barang']);
                $('#form-add-resep-nama_barang').val(response['nama_barang']);
                $('#form-add-resep-id_satuan').val(response['id_satuan']);
                $('#form-add-resep-nama_satuan').val(response['nama_satuan']);
            }
        });
    }
    // function formatRupiah(angka, prefix){
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //     split           = number_string.split(','),
    //     sisa            = split[0].length % 3,
    //     rupiah          = split[0].substr(0, sisa),
    //     ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
    //     if(ribuan){
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    // }
    function addResep(){
        var id_barang_bahan_baku = $('#form-add-resep-id_barang_bahan_baku').val();
        var nama_barang = $('#form-add-resep-nama_barang').val();
        var nama_satuan = $('#form-add-resep-nama_satuan').val();
        var qty = $('#form-add-resep-qty').val();
        if (id_barang_bahan_baku == '') {
            alert('ID Barang belum diisi !');
            return false;
        }
        if (nama_satuan == '') {
            alert('Satuan belum diisi !');
            return false;
        }
        if (qty == '') {
            alert('Qty belum diisi !');
            return false;
        }
        if ($('tr[data-row-id="'+id_barang_bahan_baku+'"]').length == 0) {
            var html_row = '<tr data-row-id="'+id_barang_bahan_baku+'">';
                html_row += '<td><input type="hidden" class="form-control" name="resep['+id_barang_bahan_baku+'][id_barang_bahan_baku]" value="'+id_barang_bahan_baku+'" id="resep-id_barang_bahan_baku-'+id_barang_bahan_baku+'" readonly><input type="text" class="form-control" name="resep['+id_barang_bahan_baku+'][nama_barang]" value="'+nama_barang+'" id="resep-nama_barang-'+nama_barang+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="resep['+id_barang_bahan_baku+'][nama_satuan]" value="'+nama_satuan+'" id="resep-nama_satuan-'+id_barang_bahan_baku+'" readonly></td>';
                html_row += '<td><input type="text" class="form-control text-center" name="resep['+id_barang_bahan_baku+'][qty]" value="'+qty+'" id="resep-qty-'+id_barang_bahan_baku+'"></td>';
                html_row += '<td><button type="button" class="btn btn-danger btn-sm" onclick="removeResep(\''+id_barang_bahan_baku+'\')"><i class="fa fa-trash"></i></button></td>';
            html_row += '</tr>';
            $('#form-add-resep').before(html_row);
            $('#form-add-resep-id_barang_bahan_baku').val('').change();
            $('#form-add-resep-nama_barang').val('');
            $('#form-add-resep-nama_satuan').val('');
            $('#form-add-resep-qty').val('');
        } else {
            alert('Data barang sudah ada !');
        }
    }
    function removeResep(id_barang_bahan_baku) {
        $('#reseps tbody tr[data-row-id="'+id_barang_bahan_baku+'"]').remove();
    }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Resep</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('bahan_baku/ubah_aksi') ?>" method="post">
            <?php foreach($data_bahan_baku as $data){  ?>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Resep</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_bahan_baku" value="<?= $data->id_bahan_baku ?>" placeholder="Masukkan Kode Bahan Baku" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pilih Barang Produksi</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="barang_produksi" style="width: 100%;" required>
                            <option value="">- Pilih Barang Yang Diproduksi -</option>
                            <?php
                                $SelectBarang = $data->barang_produksi;
                                foreach($this->barang_m->barang_produksi()->result() as $barang){
                            ?>
                                <option value="<?= $barang->id_barang ?>"
                                    <?php
                                        if($SelectBarang==$barang->id_barang){
                                            echo "selected";
                                        }
                                    ?>
                                ><?= $barang->id_barang ?> - <?= $barang->nama_barang ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga_produksi" value="<?= $data->harga_produksi ?>" onclick="BlockInputHargaProduksi()" id="harga_produksi" placeholder="Masukkan Harga Produksi" required>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="box-footer">
                <p><h4 class="blue">Daftar Bahan Baku</h4></p>
                <table class="table table-striped table-bordered" id="reseps">
                    <thead>
                        <tr>
                            <th>Bahan Baku</th>
                            <th width="300px" class="text-center">Satuan</th>
                            <th width="200px" class="text-center">Qty</th>
                            <th width="1" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data_detail_bahan_baku as $key => $data_detail){ ?>
                            <tr data-row-id="<?= $data_detail->id_barang_bahan_baku ?>">
                            <!-- <tr> -->
                                <td>
                                    <!-- <select class="form-control select2" id="resep-id_barang-'.$bahan_baku['id_barang_bahan_baku'].'" name="resep[<?= $key ?>][id_barang_bahan_baku]" style="width: 100%;" readonly>
                                        <?php
                                            $SelectBarang = $data_detail->id_barang_bahan_baku;
                                            foreach($this->db->get('barang')->result() as $select){
                                        ?>
                                            <option value="<?= $select->id_barang ?>"
                                                <?php
                                                    if($SelectBarang==$select->id_barang){
                                                        echo "selected";
                                                    }
                                                ?>
                                            ><?= $select->nama_barang ?></option>
                                        <?php } ?>
                                    </select> -->
                                    <input type="hidden" class="form-control" name="resep[<?= $key ?>][id_barang_bahan_baku]" id="resep-id_barang-'.$bahan_baku['id_barang_bahan_baku'].'" value="<?= $data_detail->id_barang_bahan_baku ?>" placeholder="Masukkan Satuan" required readonly>
                                    <input type="text" class="form-control" value="<?= $data_detail->nama_barang ?>" placeholder="Masukkan Satuan" required readonly>
                                </td>
                                <td><input type="text" class="form-control text-center" name="resep[<?= $key ?>][nama_satuan]" id="resep-nama_satuan-'.$bahan_baku['nama_satuan'].'" value="<?= $data_detail->nama_satuan ?>" placeholder="Masukkan Satuan" required readonly></td>
                                <td><input type="text" class="form-control text-center" name="resep[<?= $key ?>][qty]" id="resep-qty-'.$bahan_baku['qty'].'" value="<?= $data_detail->qty ?>" placeholder="Masukkan Qty" required></td>
                                <td><button class="btn btn-sm btn-danger" type="button" onclick="removeResep('<?= $data_detail->id_barang_bahan_baku ?>')"><i class="fa fa-trash"></i></button></td>
                            </tr>
                        <?php } ?>
                        <tr id="form-add-resep">
                            <td>
                                <select class="form-control select2" id="form-add-resep-id_barang_bahan_baku" onchange="ChangeSelectBarang()" style="width: 100%;">
                                    <option value="">- Pilih Bahan Baku -</option>
                                    <?php foreach($bahan_baku as $select){ ?>
                                        <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="nama_barang" id="form-add-resep-nama_barang" placeholder="Masukkan Nama Barang">
                            </td>
                            <td><input type="text" class="form-control text-center" name="nama_satuan" id="form-add-resep-nama_satuan" placeholder="Masukkan Satuan" required readonly></td>
                            <td><input type="text" class="form-control text-center" name="qty" id="form-add-resep-qty" placeholder="Masukkan Qty"></td>
                            <td><button class="btn btn-sm btn-primary" type="button" name="add_resep" onclick="addResep()"><i class="fa fa-plus"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-12">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data</button>
                        <a href="<?= base_url('bahan_baku/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>