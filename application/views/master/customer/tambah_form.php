<?php section('css') ?>
<?php endsection() ?>

<?php section('js') ?>
<?php endsection() ?>

<?php section('custom_js') ?>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Customer</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('customer/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Customer</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_customer" value="<?= $kode; ?>" placeholder="Masukkan ID Customer" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Customer</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_customer" placeholder="Masukkan Nama Customer" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="telephone" placeholder="Masukkan Telephone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Masukkan E-mail" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('customer/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>