<?php section('css') ?>
<?php endsection() ?>

<?php section('js') ?>
<?php endsection() ?>

<?php section('custom_js') ?>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Satuan</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('satuan/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Satuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_satuan" placeholder="Masukkan Nama Satuan" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alias</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="alias" placeholder="Masukkan Alias" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?= base_url('satuan/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>