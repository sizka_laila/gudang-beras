<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    function confirmDialog(url) {
        $('#btn-yes').attr('href',url);
    }
    $(document).ready(function () {
        $('#datatable').DataTable();
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Produksi</h1>
</section>
<section class="content">
    <?= $this->message->show('produksi') ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <a href="<?= base_url('produksi/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th width="60">Tanggal</th>
                        <th>Kode Produksi</th>
                        <th>Nama Barang Produksi</th>
                        <th class="text-center">Jumlah Produksi</th>
                        <th class="text-right">Total Harga Produksi</th>
                        <th class="text-center">Status Produksi</th>
                        <th width="130" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        foreach($data_produksi as $data){
                            if($data->status_produksi==1){
                                $data->status_produksi = 'Aktif';
                                $label_status_produksi = 'label-success';
                                $disable = '';
                            }else{
                                $data->status_produksi = 'Dibatalkan';
                                $label_status_produksi = 'label-danger';
                                $disable = 'disabled';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= mediumdate($data->tanggal_produksi) ?></td>
                        <td><?= $data->id_produksi ?></td>
                        <td><?= $data->nama_barang ?></td>
                        <td class="text-center"><?= $data->jumlah ?></td>
                        <td class="text-right"><?= rupiah($data->total_produksi) ?></td>
                        <td class="text-center"><label class="label <?= $label_status_produksi ?>"><?= $data->status_produksi ?></label></td>
                        <td class="text-center">
                            <a href="<?= base_url('produksi/detail/'.$data->id_produksi) ?>" class="btn btn-info btn-sm"><i class="fa fa-file-text"></i> Detail</a>
                            <a href="#HapusData" class="btn btn-danger btn-sm" onclick="confirmDialog('<?= base_url('produksi/batal/'.$data->id_produksi) ?>')" data-toggle="modal" <?= $disable ?>><i class="fa fa-flag"></i> Batalkan</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<div class="modal modal-default" id="HapusData" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Hapus Data</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>membatalkan</b> data ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" id="btn-yes"><i class="fa fa-check"></i> Ya</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>
