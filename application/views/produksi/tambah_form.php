<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/jquery-number/jquery.number.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('#produksi-harga_produksi').number(true, 0, ',', '.');
        $('#produksi-harga_produksi').keyup(function() {
            // $(this).val(formatRupiah($(this).val()));
            var qty = $('#produksi-jumlah_produksi').val();
            var harga_produksi = toFloat(number_value($('#produksi-harga_produksi').val()));
            var total_harga = qty*harga_produksi;
            $('#produksi-total_produksi').val(total_harga.toString()).number(true, 2, ',', '.');
        });
        $('#produksi-jumlah_produksi').keyup(function() {
            var qty = $('#produksi-jumlah_produksi').val();
            var harga_produksi = toFloat(number_value($('#produksi-harga_produksi').val()));
            var total_harga = qty*harga_produksi;
            $('#produksi-total_produksi').val(total_harga.toString()).number(true, 2, ',', '.');
        });
    });
    function BlockInputHargaProduksi() {
        $('#produksi-harga_produksi').select();
    }
    // $('#produksi-harga_produksi').keyup(function() {
    //     $(this).val(formatRupiah($(this).val()));
    // });

    // function formatRupiah(angka, prefix){
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //     split           = number_string.split(','),
    //     sisa            = split[0].length % 3,
    //     rupiah          = split[0].substr(0, sisa),
    //     ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
    //     if(ribuan){
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    // }

    function ChangeSelectBarang(){
        var id_barang = $('#produksi-id_barang').val();
        // var total_produksi = $('#produksi-jumlah_produksi').val();
        // if(id_barang==""){
        //     document.getElementById('produksi_kosong').innerHTML="";
        //     return;
        // }else{ 
        //     document.getElementById('produksi_data');
        //     return;
            $.ajax({
                url : '<?= base_url() ?>/produksi/cari_barang/'+id_barang,
                type : 'post',
                dataType : 'json',
                success:function(response){
                    var data = response.data_bahan_baku;
                    var detail = response.data_detail_bahan_baku;
                    // $('#produksi-id_barang').val(response.data_bahan_baku['id_barang']);
                    // $('#produksi-nama_barang').val(response.data_bahan_baku['nama_barang']);
                    // $('#produksi-id_satuan').val(response.data_bahan_baku['id_satuan']);                    
                    $('#produksi-id_barang_produksi').val(data.barang_produksi);
                    $('#produksi-nama_satuan').val(data.nama_satuan);
                    $('#produksi-harga_produksi').val(data.harga_produksi).number(true, 0, ',', '.');
                    var html_row = '';
                    $.each(detail, function(i, row){
                        html_row += '<tr>';
                            html_row += '<td><input type="hidden" class="form-control" name="produksi['+row.id_barang_bahan_baku+'][id_barang_bahan_baku]" value="'+row.id_barang_bahan_baku+'" id="produksi-id_barang_bahan_baku-'+row.id_barang_bahan_baku+'" readonly><input type="text" class="form-control" name="produksi['+row.id_barang_bahan_baku+'][nama_barang]" value="'+row.nama_barang+'" id="produksi-nama_barang-'+row.nama_barang+'" readonly></td></td>';
                            html_row += '<td><input type="text" class="form-control text-center" name="produksi['+row.id_barang_bahan_baku+'][qty]" value="'+row.qty+'" id="produksi-qty-'+row.id_barang_bahan_baku+'" required></td>';
                            html_row += '<td><input type="text" class="form-control text-center" name="produksi['+row.id_barang_bahan_baku+'][nama_satuan]" value="'+row.nama_satuan+'" id="produksi-nama_satuan-'+row.id_barang_bahan_baku+'" readonly></td>';
                            // html_row += '<td><input type="text" class="form-control text-center" name="produksi['+row.id_barang_bahan_baku+'][total]" value="'+total_produksi*row.qty+'" id="produksi-total-'+row.id_barang_bahan_baku+'" readonly></td>';
                            // html_row += '<td><p class="text-center">'+row.nama_satuan+'</p></td>';
                        html_row += '</tr>';
                        // $('#detail_produksi-nama_barang').val(detail.nama_barang);
                        // $('#detail_produksi-qty').val(detail.qty);
                        // $('#detail_produksi-nama_satuan').val(detail.nama_satuan);
                    });
                    $('#data_detail').html(html_row);
                }
            });
        // }
    }

    // function get_produksi_data(){

    // }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Barang Produksi</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('produksi/tambah_aksi') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Barang Produksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_produksi" value="<?= $kode; ?>" placeholder="Masukkan ID Barang Produksi" required readonly>
                        <input type="hidden" value="<?= $this->session->userdata('id_user'); ?>" name="id_user">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Barang Produksi</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_bahan_baku" id="produksi-id_barang" onchange="ChangeSelectBarang()" style="width: 100%;" required>
                            <option value="">- Pilih Barang Yang Diproduksi -</option>
                            <?php foreach($barang_produksi as $select){ ?>
                                <option value="<?= $select->id_bahan_baku ?>"><?= $select->id_bahan_baku ?> - <?= $select->nama_barang ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" class="form-control" name="id_barang_produksi" id="produksi-id_barang_produksi" placeholder="Masukkan Kode Barang Produksi" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Satuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="produksi-nama_satuan" placeholder="Masukkan Satuan" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jumlah Produksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="jumlah" id="produksi-jumlah_produksi" placeholder="Masukkan Jumlah Produksi" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga_produksi" id="produksi-harga_produksi" onclick="BlockInputHargaProduksi()" placeholder="Masukkan Harga Produksi" required>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="box-footer">
                <p><h4 class="blue">Bahan Baku</h4></p>
                <table class="table table-striped table-bordered" id="reseps">
                    <thead>
                        <tr>
                            <th>Bahan Baku</th>
                            <th width="200px" class="text-center">Qty</th>
                            <th width="300px" class="text-center">Satuan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Beras 10 Kg</td>
                            <td class="text-center">20</td>
                            <td class="text-center">Karung</td>
                        </tr>
                        <tr>
                            <td>Karung 5 Kg</td>
                            <td class="text-center">40</td>
                            <td class="text-center">Biji</td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
            <!-- <div class="box-footer" id="produksi_kosong"></div> -->
            <div class="box-footer" id="produksi_data">
                <p><h4 class="blue">Produksi Bahan Baku</h4></p>
                <table class="table table-striped table-bordered" id="reseps">
                    <thead>
                        <tr>
                            <th>Bahan Baku</th>
                            <th width="200px" class="text-center">Qty</th>
                            <th width="300px" class="text-center">Satuan</th>
                        </tr>
                    </thead>
                    <tbody id="data_detail">
                        <!-- <tr>
                            <td><input type="text" class="form-control" placeholder="Masukkan Nama Barang" id="detail_produksi-nama_barang" required readonly></td>
                            <td><input type="text" class="form-control text-center" placeholder="Masukkan Qty" id="detail_produksi-qty" required></td>
                            <td><input type="text" class="form-control text-center" placeholder="Masukkan Satuan" id="detail_produksi-nama_satuan" required readonly></td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control price text-right" name="total_produksi" id="produksi-total_produksi" placeholder="0" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data</button>
                        <a href="<?= base_url('produksi/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>