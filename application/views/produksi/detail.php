<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Barang Produksi</h1>
</section>
<section class="content">
    <div class="box box-warning with-border">
        <div class="box-header"></div>
        <form class="form-horizontal" method="post">
            <?php foreach($data_produksi as $data){ ?>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Barang Produksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_produksi" value="<?= $data->id_produksi ?>" placeholder="Masukkan ID Barang Produksi" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Barang Produksi</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" name="id_bahan_baku" id="produksi-id_barang" style="width: 100%;" required disabled>
                            <!-- <option value="">- Pilih Barang Yang Diproduksi -</option> -->
                            <?php
                                $SelectBarangProduksi = $data->id_barang_produksi;
                                foreach($this->db->get('barang')->result() as $select){
                            ?>
                                <option value="<?= $select->id_barang ?>"
                                    <?php
                                        if($SelectBarangProduksi==$select->id_barang){
                                            echo "selected";
                                        }
                                    ?>
                                ><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                            <?php } ?>
                        </select>
                        <!-- <input type="hidden" class="form-control" name="id_barang_produksi" value="<?= $data->id_barang_produksi ?>" id="produksi-id_barang_produksi" placeholder="Masukkan Kode Barang Produksi" required readonly> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Satuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="produksi-nama_satuan" value="<?= $data->nama_satuan ?>" placeholder="Masukkan Satuan" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jumlah Produksi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="jumlah" value="<?= $data->jumlah ?>" id="produksi-jumlah_produksi" placeholder="Masukkan Jumlah Produksi" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control text-right" name="harga_produksi" value="<?= rupiah_format($data->harga_produksi) ?>" id="produksi-harga_produksi" placeholder="Masukkan Harga Produksi" required readonly>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="box-footer" id="produksi_data">
                <p><h4 class="blue">Produksi Bahan Baku</h4></p>
                <table class="table table-striped table-bordered" id="reseps">
                    <thead>
                        <tr>
                            <th>Bahan Baku</th>
                            <th width="200px" class="text-center">Qty</th>
                            <th width="300px" class="text-center">Satuan</th>
                        </tr>
                    </thead>
                    <tbody id="data_detail">
                        <?php foreach($data_detail_produksi as $data_detail){ ?>
                        <tr>
                            <td><input type="text" class="form-control" placeholder="Masukkan Nama Barang" value="<?= $data_detail->id_barang_bahan_baku ?> - <?= $data_detail->nama_barang ?>" id="detail_produksi-nama_barang" required readonly></td>
                            <td><input type="text" class="form-control text-center" value="<?= $data_detail->qty ?>" placeholder="Masukkan Qty" id="detail_produksi-qty" required readonly></td>
                            <td><input type="text" class="form-control text-center" value="<?= $data_detail->nama_satuan ?>" placeholder="Masukkan Satuan" id="detail_produksi-nama_satuan" required readonly></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <?php foreach($data_produksi as $data){ ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Total Harga Produksi</label>
                    <div class="col-sm-10">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control price text-right" name="total_produksi" value="<?= rupiah_format($data->total_produksi) ?>" id="produksi-total_produksi" placeholder="0" required readonly>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <div class="col-sm-10">
                        <!-- <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data</button> -->
                        <a href="<?= base_url('produksi/index') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>