<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) { 
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Stok Opname</h1>
</section>
<section class="content">
    <?= $this->message->show('opname') ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <a href="<?= base_url('opname/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <form class="form-horizontal" action="<?= base_url('opname/cari_data') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Barang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_barang" placeholder="Masukkan Kode Barang">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" name="periode" placeholder="Masukkan Tanggal">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-info" type="submit"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="box-footer">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th width="1" class="text-center">No</th>
                        <th width="60">Tanggal</th>
                        <th>Barang</th>
                        <th class="text-center">Stok Akhir</th>
                        <th class="text-center">Stok Opname</th>
                        <th class="text-center">Selisih</th>
                        <th class="text-center">Keterangan</th>
                        <th>Catatan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        foreach($data_opname as $data){
                            if($data->keterangan==2){
                                $data->keterangan = 'Minus';
                                $label_status_opname = 'label-danger';
                            }elseif($data->keterangan==3){
                                $data->keterangan = 'Plus';
                                $label_status_opname = 'label-warning';
                            }else{
                                $data->keterangan = 'Cocok';
                                $label_status_opname = 'label-success';
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= mediumdate($data->tanggal) ?></td>
                        <td><?= $data->id_barang ?> - <?= $data->nama_barang ?></td>
                        <td class="text-center"><?= $data->stok_akhir ?></td>
                        <td class="text-center"><?= $data->stok_opname ?></td>
                        <td class="text-center"><?= $data->selisih ?></td>
                        <td class="text-center"><label class="label <?= $label_status_opname ?>"><?= $data->keterangan ?></label></td>
                        <td><?= $data->keterangan_lain ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php endsection() ?>
<?php getview('layouts/layout') ?>
