<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/select2/select2.min.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/select2/select2.full.min.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable({
            'scrollX': true
        });
        $('.datepicker').datepicker('setDate', new Date());
        $('.select2').select2();
        // $('#semua').show();
        // $('#minus').hide();
        // $('#plus').hide();
        // $('#cocok').hide();
        // $('#opname-stok_opname').keyup(function(){
        //     var stok_akhir = $('#opname-stok_akhir').val();
        //     var stok_opname = $('#opname-stok_opname').val();
        //     var selisih = stok_akhir-stok_opname;
        //     $('#opname-selisih').val(selisih);
        // });
        $('#SimpanData').submit(function(){
            var conf = confirm('Apakah Anda yakin menyimpan data ini ?');
            if(conf==true){
                return true;
            }else{
                return false;
            }
        });
    });
    function HitungSelisih(id_barang){
        var stok_akhir = $('#opname-stok_akhir-'+id_barang).val();
        var stok_opname = $('#opname-stok_opname-'+id_barang).val();
        var selisih = stok_opname-stok_akhir;
        // if(selisih==0){
        //     alert('cocok');
        // }
        if(selisih<0){
            // $('#semua-'+id_barang).hide();
            // $('#minus-'+id_barang).show();
            // $('#plus-'+id_barang).hide();
            // $('#cocok-'+id_barang).hide();
            $('#ket-'+id_barang).val('Minus');
            $('#keterangan-'+id_barang).val(2);
        }else if(selisih>0){
            // $('#semua-'+id_barang).hide();
            // $('#minus-'+id_barang).hide();
            // $('#plus-'+id_barang).show();
            // $('#cocok-'+id_barang).hide();
            $('#ket-'+id_barang).val('Plus');
            $('#keterangan-'+id_barang).val(3);
        }else{
            // $('#semua-'+id_barang).hide();
            // $('#minus-'+id_barang).hide();
            // $('#plus-'+id_barang).hide();
            // $('#cocok-'+id_barang).show();
            $('#ket-'+id_barang).val('Cocok');
            $('#keterangan-'+id_barang).val(1);
        }
        $('#opname-selisih-'+id_barang).val(selisih);
    }
    function ChangeSelectBarang(){
        var id_barang = $('#barang-id_barang').val();
        $.ajax({
            url : '<?= base_url() ?>opname/cari_barang/'+id_barang,
            type : 'post',
            dataType : 'json',
            success:function(response){
                $('#barang-id_barang').val(response['id_barang']);
                $('#barang-nama_barang').val(response['nama_barang']);
                $('#barang-id_satuan').val(response['id_satuan']);
                $('#barang-nama_satuan').val(response['nama_satuan']);
                $('#barang-qty').val(response['qty']);
            }
        });
    }
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Stok Opname</h1>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('opname/tambah_aksi') ?>" id="SimpanData" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Opname</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Opname" disabled>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Barang</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" id="barang-id_barang" onchange="ChangeSelectBarang()" style="width: 100%;">
                            <option value="">- Pilih Barang -</option>
                            <?php foreach($barang as $select){ ?>
                                <option value="<?= $select->id_barang ?>"><?= $select->id_barang ?> - <?= $select->nama_barang ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Stok Akhir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="barang-qty" placeholder="Masukkan Stok Akhir" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Stok Opname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Masukkan Stok Opname">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Selisih</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Masukkan Selisih" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-10">
                        <select class="form-control" style="width: 100%;" required>
                            <option value="">- Pilih Keterangan Opname -</option>
                            <option value="1">Cocok</option>
                            <option value="2">Hilang</option>
                            <option value="3">Rusak</option>
                            <option value="4">Lainnya</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="#" class="btn btn-success"><i class="fa fa-check"></i> Simpan</a>
                    </div>
                </div> -->
            </div>
            <div class="box-footer">
                <table class="table table-striped table-bordered" id="datatable">
                    <thead class="thead-dark">
                        <tr>
                            <th width="1" class="text-center">No</th>
                            <th width="300">Barang</th>
                            <th class="text-center">Stok Akhir</th>
                            <th class="text-center">Stok Opname</th>
                            <th class="text-center">Selisih</th>
                            <th>Keterangan</th>
                            <th class="text-center">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 1;
                            foreach($barang as $data){
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td>
                                <?= $data->id_barang ?> - <?= $data->nama_barang ?>
                            </td>
                            <td class="text-center">
                                <input type="text" class="form-control text-center" name="opname[<?= $data->id_barang ?>][stok_akhir]" id="opname-stok_akhir-<?= $data->id_barang ?>" value="<?= $data->qty ?>" placeholder="Masukkan Stok Akhir" readonly>
                            </td>
                            <td class="text-center"><input type="text" class="form-control text-center" name="opname[<?= $data->id_barang ?>][stok_opname]" id="opname-stok_opname-<?= $data->id_barang ?>" onchange="HitungSelisih('<?= $data->id_barang ?>')" placeholder="Masukkan Stok Opname"></td>
                            <td class="text-center"><input type="text" class="form-control text-center" name="opname[<?= $data->id_barang ?>][selisih]" id="opname-selisih-<?= $data->id_barang ?>" placeholder="Selisih" readonly></td>
                            <td>
                                <!-- select class="form-control" id="keterangan" style="width: 100%;" required>
                                    <option id="cocok" value="1">Cocok</option>
                                    <option id="minus" value="2">Minus</option>
                                    <option id="plus" value="3">Plus</option>
                                    <option value="4">Lainnya</option>
                                </select> -->
                                <!-- <input type="text" class="form-control" id="semua-<?= $data->id_barang ?>" value="" disabled>
                                <input type="text" class="form-control" id="cocok-<?= $data->id_barang ?>" value="Cocok" disabled>
                                <input type="text" class="form-control" id="minus-<?= $data->id_barang ?>" value="Minus" disabled> -->
                                <input type="text" class="form-control" id="ket-<?= $data->id_barang ?>" placeholder="Keterangan" disabled>
                                <input type="hidden" id="keterangan-<?= $data->id_barang ?>" name="opname[<?= $data->id_barang ?>][keterangan]">
                            </td>
                            <td>
                                <input type="text" class="form-control" name="opname[<?= $data->id_barang ?>][keterangan_lain]" placeholder="Masukkan Catatan">
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-12">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Simpan Data Opname</button>
                        <a href="<?= base_url('opname/index') ?>" class="btn btn-default"><i class="fa fa-times"></i> Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<div class="modal modal-default" id="SimpanConfirm" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Simpan Data Opname</strong></h4>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin akan <b>menyimpan</b> data ini ?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" id="btn-yes"><i class="fa fa-check"></i> Simpan</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tidak</button>
            </div>
        </div>
    </div>
</div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>
