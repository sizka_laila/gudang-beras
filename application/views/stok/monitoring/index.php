<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('public/plugin/datatables/dataTables.bootstrap.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/plugin/datepicker/datepicker3.css') ?>">
<?php endsection() ?>

<?php section('js') ?>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/bootstrap-datepicker.js') ?>"></script>
<?php endsection() ?>

<?php section('custom_js') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('.datepicker').datepicker();
    });
</script>
<?php endsection() ?>

<?php section('content') ?>
<section class="content-header">
    <h1>Monitoring Stok</h1>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header"></div>
        <form class="form-horizontal" action="<?= base_url('monitoring/cari_data') ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="start_date" class="form-control datepicker" value="<?= (isset($start_date) ? $end_date:date('01/m/Y')) ?>" placeholder="Masukkan Tanggal">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="end_date" class="form-control datepicker" value="<?= (isset($end_date) ? $end_date:date('d/m/Y')) ?>" placeholder="Masukkan Tanggal">
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Barang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id_barang" placeholder="Masukkan Kode Barang">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-info" type="submit"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="box-footer">
            <table class="table table-striped table-bordered" id="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th rowspan="2" width="1" class="text-center">No</th>
                        <th rowspan="2">Barang</th>
                        <th rowspan="2" class="text-center">Stok Awal</th>
                        <th colspan="2" class="text-center">Masuk</th>
                        <th rowspan="2" class="text-center">Opname</th>
                        <th colspan="2" class="text-center">Keluar</th>
                        <th rowspan="2" class="text-center">Stok Akhir</th>
                    </tr>
                    <tr>
                        <td class="text-center">Pembelian</td>
                        <td class="text-center">Produksi</td>
                        <td class="text-center">Penjualan</td>
                        <td class="text-center">Produksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        foreach($monitoring as $data){
                    ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <!-- <td><?= date('m-Y', strtotime($data->periode)) ?></td> -->
                        <td><?= $data->id_barang ?> - <?= $data->nama_barang ?></td>
                        <td class="text-center"><?= $data->stok_awal ?></td>
                        <td class="text-center"><?= $data->pembelian ?></td>
                        <td class="text-center"><?= $data->produksi ?></td>
                        <td class="text-center"><?= $data->opname ?></td>
                        <td class="text-center"><?= $data->penjualan ?></td>
                        <td class="text-center"><?= $data->bahan_baku ?></td>
                        <td class="text-center"><?= ($data->stok_awal + $data->stok_mutasi) ?></td>
                    <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?php endsection() ?>
<?php getview('layouts/layout') ?>