SELECT
    barang.id_barang,
    barang.nama_barang,
    COALESCE(monitoring.stok_periode, 0) AS stok_periode,
    COALESCE(pembelian_awal.qty, 0) AS pembelian_awal,
    COALESCE(produksi_awal.jumlah, 0) AS produksi_awal,
    COALESCE(opname_awal.selisih, 0) AS opname_awal,
    COALESCE(penjualan_awal.qty, 0) AS pejualan_awal,
    COALESCE(produksi_bahan_awal.total,0) AS bahan_baku_awal,
    COALESCE(pembelian_mutasi.qty, 0) AS pembelian,
    COALESCE(produksi_mutasi.jumlah, 0) AS produksi,
    COALESCE(opname_mutasi.selisih, 0) AS opname,
    COALESCE(penjualan_mutasi.qty, 0) AS penjualan,
    COALESCE(produksi_bahan_mutasi.total, 0) AS bahan_baku,
    (COALESCE(monitoring.stok_periode, 0) + COALESCE(pembelian_awal.qty, 0) + COALESCE(produksi_awal.jumlah, 0) + COALESCE(opname_awal.selisih, 0) - COALESCE(penjualan_awal.qty, 0) - COALESCE(produksi_bahan_awal.total,0)) AS stok_awal,
    (COALESCE(monitoring.stok_periode, 0) + COALESCE(pembelian_mutasi.qty, 0) + COALESCE(produksi_mutasi.jumlah, 0) + COALESCE(opname_mutasi.selisih, 0) - COALESCE(penjualan_mutasi.qty, 0) - COALESCE(produksi_bahan_mutasi.total,0)) AS stok_mutasi

FROM barang
LEFT JOIN (
    SELECT * FROM monitoring
    WHERE periode = (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
) monitoring ON monitoring.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang, SUM(qty) AS qty FROM detail_pembelian
    JOIN pembelian ON pembelian.id_pembelian = detail_pembelian.id_pembelian
    WHERE LEFT(pembelian.tanggal_pembelian,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
    AND pembelian.tanggal_pembelian < '2020-02-01'
    GROUP BY id_barang
) pembelian_awal ON pembelian_awal.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang_produksi, SUM(jumlah) AS jumlah FROM produksi
    WHERE LEFT(tanggal_produksi,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
    AND tanggal_produksi < '2020-02-01'
    GROUP BY id_barang_produksi
) produksi_awal ON produksi_awal.id_barang_produksi = barang.id_barang
LEFT JOIN (
    SELECT id_barang, SUM(selisih) AS selisih FROM opname
    WHERE LEFT(tanggal,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
    AND tanggal < '2020-02-01'
    GROUP BY id_barang
) opname_awal ON opname_awal.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang, SUM(qty) AS qty FROM detail_penjualan
    JOIN penjualan ON penjualan.id_penjualan = detail_penjualan.id_penjualan
    WHERE LEFT(penjualan.tanggal_penjualan,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
    AND penjualan.tanggal_penjualan < '2020-02-01'
    GROUP BY id_barang
) penjualan_awal ON penjualan_awal.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang_bahan_baku, SUM(total) AS total FROM detail_produksi
    JOIN produksi ON produksi .id_produksi  = detail_produksi .id_produksi
    WHERE LEFT(produksi .tanggal_produksi ,7) > (SELECT MAX(periode) FROM monitoring WHERE periode < '2020-02')
    AND produksi .tanggal_produksi < '2020-02-01'
    GROUP BY id_barang_bahan_baku
) produksi_bahan_awal ON produksi_bahan_awal.id_barang_bahan_baku = barang.id_barang

LEFT JOIN (
    SELECT id_barang, SUM(qty) AS qty FROM detail_pembelian
    JOIN pembelian ON pembelian.id_pembelian = detail_pembelian.id_pembelian
    WHERE pembelian.tanggal_pembelian >= '2020-02-01'
    AND pembelian.tanggal_pembelian <= '2020-02-29'
    GROUP BY id_barang
) pembelian_mutasi ON pembelian_mutasi.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang_produksi, SUM(jumlah) AS jumlah FROM produksi
    WHERE tanggal_produksi >= '2020-02-01'
    AND tanggal_produksi <= '2020-02-29'
    GROUP BY id_barang_produksi
) produksi_mutasi ON produksi_awal.id_barang_produksi = barang.id_barang
LEFT JOIN (
    SELECT id_barang, SUM(selisih) AS selisih FROM opname
    WHERE tanggal >= '2020-02-01'
    AND tanggal <= '2020-02-29'
    GROUP BY id_barang
) opname_mutasi ON opname_mutasi.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang, SUM(qty) AS qty FROM detail_penjualan
    JOIN penjualan ON penjualan.id_penjualan = detail_penjualan.id_penjualan
    WHERE penjualan.tanggal_penjualan >= '2020-02-01'
    AND penjualan.tanggal_penjualan <= '2020-02-29'
    GROUP BY id_barang
) penjualan_mutasi ON penjualan_mutasi.id_barang = barang.id_barang
LEFT JOIN (
    SELECT id_barang_bahan_baku, SUM(total) AS total FROM detail_produksi
    JOIN produksi ON produksi .id_produksi  = detail_produksi .id_produksi
    WHERE produksi .tanggal_produksi >= '2020-02-01'
    AND produksi .tanggal_produksi < '2020-02-29'
    GROUP BY id_barang_bahan_baku
) produksi_bahan_mutasi ON produksi_bahan_mutasi.id_barang_bahan_baku = barang.id_barang
