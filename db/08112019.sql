-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2019 at 09:56 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gudang-beras`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_baku`
--

CREATE TABLE IF NOT EXISTS `bahan_baku` (
  `id_bahan_baku` varchar(24) NOT NULL,
  `tanggal_terdaftar` date NOT NULL,
  `barang_produksi` varchar(24) NOT NULL,
  `harga_produksi` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan_baku`
--

INSERT INTO `bahan_baku` (`id_bahan_baku`, `tanggal_terdaftar`, `barang_produksi`, `harga_produksi`) VALUES
('RSP001112019018', '2019-11-02', 'BRG001102019004', 560000),
('RSP012102019010', '2019-10-31', 'BRG001102019004', 0),
('RSP012102019011', '2019-10-12', 'BRG001102019004', 0),
('RSP012102019012', '2019-10-12', '', 0),
('RSP012102019013', '2019-10-12', 'BRG001102019004', 0),
('RSP029102019015', '2019-10-29', 'BRG001102019004', 0),
('RSP030102019016', '2019-10-30', 'BRG001102019004', 100000),
('RSP031102019017', '2019-10-31', 'BRG015102019006', 600000);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` varchar(24) NOT NULL,
  `nama_barang` varchar(64) NOT NULL,
  `qty` int(4) NOT NULL,
  `id_satuan` int(4) NOT NULL,
  `jenis_barang` int(4) NOT NULL,
  `harga` double NOT NULL,
  `ppn` int(4) NOT NULL,
  `nominal_ppn` double NOT NULL,
  `harga_jual` double NOT NULL,
  `ppn_jual` int(4) NOT NULL,
  `nominal_ppn_jual` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `qty`, `id_satuan`, `jenis_barang`, `harga`, `ppn`, `nominal_ppn`, `harga_jual`, `ppn_jual`, `nominal_ppn_jual`) VALUES
('BRG001102019003', 'Karung 5 kg', 32, 3, 1, 15000, 0, 0, 0, 0, 0),
('BRG001102019004', 'Beras', 639, 1, 2, 25000, 1, 2500, 0, 0, 0),
('BRG007112019007', 'Beras 15 Kg', 50, 3, 2, 70000, 1, 7000, 90000, 1, 0),
('BRG008112019008', 'adasdfdgdf', 50, 3, 2, 500000, 0, 0, 100000, 1, 10000),
('BRG009102019005', 'Bekatul 5 kg', 89, 3, 3, 25000, 1, 2500, 0, 0, 0),
('BRG015102019006', 'Padi 10kg', 220, 3, 1, 20000, 1, 2000, 0, 0, 0),
('BRG030092019002', 'Karung 10 kg', 169, 3, 1, 15000, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` varchar(24) NOT NULL,
  `nama_customer` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('CTM025102019002', 'Toko Pak Jaya', 'Banyumas', '089999000888', 'jaya@gmail.com', '2019-10-25'),
('CTM027092019001', 'Toko Pak Budi', 'Podorejo', '085777888999', 'budi@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `detail_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `detail_bahan_baku` (
  `id_detail_bahan_baku` int(4) NOT NULL,
  `id_bahan_baku` varchar(24) NOT NULL,
  `id_barang_bahan_baku` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_bahan_baku`
--

INSERT INTO `detail_bahan_baku` (`id_detail_bahan_baku`, `id_bahan_baku`, `id_barang_bahan_baku`, `qty`) VALUES
(3, 'RSP012102019011', 'BRG030092019002', 9),
(4, 'RSP012102019012', 'BRG030092019002', 2),
(5, 'RSP012102019013', 'BRG001102019003', 0),
(7, 'RSP029102019015', 'BRG001102019003', 50),
(8, 'RSP029102019015', 'BRG015102019006', 25),
(9, 'RSP030102019016', 'BRG001102019003', 4),
(10, 'RSP030102019016', 'BRG030092019002', 2),
(22, 'RSP031102019017', 'BRG001102019003', 2),
(34, 'RSP012102019010', 'BRG001102019003', 2),
(35, 'RSP012102019010', 'BRG015102019006', 1),
(44, 'RSP001112019018', 'BRG001102019003', 8),
(45, 'RSP001112019018', 'BRG015102019006', 6),
(46, 'RSP001112019018', 'BRG030092019002', 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE IF NOT EXISTS `detail_pembelian` (
  `id_detail_pembelian` int(4) NOT NULL,
  `id_pembelian` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL,
  `harga` double NOT NULL,
  `ppn_item` int(4) DEFAULT NULL,
  `nominal_ppn_item` double DEFAULT NULL,
  `sub_total_harga` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`id_detail_pembelian`, `id_pembelian`, `id_barang`, `qty`, `harga`, `ppn_item`, `nominal_ppn_item`, `sub_total_harga`) VALUES
(45, 'PBL025102019001', 'BRG001102019004', 5, 25000, 1, 2500, 137500),
(46, 'PBL025102019001', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(47, 'PBL025102019001', 'BRG001102019003', 5, 15000, 0, 0, 75000),
(48, 'PBL025102019002', 'BRG015102019006', 10, 20000, 1, 2000, 220000),
(49, 'PBL025102019002', 'BRG001102019004', 10, 25000, 1, 2500, 275000),
(50, 'PBL025102019002', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(51, 'PBL025102019003', 'BRG030092019002', 50, 15000, 0, 1500, 825000),
(52, 'PBL025102019003', 'BRG009102019005', 20, 25000, 0, 0, 500000),
(53, 'PBL025102019003', 'BRG015102019006', 25, 20000, 1, 2000, 550000),
(54, 'PBL008112019004', 'BRG001102019004', 100, 25000, 0, 0, 2500000),
(55, 'PBL008112019004', 'BRG015102019006', 50, 20000, 1, 2000, 1100000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE IF NOT EXISTS `detail_penjualan` (
  `id_detail_penjualan` int(4) NOT NULL,
  `id_penjualan` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_produksi`
--

CREATE TABLE IF NOT EXISTS `detail_produksi` (
  `id_detail_produksi` int(4) NOT NULL,
  `id_produksi` varchar(24) NOT NULL,
  `id_barang_bahan_baku` varchar(24) NOT NULL,
  `qty` int(4) NOT NULL,
  `total` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_produksi`
--

INSERT INTO `detail_produksi` (`id_detail_produksi`, `id_produksi`, `id_barang_bahan_baku`, `qty`, `total`) VALUES
(1, 'PRD003112019005', 'BRG001102019003', 10, 40),
(2, 'PRD003112019005', 'BRG015102019006', 5, 20),
(3, 'PRD003112019006', 'BRG001102019003', 2, 4),
(4, 'PRD003112019006', 'BRG015102019006', 1, 2),
(5, 'PRD003112019007', 'BRG001102019003', 2, 4),
(6, 'PRD003112019007', 'BRG015102019006', 1, 2),
(7, 'PRD003112019008', 'BRG001102019003', 1, 2),
(8, 'PRD003112019009', 'BRG030092019002', 1, 2),
(9, 'PRD003112019010', 'BRG001102019003', 2, 10),
(10, 'PRD008112019011', 'BRG001102019003', 2, 10),
(11, 'PRD008112019012', 'BRG030092019002', 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(24) NOT NULL,
  `nama_pegawai` varchar(64) NOT NULL,
  `jenis_kelamin` int(4) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('PGW024092019001', 'SizkaLH', 2, 'Podorejo', '085777888999', 'sizkalailatul96@gmail.com', '2019-09-24'),
('PGW026092019002', 'Jaya Arifin', 1, 'Pagerwojo', '089999000888', 'sdj@gmail.com', '2019-09-26'),
('PGW026102019004', 'Nurillia Ainia Ratri', 2, 'Tulungagung', '0895367255770', 'nurilliaaira.haltec@gmail.com', '2019-10-26'),
('PGW027092019003', 'Nurmala Sari', 2, 'Lamongan', '089999000888', 'nurmala@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(24) NOT NULL,
  `id_transaksi` varchar(24) DEFAULT NULL,
  `tanggal_pembelian` date NOT NULL,
  `id_supplier` varchar(24) NOT NULL,
  `cara_beli` int(4) NOT NULL,
  `id_pengambil` varchar(24) DEFAULT NULL,
  `nama_pengantar` varchar(64) DEFAULT NULL,
  `total_beli` double NOT NULL,
  `ppn_header` int(4) DEFAULT NULL,
  `nominal_ppn_header` double NOT NULL,
  `total_diskon` double DEFAULT NULL,
  `potongan` double DEFAULT NULL,
  `total_bayar` double NOT NULL,
  `jenis_pembayaran` int(4) NOT NULL,
  `bank` varchar(64) DEFAULT NULL,
  `status_pembelian` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_transaksi`, `tanggal_pembelian`, `id_supplier`, `cara_beli`, `id_pengambil`, `nama_pengantar`, `total_beli`, `ppn_header`, `nominal_ppn_header`, `total_diskon`, `potongan`, `total_bayar`, `jenis_pembayaran`, `bank`, `status_pembelian`) VALUES
('PBL008112019004', 'TRANS20191108001', '2019-11-20', 'SPL025102019003', 2, 'PGW026102019004', '', 3600000, 0, 100000, 5, 20000, 3400000, 2, 'Mandiri', 1),
('PBL025102019001', 'TRANS20191024001', '2019-10-24', 'SPL027092019001', 2, 'PGW026092019002', '', 350000, 0, 25000, 5, 2500, 330000, 2, 'Mandiri', 0),
('PBL025102019002', 'TRANS20191025001', '2019-10-25', 'SPL025102019003', 1, '', 'Agung Laksono', 632500, 0, 57500, 0, 500, 632000, 1, '', 1),
('PBL025102019003', 'TRANS20191025002', '2019-10-25', 'SPL025102019002', 2, 'PGW024092019001', '', 1875000, 1, 125000, 10, 0, 1687500, 2, 'BRI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(24) NOT NULL,
  `tanggal_penjualan` date NOT NULL,
  `id_customer` varchar(24) NOT NULL,
  `id_pengantar` varchar(24) NOT NULL,
  `nama_pengantar` varchar(64) NOT NULL,
  `total_jual` double NOT NULL,
  `total_diskon` double NOT NULL,
  `total_bayar` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE IF NOT EXISTS `produksi` (
  `id_produksi` varchar(24) NOT NULL,
  `tanggal_produksi` date NOT NULL,
  `id_bahan_baku` varchar(24) NOT NULL,
  `id_barang_produksi` varchar(24) DEFAULT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_produksi` double NOT NULL,
  `status_produksi` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi`
--

INSERT INTO `produksi` (`id_produksi`, `tanggal_produksi`, `id_bahan_baku`, `id_barang_produksi`, `jumlah`, `harga_produksi`, `status_produksi`) VALUES
('PRD003112019005', '2019-11-03', 'RSP029102019015', 'BRG001102019004', 4, 50000, 1),
('PRD003112019006', '2019-11-03', 'RSP012102019010', 'BRG001102019004', 2, 0, 0),
('PRD003112019007', '2019-11-03', 'RSP012102019010', 'BRG001102019004', 2, 600000, 1),
('PRD003112019008', '2019-11-03', 'RSP012102019013', 'BRG001102019004', 2, 60000, 1),
('PRD003112019009', '2019-11-03', 'RSP012102019011', 'BRG001102019004', 2, 50000, 0),
('PRD003112019010', '2019-11-03', 'RSP012102019013', 'BRG001102019004', 5, 0, 0),
('PRD008112019011', '2019-11-08', 'RSP012102019013', 'BRG001102019004', 5, 50000, 1),
('PRD008112019012', '2019-11-08', 'RSP012102019011', 'BRG001102019004', 5, 20000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(4) NOT NULL,
  `nama_satuan` varchar(64) NOT NULL,
  `alias` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `alias`) VALUES
(1, 'Kilogram', 'kg'),
(3, 'Biji', 'Biji'),
(4, 'Gram', 'gr');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` varchar(24) NOT NULL,
  `nama_supplier` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('SPL025102019002', 'PT. Jaya Abadi', 'Mojokerto', '087999000777', 'jayaabadi@gmail.com', '2019-10-25'),
('SPL025102019003', 'PT. Makmur Sejahtera', 'Banyuwangi', '087888999000', 'makmursejahtera@gmail.com', '2019-10-25'),
('SPL027092019001', 'PT. Tani Jaya', 'Malang', '087888999000', 'tanijaya@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL,
  `id_pegawai` varchar(24) NOT NULL,
  `hak_akses` int(4) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` int(4) DEFAULT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `hak_akses`, `username`, `password`, `status`, `tanggal_terdaftar`) VALUES
(1, 'PGW024092019001', 3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-09-26'),
(2, 'PGW026092019002', 2, 'jaya', 'ce9689abdeab50b5bee3b56c7aadee27', 0, '2019-09-26'),
(4, 'PGW027092019003', 1, 'nurmala', '60d075b97943df3727e2150ea7a20eb5', 1, '2019-09-27'),
(5, 'PGW026102019004', 3, 'nurilliaratri', '25f9e794323b453885f5181f1b624d0b', 1, '2019-10-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_baku`
--
ALTER TABLE `bahan_baku`
  ADD PRIMARY KEY (`id_bahan_baku`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  ADD PRIMARY KEY (`id_detail_bahan_baku`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id_detail_pembelian`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_detail_penjualan`);

--
-- Indexes for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  ADD PRIMARY KEY (`id_detail_produksi`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id_produksi`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  MODIFY `id_detail_bahan_baku` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id_detail_pembelian` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_detail_penjualan` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  MODIFY `id_detail_produksi` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
