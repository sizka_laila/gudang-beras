-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2019 at 07:08 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gudang-beras`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` varchar(24) NOT NULL,
  `nama_barang` varchar(64) NOT NULL,
  `qty` int(4) NOT NULL,
  `id_satuan` int(4) NOT NULL,
  `harga` double NOT NULL,
  `ppn` int(4) NOT NULL,
  `nominal_ppn` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `qty`, `id_satuan`, `harga`, `ppn`, `nominal_ppn`) VALUES
('BRG030092019001', 'Padi', 2, 1, 200000, 1, 100),
('BRG030092019002', 'Karung 1 kg', 50, 3, 10000, 0, 100);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` varchar(24) NOT NULL,
  `nama_customer` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('CTM027092019001', 'Toko Pak Budi', 'Podorejo', '085777888999', 'budi@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(24) NOT NULL,
  `nama_pegawai` varchar(64) NOT NULL,
  `jenis_kelamin` int(4) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('PGW024092019001', 'SizkaLH', 2, 'Podorejo', '085777888999', 'sizkalailatul96@gmail.com', '2019-09-24'),
('PGW026092019002', 'Jaya Arifin', 1, 'Pagerwojo', '089999000888', 'sdj@gmail.com', '2019-09-26'),
('PGW027092019003', 'Nurmala Sari', 2, 'Lamongan', '089999000888', 'nurmala@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(4) NOT NULL,
  `nama_satuan` varchar(64) NOT NULL,
  `alias` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `alias`) VALUES
(1, 'Kilogram', 'Kg'),
(3, 'Biji', 'Biji');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` varchar(24) NOT NULL,
  `nama_supplier` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('SPL027092019001', 'PT. Tani Jaya', 'Malang', '087888999000', 'tanijaya@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL,
  `id_pegawai` varchar(24) NOT NULL,
  `hak_akses` int(4) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` int(4) DEFAULT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `hak_akses`, `username`, `password`, `status`, `tanggal_terdaftar`) VALUES
(1, 'PGW024092019001', 3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-09-26'),
(2, 'PGW026092019002', 2, 'jaya', '21232f297a57a5a743894a0e4a801fc3', 0, '2019-09-26'),
(4, 'PGW027092019003', 1, 'nurmala', '60d075b97943df3727e2150ea7a20eb5', 1, '2019-09-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
