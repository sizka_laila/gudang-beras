-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2019 at 05:28 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gudang-beras`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_baku`
--

CREATE TABLE IF NOT EXISTS `bahan_baku` (
  `id_bahan_baku` varchar(24) NOT NULL,
  `tanggal_terdaftar` date NOT NULL,
  `barang_produksi` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan_baku`
--

INSERT INTO `bahan_baku` (`id_bahan_baku`, `tanggal_terdaftar`, `barang_produksi`) VALUES
('RSP012102019010', '2019-10-12', 'BRG001102019004'),
('RSP012102019011', '2019-10-12', 'BRG001102019004'),
('RSP012102019012', '2019-10-12', ''),
('RSP012102019013', '2019-10-12', 'BRG001102019004'),
('RSP015102019014', '2019-10-15', 'BRG001102019004');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` varchar(24) NOT NULL,
  `nama_barang` varchar(64) NOT NULL,
  `qty` int(4) NOT NULL,
  `id_satuan` int(4) NOT NULL,
  `jenis_barang` int(4) NOT NULL,
  `harga` double NOT NULL,
  `ppn` int(4) NOT NULL,
  `nominal_ppn` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `qty`, `id_satuan`, `jenis_barang`, `harga`, `ppn`, `nominal_ppn`) VALUES
('BRG001102019003', 'Karung 5 kg', 52, 3, 1, 15000, 0, 0),
('BRG001102019004', 'Beras', 518, 1, 2, 25000, 1, 2500),
('BRG009102019005', 'Bekatul 5 kg', 64, 3, 3, 25000, 1, 2500),
('BRG015102019006', 'Padi 10kg', 135, 3, 1, 20000, 1, 2000),
('BRG030092019002', 'Karung 10 kg', 120, 3, 1, 15000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` varchar(24) NOT NULL,
  `nama_customer` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('CTM027092019001', 'Toko Pak Budi', 'Podorejo', '085777888999', 'budi@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `detail_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `detail_bahan_baku` (
  `id_detail_bahan_baku` int(4) NOT NULL,
  `id_bahan_baku` varchar(24) NOT NULL,
  `id_barang_bahan_baku` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_bahan_baku`
--

INSERT INTO `detail_bahan_baku` (`id_detail_bahan_baku`, `id_bahan_baku`, `id_barang_bahan_baku`, `qty`) VALUES
(1, 'RSP012102019010', 'BRG001102019003', 2),
(2, 'RSP012102019010', 'BRG001102019003', 2),
(3, 'RSP012102019011', 'BRG030092019002', 9),
(4, 'RSP012102019012', 'BRG030092019002', 2),
(5, 'RSP012102019013', 'BRG001102019003', 0),
(6, 'RSP015102019014', 'BRG015102019006', 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE IF NOT EXISTS `detail_pembelian` (
  `id_detail_pembelian` int(4) NOT NULL,
  `id_pembelian` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL,
  `harga` double NOT NULL,
  `ppn_item` int(4) DEFAULT NULL,
  `nominal_ppn_item` double DEFAULT NULL,
  `sub_total_harga` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`id_detail_pembelian`, `id_pembelian`, `id_barang`, `qty`, `harga`, `ppn_item`, `nominal_ppn_item`, `sub_total_harga`) VALUES
(1, 'PBL020102019001', 'BRG001102019004', 1, 25000, 1, 2500, 27500),
(2, 'PBL020102019001', 'BRG001102019003', 1, 25000, 1, 2500, 27500),
(3, 'PBL020102019004', 'BRG001102019004', 1, 25000, 1, 2500, 27500),
(4, 'PBL022102019005', 'BRG001102019003', 2, 15000, NULL, 0, 30000),
(5, 'PBL022102019005', 'BRG009102019005', 1, 25000, 1, 2500, 27500),
(6, 'PBL022102019006', 'BRG015102019006', 100, 50000, 0, 500000, 5500000),
(7, 'PBL022102019006', 'BRG030092019002', 50, 5000, NULL, 0, 250000),
(8, 'PBL023102019007', 'BRG001102019003', 1, 15000, NULL, 0, 15000),
(9, 'PBL023102019007', 'BRG001102019004', 2, 25000, 1, 2500, 55000),
(10, 'PBL023102019007', 'BRG015102019006', 1, 20000, 1, 2000, 22000),
(11, 'PBL024102019008', 'BRG001102019004', 10, 25000, 1, 25000, 275000),
(12, 'PBL024102019008', 'BRG009102019005', 10, 25000, 1, 25000, 275000),
(13, 'PBL024102019009', 'BRG009102019005', 4, 25, 1, 2.5, 110),
(14, 'PBL024102019009', 'BRG015102019006', 2, 20, 1, 2, 44),
(15, 'PBL024102019010', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(16, 'PBL024102019010', 'BRG015102019006', 5, 20000, 1, 2000, 110000),
(17, 'PBL024102019011', 'BRG009102019005', 5, 25000, 1, 12500, 137500),
(18, 'PBL024102019011', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(19, 'PBL024102019011', 'BRG015102019006', 3, 20000, 1, 2000, 66000),
(20, 'PBL024102019011', 'BRG030092019002', 15, 15000, 0, 0, 225000),
(21, 'PBL024102019012', 'BRG030092019002', 2, 15000, 0, 0, 30000),
(22, 'PBL024102019012', 'BRG001102019004', 2, 25000, 1, 2500, 55000),
(23, 'PBL024102019012', 'BRG015102019006', 1, 20000, 1, 2000, 22000),
(24, 'PBL024102019013', 'BRG001102019004', 2, 25000, 1, 2500, 55000),
(25, 'PBL024102019013', 'BRG009102019005', 2, 25000, 1, 2500, 55000),
(26, 'PBL024102019013', 'BRG030092019002', 1, 15000, 0, 0, 15000),
(27, 'PBL024102019013', 'BRG015102019006', 1, 20000, 1, 2000, 22000),
(28, 'PBL024102019014', 'BRG009102019005', 3, 25000, 1, 2500, 82500),
(29, 'PBL024102019015', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(30, 'PBL024102019015', 'BRG001102019003', 5, 15000, 0, 0, 75000),
(31, 'PBL024102019016', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(32, 'PBL024102019016', 'BRG001102019004', 2, 25000, 1, 2500, 55000),
(33, 'PBL024102019016', 'BRG009102019005', 1, 25000, 1, 2500, 27500),
(34, 'PBL024102019017', 'BRG001102019003', 3, 15000, 0, 0, 45000),
(35, 'PBL024102019017', 'BRG009102019005', 4, 25000, 1, 2500, 110000),
(36, 'PBL024102019017', 'BRG015102019006', 2, 20000, 1, 2000, 44000),
(37, 'PBL024102019018', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(38, 'PBL024102019018', 'BRG009102019005', 2, 25000, 1, 2500, 55000),
(39, 'PBL024102019018', 'BRG030092019002', 2, 15000, 0, 1500, 33000),
(40, 'PBL024102019019', 'BRG001102019004', 2, 25000, 1, 2500, 55000),
(41, 'PBL024102019019', 'BRG009102019005', 2, 25000, 1, 2500, 55000),
(42, 'PBL024102019020', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(43, 'PBL024102019020', 'BRG001102019004', 1, 25000, 1, 2500, 27500),
(44, 'PBL024102019020', 'BRG009102019005', 1, 25000, 1, 2500, 27500);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE IF NOT EXISTS `detail_penjualan` (
  `id_detail_penjualan` int(4) NOT NULL,
  `id_penjualan` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_produksi`
--

CREATE TABLE IF NOT EXISTS `detail_produksi` (
  `id_detail_produksi` int(4) NOT NULL,
  `id_produksi` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `jumlah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(24) NOT NULL,
  `nama_pegawai` varchar(64) NOT NULL,
  `jenis_kelamin` int(4) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('PGW024092019001', 'SizkaLH', 2, 'Podorejo', '085777888999', 'sizkalailatul96@gmail.com', '2019-09-24'),
('PGW026092019002', 'Jaya Arifin', 1, 'Pagerwojo', '089999000888', 'sdj@gmail.com', '2019-09-26'),
('PGW027092019003', 'Nurmala Sari', 2, 'Lamongan', '089999000888', 'nurmala@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(24) NOT NULL,
  `id_transaksi` varchar(24) DEFAULT NULL,
  `tanggal_pembelian` date NOT NULL,
  `id_supplier` varchar(24) NOT NULL,
  `cara_beli` int(4) NOT NULL,
  `id_pengantar` varchar(24) DEFAULT NULL,
  `nama_pengambil` varchar(64) DEFAULT NULL,
  `total_beli` double NOT NULL,
  `ppn_header` int(4) DEFAULT NULL,
  `nominal_ppn_header` double NOT NULL,
  `total_diskon` double DEFAULT NULL,
  `potongan` double DEFAULT NULL,
  `total_bayar` double NOT NULL,
  `jenis_pembayaran` int(4) NOT NULL,
  `bank` varchar(64) DEFAULT NULL,
  `status_pembelian` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_transaksi`, `tanggal_pembelian`, `id_supplier`, `cara_beli`, `id_pengantar`, `nama_pengambil`, `total_beli`, `ppn_header`, `nominal_ppn_header`, `total_diskon`, `potongan`, `total_bayar`, `jenis_pembayaran`, `bank`, `status_pembelian`) VALUES
('PBL020102019001', '', '2019-10-14', 'SPL027092019001', 1, 'PGW024092019001', '', 27500, NULL, 0, 0, 500, 27000, 2, 'Mandiri', 0),
('PBL020102019002', '', '2019-10-14', 'SPL027092019001', 1, 'PGW026092019002', '', 55000, NULL, 0, 0, 5000, 50000, 1, '', 0),
('PBL020102019003', '', '2019-10-08', 'SPL027092019001', 1, 'PGW026092019002', '', 37000, NULL, 0, 0, 2000, 35000, 1, '', 1),
('PBL020102019004', '', '2019-10-23', 'SPL027092019001', 2, '', 'Bambang', 27500, NULL, 0, 0, 500, 27000, 1, '', 1),
('PBL022102019005', 'TR22102019001', '2019-10-19', 'SPL027092019001', 2, NULL, 'Erika', 57500, NULL, 0, 0, 500, 57000, 1, '', 1),
('PBL022102019006', 'ART123456789', '2019-08-07', 'SPL027092019001', 2, NULL, 'Retno', 5832500, NULL, 0, 5, 2500, 5538375, 1, '', 1),
('PBL023102019007', '', '2019-10-17', 'SPL027092019001', 1, 'PGW026092019002', '', 92000, NULL, 0, 0, 2000, 90000, 1, '', 1),
('PBL024102019008', 'SLH0099897889', '2019-10-14', 'SPL027092019001', 2, '', 'Wulan', 550000, 1, 0, 5, 500, 522000, 2, 'Mandiri', 1),
('PBL024102019009', 'SLH0099897880', '2019-10-05', 'SPL027092019001', 1, 'PGW024092019001', '', 154, 0, 0, 5, 1.3, 145, 2, 'Mandiri', 1),
('PBL024102019010', 'ART123456780', '2019-10-11', 'SPL027092019001', 1, 'PGW026092019002', '', 140000, 0, 0, 2, 2200, 135000, 1, '', 1),
('PBL024102019011', 'art432543543', '2019-10-17', 'SPL027092019001', 2, '', 'ratri', 458500, 0, 0, 0, 0, 458500, 2, 'BRI', 1),
('PBL024102019012', 'rtr3123123123', '2019-10-26', 'SPL027092019001', 1, 'PGW024092019001', '', 107000, 0, 0, 5, 1650, 100000, 1, '', 1),
('PBL024102019013', 'sadasshd', '2019-10-26', 'SPL027092019001', 1, 'PGW024092019001', '', 147000, 0, 0, 0, 0, 147000, 1, '', 1),
('PBL024102019014', 'asjdhjash2343243', '2019-10-27', 'SPL027092019001', 1, 'PGW024092019001', '', 126500, 0, 0, 0, 0, 126500, 1, '', 1),
('PBL024102019015', '7326276ysdhhdhjshjds', '2019-10-29', 'SPL027092019001', 2, '', 'Yuan Sombong bianto raharjo', 295000, 0, 0, 0, 0, 295000, 1, '', 1),
('PBL024102019016', 'yuansombong646453', '2019-10-25', 'SPL027092019001', 2, '', 'ratritestersejati', 112500, 0, 0, 0, 0, 112500, 1, '', 1),
('PBL024102019017', '2019/10/001', '2019-10-24', 'SPL027092019001', 1, 'PGW024092019001', '', 199000, 0, 0, 5, 9000, 180050, 1, '', 1),
('PBL024102019018', '', '2019-10-08', 'SPL027092019001', 1, 'PGW026092019002', '', 118000, 0, 0, 2, 5640, 110000, 1, '', 1),
('PBL024102019019', '', '2019-10-16', 'SPL027092019001', 1, 'PGW027092019003', '', 110000, 0, 10000, 2, 800, 107000, 1, '', 1),
('PBL024102019020', '4534rrrrt', '2019-10-25', 'SPL027092019001', 1, 'PGW026092019002', '', 85000, 0, 5000, 5, 750, 80000, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(24) NOT NULL,
  `tanggal_penjualan` date NOT NULL,
  `id_customer` varchar(24) NOT NULL,
  `id_pengantar` varchar(24) NOT NULL,
  `nama_pengantar` varchar(64) NOT NULL,
  `total_jual` double NOT NULL,
  `total_diskon` double NOT NULL,
  `total_bayar` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE IF NOT EXISTS `produksi` (
  `id_produksi` varchar(24) NOT NULL,
  `tanggal_produksi` date NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `nama_barang` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(4) NOT NULL,
  `nama_satuan` varchar(64) NOT NULL,
  `alias` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `alias`) VALUES
(1, 'Kilogram', 'Kg'),
(3, 'Biji', 'Biji'),
(4, 'xxx', 'xxxxx');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` varchar(24) NOT NULL,
  `nama_supplier` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('SPL027092019001', 'PT. Tani Jaya', 'Malang', '087888999000', 'tanijaya@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL,
  `id_pegawai` varchar(24) NOT NULL,
  `hak_akses` int(4) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` int(4) DEFAULT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `hak_akses`, `username`, `password`, `status`, `tanggal_terdaftar`) VALUES
(1, 'PGW024092019001', 3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-09-26'),
(2, 'PGW026092019002', 2, 'jaya', 'ce9689abdeab50b5bee3b56c7aadee27', 0, '2019-09-26'),
(4, 'PGW027092019003', 1, 'nurmala', '60d075b97943df3727e2150ea7a20eb5', 1, '2019-09-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_baku`
--
ALTER TABLE `bahan_baku`
  ADD PRIMARY KEY (`id_bahan_baku`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  ADD PRIMARY KEY (`id_detail_bahan_baku`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id_detail_pembelian`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_detail_penjualan`);

--
-- Indexes for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  ADD PRIMARY KEY (`id_detail_produksi`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id_produksi`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  MODIFY `id_detail_bahan_baku` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id_detail_pembelian` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_detail_penjualan` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  MODIFY `id_detail_produksi` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
