-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2019 at 08:22 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gudang-beras`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_baku`
--

CREATE TABLE IF NOT EXISTS `bahan_baku` (
  `id_bahan_baku` varchar(24) NOT NULL,
  `tanggal_terdaftar` date NOT NULL,
  `barang_produksi` varchar(24) NOT NULL,
  `harga_produksi` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan_baku`
--

INSERT INTO `bahan_baku` (`id_bahan_baku`, `tanggal_terdaftar`, `barang_produksi`, `harga_produksi`) VALUES
('RSP001112019018', '2019-11-02', 'BRG001102019004', 560000),
('RSP012102019010', '2019-11-08', 'BRG007112019007', 550000),
('RSP012102019012', '2019-10-12', '', 0),
('RSP012112019019', '2019-11-12', 'BRG001102019004', 15000),
('RSP012112019020', '2019-11-12', 'BRG012112019008', 20000),
('RSP019112019021', '2019-11-19', 'BRG018112019010', 50000),
('RSP030102019016', '2019-10-30', 'BRG001102019004', 100000),
('RSP031102019017', '2019-10-31', 'BRG015102019006', 600000);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` varchar(24) NOT NULL,
  `nama_barang` varchar(64) NOT NULL,
  `qty` double NOT NULL,
  `id_satuan` int(4) NOT NULL,
  `jenis_barang` int(4) NOT NULL,
  `harga` double NOT NULL,
  `ppn` int(4) NOT NULL,
  `nominal_ppn` double NOT NULL,
  `harga_jual` double NOT NULL,
  `ppn_jual` int(4) NOT NULL,
  `nominal_ppn_jual` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `qty`, `id_satuan`, `jenis_barang`, `harga`, `ppn`, `nominal_ppn`, `harga_jual`, `ppn_jual`, `nominal_ppn_jual`) VALUES
('BRG001102019003', 'Karung 5 kg', 44, 3, 1, 15000, 0, 0, 16000, 0, 0),
('BRG001102019004', 'Beras 5 Kg', 640.5, 1, 2, 25000, 1, 2500, 30000, 1, 3000),
('BRG007112019007', 'Beras 15 Kg', 47.5, 3, 2, 70000, 1, 7000, 90000, 1, 9000),
('BRG009102019005', 'Bekatul 5 kg', 93, 3, 3, 25000, 1, 2500, 30000, 0, 0),
('BRG012112019008', 'Beras Super', 92, 1, 2, 50000, 0, 0, 75000, 1, 7500),
('BRG015102019006', 'Padi 10kg', 163.75, 3, 1, 20000, 1, 2000, 25000, 1, 2500),
('BRG018112019009', 'Karung 20 kg', 500, 1, 1, 50000, 1, 5000, 70000, 1, 7000),
('BRG018112019010', 'Beras 20 kg', 65, 1, 2, 60000, 0, 0, 80000, 0, 0),
('BRG030092019002', 'Karung 10 kg', 150, 3, 1, 15000, 0, 0, 20000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` varchar(24) NOT NULL,
  `nama_customer` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('CTM025102019002', 'Toko Pak Jaya', 'Banyumas', '089999000888', 'jaya@gmail.com', '2019-10-25'),
('CTM027092019001', 'Toko Pak Budi', 'Podorejo', '085777888999', 'budi@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `detail_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `detail_bahan_baku` (
  `id_detail_bahan_baku` int(4) NOT NULL,
  `id_bahan_baku` varchar(24) NOT NULL,
  `id_barang_bahan_baku` varchar(24) NOT NULL,
  `qty` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_bahan_baku`
--

INSERT INTO `detail_bahan_baku` (`id_detail_bahan_baku`, `id_bahan_baku`, `id_barang_bahan_baku`, `qty`) VALUES
(4, 'RSP012102019012', 'BRG030092019002', 2),
(9, 'RSP030102019016', 'BRG001102019003', 4),
(10, 'RSP030102019016', 'BRG030092019002', 2),
(22, 'RSP031102019017', 'BRG001102019003', 2),
(44, 'RSP001112019018', 'BRG001102019003', 8),
(45, 'RSP001112019018', 'BRG015102019006', 6),
(46, 'RSP001112019018', 'BRG030092019002', 2),
(47, 'RSP012102019010', 'BRG001102019003', 2),
(48, 'RSP012102019010', 'BRG015102019006', 1),
(49, 'RSP012112019019', 'BRG001102019003', 1),
(50, 'RSP012112019019', 'BRG015102019006', 1),
(51, 'RSP012112019020', 'BRG015102019006', 2),
(52, 'RSP012112019020', 'BRG030092019002', 1),
(53, 'RSP019112019021', 'BRG001102019003', 4),
(54, 'RSP019112019021', 'BRG015102019006', 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE IF NOT EXISTS `detail_pembelian` (
  `id_detail_pembelian` int(4) NOT NULL,
  `id_pembelian` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` double NOT NULL,
  `harga` double NOT NULL,
  `ppn_item` int(4) DEFAULT NULL,
  `nominal_ppn_item` double DEFAULT NULL,
  `sub_total_harga` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`id_detail_pembelian`, `id_pembelian`, `id_barang`, `qty`, `harga`, `ppn_item`, `nominal_ppn_item`, `sub_total_harga`) VALUES
(45, 'PBL025102019001', 'BRG001102019004', 5, 25000, 1, 2500, 137500),
(46, 'PBL025102019001', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(47, 'PBL025102019001', 'BRG001102019003', 5, 15000, 0, 0, 75000),
(48, 'PBL025102019002', 'BRG015102019006', 10, 20000, 1, 2000, 220000),
(49, 'PBL025102019002', 'BRG001102019004', 10, 25000, 1, 2500, 275000),
(50, 'PBL025102019002', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(51, 'PBL025102019003', 'BRG030092019002', 50, 15000, 0, 1500, 825000),
(52, 'PBL025102019003', 'BRG009102019005', 20, 25000, 0, 0, 500000),
(53, 'PBL025102019003', 'BRG015102019006', 25, 20000, 1, 2000, 550000),
(54, 'PBL008112019004', 'BRG001102019004', 100, 25000, 0, 0, 2500000),
(55, 'PBL008112019004', 'BRG015102019006', 50, 20000, 1, 2000, 1100000),
(56, 'PBL012112019005', 'BRG012112019008', 2, 50000, 1, 5000, 110000),
(57, 'PBL012112019005', 'BRG001102019003', 1, 15000, 1, 1500, 16500),
(58, 'PBL012112019005', 'BRG007112019007', 3, 70000, 1, 7000, 231000),
(59, 'PBL012112019005', 'BRG001102019004', 5, 25000, 1, 2500, 137500),
(60, 'PBL012112019005', 'BRG030092019002', 2, 15000, 1, 1500, 33000),
(61, 'PBL019112019006', 'BRG001102019003', 5, 15000, 0, 0, 75000),
(62, 'PBL020112019007', 'BRG012112019008', 505, 50000, 0, 0, 25250000),
(63, 'PBL020112019008', 'BRG012112019008', 505, 50000, 0, 0, 25250000),
(64, 'PBL020112019009', 'BRG012112019008', 2.5, 50000, 0, 0, 125000),
(65, 'PBL023112019010', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(66, 'PBL023112019011', 'BRG001102019003', 2, 15000, 0, 0, 30000),
(67, 'PBL023112019012', 'BRG001102019003', 5, 15000, 0, 0, 75000),
(68, 'PBL023112019013', 'BRG001102019003', 2, 15000, NULL, 0, 30000),
(69, 'PBL023112019014', 'BRG001102019003', 2, 15000, NULL, 0, 55000),
(70, 'PBL023112019015', 'BRG001102019003', 3, 15000, NULL, 0, 45000),
(71, 'PBL023112019016', 'BRG001102019003', 6, 15000, NULL, 0, 90000),
(72, 'PBL023112019017', 'BRG001102019003', 5, 15000, NULL, 0, 75000),
(73, 'PBL023112019018', 'BRG001102019003', 5, 15000, NULL, 0, 75000),
(74, 'PBL023112019019', 'BRG001102019003', 5, 15000, NULL, 0, 75000),
(75, 'PBL023112019020', 'BRG001102019003', 5, 15000, NULL, 0, 75000),
(76, 'PBL023112019021', 'BRG001102019003', 5, 15000, NULL, 0, 75000),
(77, 'PBL023112019022', 'BRG009102019005', 5, 25000, 1, 2500, 137500),
(78, 'PBL023112019023', 'BRG012112019008', 5, 50000, NULL, 0, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE IF NOT EXISTS `detail_penjualan` (
  `id_detail_penjualan` int(4) NOT NULL,
  `id_penjualan` varchar(24) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `qty` double NOT NULL,
  `harga` double NOT NULL,
  `ppn_item` int(4) DEFAULT NULL,
  `nominal_ppn_item` double DEFAULT NULL,
  `sub_total_harga` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id_detail_penjualan`, `id_penjualan`, `id_barang`, `qty`, `harga`, `ppn_item`, `nominal_ppn_item`, `sub_total_harga`) VALUES
(1, 'PJL017112019001', 'BRG001102019003', 5, 16000, 0, 0, 80000),
(2, 'PJL017112019002', 'BRG001102019003', 2, 16000, 0, 0, 32000),
(3, 'PJL017112019003', 'BRG001102019003', 2, 16000, 0, 0, 32000),
(4, 'PJL017112019003', 'BRG001102019004', 2, 30000, 1, 3000, 66000),
(5, 'PJL018112019004', 'BRG007112019007', 2, 90000, 1, 9000, 198000),
(6, 'PJL018112019004', 'BRG009102019005', 2, 30000, 0, 0, 60000),
(7, 'PJL019112019005', 'BRG001102019003', 25, 16000, 0, 0, 400000),
(8, 'PJL019112019006', 'BRG001102019004', 25, 30000, 1, 3000, 825000),
(9, 'PJL019112019007', 'BRG001102019004', 25, 30000, 1, 3000, 825000),
(10, 'PJL019112019008', 'BRG001102019004', 5, 30000, 1, 3000, 165000),
(11, 'PJL019112019009', 'BRG001102019004', 5, 30000, 1, 3000, 165000),
(12, 'PJL020112019010', 'BRG001102019004', 5, 30000, 1, 3000, 165000),
(13, 'PJL020112019011', 'BRG001102019004', 25, 30000, 1, 3000, 82500),
(14, 'PJL023112019012', 'BRG001102019003', 5, 16000, NULL, 0, 80000),
(15, 'PJL023112019013', 'BRG001102019003', 5, 16000, NULL, 0, 80000),
(16, 'PJL023112019014', 'BRG001102019003', 5, 16000, NULL, 0, 80000),
(17, 'PJL023112019015', 'BRG001102019003', 5, 16000, NULL, 0, 80000),
(18, 'PJL023112019016', 'BRG001102019003', 5, 16000, NULL, 0, 80000),
(19, 'PJL023112019017', 'BRG001102019003', 2, 16000, NULL, 0, 32000),
(20, 'PJL023112019018', 'BRG001102019003', 2, 16000, NULL, 0, 32000),
(21, 'PJL024112019019', 'BRG001102019003', 3, 16000, NULL, 0, 48000),
(22, 'PJL024112019019', 'BRG001102019004', 2, 30000, 1, 3000, 66000),
(23, 'PJL024112019019', 'BRG009102019005', 1, 30000, NULL, 0, 30000),
(24, 'PJL024112019020', 'BRG007112019007', 5.5, 90000, 1, 9000, 544500),
(25, 'PJL027112019021', 'BRG001102019004', 2, 30000, 1, 3000, 66000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_produksi`
--

CREATE TABLE IF NOT EXISTS `detail_produksi` (
  `id_detail_produksi` int(4) NOT NULL,
  `id_produksi` varchar(24) NOT NULL,
  `id_barang_bahan_baku` varchar(24) NOT NULL,
  `qty` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_produksi`
--

INSERT INTO `detail_produksi` (`id_detail_produksi`, `id_produksi`, `id_barang_bahan_baku`, `qty`, `total`) VALUES
(1, 'PRD003112019005', 'BRG001102019003', 10, 40),
(2, 'PRD003112019005', 'BRG015102019006', 5, 20),
(3, 'PRD003112019006', 'BRG001102019003', 2, 4),
(4, 'PRD003112019006', 'BRG015102019006', 1, 2),
(5, 'PRD003112019007', 'BRG001102019003', 2, 4),
(6, 'PRD003112019007', 'BRG015102019006', 1, 2),
(7, 'PRD003112019008', 'BRG001102019003', 1, 2),
(8, 'PRD003112019009', 'BRG030092019002', 1, 2),
(9, 'PRD003112019010', 'BRG001102019003', 2, 10),
(10, 'PRD008112019011', 'BRG001102019003', 2, 10),
(11, 'PRD008112019012', 'BRG030092019002', 2, 10),
(12, 'PRD012112019013', 'BRG015102019006', 2, 10),
(13, 'PRD012112019013', 'BRG030092019002', 1, 5),
(14, 'PRD012112019014', 'BRG001102019003', 8, 48),
(15, 'PRD012112019014', 'BRG015102019006', 6, 36),
(16, 'PRD012112019014', 'BRG030092019002', 2, 12),
(17, 'PRD019112019015', 'BRG001102019003', 4, 8),
(18, 'PRD019112019015', 'BRG015102019006', 2, 4),
(19, 'PRD020112019016', 'BRG015102019006', 1.5, 3),
(20, 'PRD020112019016', 'BRG030092019002', 1.25, 3),
(21, 'PRD021112019017', 'BRG015102019006', 1.5, 2.25),
(22, 'PRD021112019017', 'BRG030092019002', 1, 1.5),
(23, 'PRD024112019018', 'BRG001102019003', 4, 12),
(24, 'PRD024112019019', 'BRG001102019003', 1, 2),
(25, 'PRD024112019019', 'BRG015102019006', 1, 2),
(26, 'PRD024112019020', 'BRG001102019003', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `monitoring`
--

CREATE TABLE IF NOT EXISTS `monitoring` (
  `id` int(8) NOT NULL,
  `periode` varchar(8) NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `stok_periode` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monitoring`
--

INSERT INTO `monitoring` (`id`, `periode`, `id_barang`, `stok_periode`) VALUES
(1, '2019-09', 'BRG001102019003', 50),
(2, '2019-11', 'BRG007112019007', 47.5),
(3, '2019-11', 'BRG001102019003', -1),
(4, '2019-11', 'BRG012112019008', 5),
(5, '2019-11', 'BRG001102019004', 0),
(6, '2019-11', 'BRG015102019006', 2),
(7, '2019-11', 'BRG009102019005', 1);

-- --------------------------------------------------------

--
-- Table structure for table `opname`
--

CREATE TABLE IF NOT EXISTS `opname` (
  `id` int(8) NOT NULL,
  `tanggal` date NOT NULL,
  `id_barang` varchar(24) NOT NULL,
  `stok_akhir` double NOT NULL,
  `stok_opname` double NOT NULL,
  `selisih` double NOT NULL,
  `keterangan` int(4) NOT NULL,
  `keterangan_lain` text
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opname`
--

INSERT INTO `opname` (`id`, `tanggal`, `id_barang`, `stok_akhir`, `stok_opname`, `selisih`, `keterangan`, `keterangan_lain`) VALUES
(205, '2019-11-23', 'BRG001102019004', 637.5, 637.5, 0, 1, ''),
(206, '2019-11-23', 'BRG001102019003', 50, 45, -5, 1, ''),
(207, '2019-11-23', 'BRG007112019007', 53, 53, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(24) NOT NULL,
  `nama_pegawai` varchar(64) NOT NULL,
  `jenis_kelamin` int(4) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('PGW024092019001', 'SizkaLH', 2, 'Podorejo', '085777888999', 'sizkalailatul96@gmail.com', '2019-09-24'),
('PGW026092019002', 'Jaya Arifin', 1, 'Pagerwojo', '089999000888', 'sdj@gmail.com', '2019-09-26'),
('PGW026102019004', 'Nurillia Ainia Ratri', 2, 'Tulungagung', '0895367255770', 'nurilliaaira.haltec@gmail.com', '2019-10-26'),
('PGW027092019003', 'Nurmala Sari', 2, 'Lamongan', '089999000888', 'nurmala@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(24) NOT NULL,
  `id_transaksi` varchar(24) DEFAULT NULL,
  `tanggal_pembelian` date NOT NULL,
  `id_supplier` varchar(24) NOT NULL,
  `cara_beli` int(4) NOT NULL,
  `id_pengambil` varchar(24) DEFAULT NULL,
  `nama_pengantar` varchar(64) DEFAULT NULL,
  `total_beli` double NOT NULL,
  `ppn_header` int(4) DEFAULT NULL,
  `nominal_ppn_header` double NOT NULL,
  `total_diskon` double DEFAULT NULL,
  `potongan` double DEFAULT NULL,
  `total_bayar` double NOT NULL,
  `jenis_pembayaran` int(4) NOT NULL,
  `bank` varchar(64) DEFAULT NULL,
  `status_pembelian` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_transaksi`, `tanggal_pembelian`, `id_supplier`, `cara_beli`, `id_pengambil`, `nama_pengantar`, `total_beli`, `ppn_header`, `nominal_ppn_header`, `total_diskon`, `potongan`, `total_bayar`, `jenis_pembayaran`, `bank`, `status_pembelian`) VALUES
('PBL008112019004', 'TRANS20191108001', '2019-11-20', 'SPL025102019003', 2, 'PGW026102019004', '', 3600000, 0, 100000, 5, 20000, 3400000, 2, 'Mandiri', 1),
('PBL012112019005', 'TRANS001', '2019-10-12', 'SPL025102019002', 2, 'PGW024092019001', '', 528000, 0, 48000, 3, 0, 512160, 1, '', 1),
('PBL019112019006', 'TRANS20191119001', '2019-09-19', 'SPL025102019003', 2, 'PGW026092019002', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL020112019007', 'TRANS20191120001', '2019-11-20', 'SPL027092019001', 2, 'PGW024092019001', '', 25250000, 0, 0, 10, 0, 22725000, 1, '', 0),
('PBL020112019008', 'TRANS20191120002', '2019-08-20', 'SPL025102019003', 2, 'PGW026092019002', '', 25250000, 0, 0, 0, 0, 25250000, 1, '', 1),
('PBL020112019009', 'TRANS20191120003', '2019-09-20', 'SPL027092019001', 1, '', 'Ica', 125000, 0, 0, 0, 0, 125000, 1, '', 1),
('PBL023112019010', 'TRANS20191120003', '2019-11-23', 'SPL025102019003', 2, 'PGW026092019002', '', 30000, 0, 0, 0, 0, 30000, 1, '', 1),
('PBL023112019011', 'TRANS20191120003', '2019-11-23', 'SPL025102019003', 2, 'PGW026092019002', '', 30000, 0, 0, 0, 0, 30000, 1, '', 1),
('PBL023112019012', 'TRANS20191120001', '2019-11-23', 'SPL025102019003', 2, 'PGW026092019002', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019013', 'TRANS20191119001', '2019-11-23', 'SPL027092019001', 2, 'PGW026102019004', '', 30000, 0, 0, 0, 0, 30000, 1, '', 1),
('PBL023112019014', 'TRANS20191120003', '2019-11-12', 'SPL025102019002', 2, 'PGW026102019004', '', 55000, 0, 5000, 0, 0, 55000, 1, '', 1),
('PBL023112019015', 'TRANS20191120003', '2019-11-19', 'SPL025102019003', 2, 'PGW026102019004', '', 45000, 0, 0, 0, 0, 45000, 1, '', 1),
('PBL023112019016', 'TRANS20191120003', '2019-11-18', 'SPL025102019003', 2, 'PGW026102019004', '', 90000, 0, 0, 0, 0, 90000, 1, '', 1),
('PBL023112019017', 'TRANS20191120001', '2019-11-13', 'SPL025102019003', 2, 'PGW026102019004', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019018', 'TRANS20191120003', '2019-11-20', 'SPL027092019001', 2, 'PGW026102019004', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019019', 'TRANS20191120001', '2019-10-21', 'SPL025102019003', 2, 'PGW026102019004', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019020', 'TRANS20191120001', '2019-11-13', 'SPL025102019003', 2, 'PGW026102019004', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019021', 'TRANS20191120001', '2019-11-12', 'SPL025102019003', 2, 'PGW026102019004', '', 75000, 0, 0, 0, 0, 75000, 1, '', 1),
('PBL023112019022', 'TRANS20191119001', '2019-11-07', 'SPL025102019003', 2, 'PGW026092019002', '', 137500, 0, 12500, 0, 0, 137500, 1, '', 1),
('PBL023112019023', 'TRANS20191120003', '2019-11-21', 'SPL025102019002', 2, 'PGW026092019002', '', 250000, 0, 0, 0, 0, 250000, 1, '', 1),
('PBL025102019001', 'TRANS20191024001', '2019-10-24', 'SPL027092019001', 2, 'PGW026092019002', '', 350000, 0, 25000, 5, 2500, 330000, 2, 'Mandiri', 0),
('PBL025102019002', 'TRANS20191025001', '2019-10-25', 'SPL025102019003', 1, '', 'Agung Laksono', 632500, 0, 57500, 0, 500, 632000, 1, '', 1),
('PBL025102019003', 'TRANS20191025002', '2019-10-25', 'SPL025102019002', 2, 'PGW024092019001', '', 1875000, 1, 125000, 10, 0, 1687500, 2, 'BRI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(24) NOT NULL,
  `tanggal_penjualan` datetime DEFAULT NULL,
  `id_customer` varchar(24) NOT NULL,
  `cara_jual` int(4) NOT NULL,
  `id_pengantar` varchar(24) DEFAULT NULL,
  `nama_pengambil` varchar(64) DEFAULT NULL,
  `total_jual` double NOT NULL,
  `total_ppn_item` double NOT NULL,
  `ppn_header` int(4) DEFAULT NULL,
  `nominal_ppn_header` double NOT NULL,
  `total_diskon` double DEFAULT NULL,
  `potongan` double DEFAULT NULL,
  `total_bayar` double NOT NULL,
  `bayar` double NOT NULL,
  `kembali` double NOT NULL,
  `jenis_pembayaran` int(4) NOT NULL,
  `bank` varchar(64) DEFAULT NULL,
  `status_penjualan` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `tanggal_penjualan`, `id_customer`, `cara_jual`, `id_pengantar`, `nama_pengambil`, `total_jual`, `total_ppn_item`, `ppn_header`, `nominal_ppn_header`, `total_diskon`, `potongan`, `total_bayar`, `bayar`, `kembali`, `jenis_pembayaran`, `bank`, `status_penjualan`) VALUES
('PJL017112019001', '2019-11-06 00:00:00', 'CTM027092019001', 1, 'PGW027092019003', '', 212000, 0, 0, 12000, 2, 760, 207000, 0, 0, 2, 'Mandiri', 1),
('PJL017112019002', '2019-11-13 00:00:00', 'CTM025102019002', 1, 'PGW026092019002', '', 32000, 0, 0, 0, 1, 680, 31000, 0, 0, 2, 'Mandiri', 1),
('PJL017112019003', '2019-11-21 00:00:00', 'CTM025102019002', 1, 'PGW026102019004', '', 98000, 0, 0, 6000, 2, 40, 96000, 0, 0, 1, '', 1),
('PJL018112019004', '2019-11-21 00:00:00', 'CTM025102019002', 2, '', 'Sizka', 258000, 0, 0, 18000, 2, 840, 252000, 0, 0, 1, '', 0),
('PJL019112019005', '2019-11-19 00:00:00', 'CTM025102019002', 1, 'PGW026092019002', '', 400000, 0, 0, 0, 0, 0, 400000, 0, 0, 1, '', 1),
('PJL019112019006', '0000-00-00 00:00:00', 'CTM027092019001', 1, 'PGW027092019003', '', 825000, 0, 0, 75000, 0, 0, 825000, 0, 0, 1, '', 1),
('PJL019112019007', '0000-00-00 00:00:00', 'CTM025102019002', 1, 'PGW027092019003', '', 825000, 0, 0, 75000, 0, 0, 825000, 0, 0, 1, '', 1),
('PJL019112019008', '0000-00-00 00:00:00', 'CTM025102019002', 1, 'PGW026092019002', '', 165000, 0, 0, 15000, 0, 0, 165000, 0, 0, 1, '', 1),
('PJL019112019009', '0000-00-00 00:00:00', 'CTM025102019002', 1, 'PGW026092019002', '', 165000, 0, 0, 15000, 0, 0, 165000, 0, 0, 1, '', 1),
('PJL020112019010', '2019-10-20 00:01:36', 'CTM025102019002', 1, 'PGW027092019003', '', 165000, 0, 0, 15000, 0, 0, 165000, 0, 0, 1, '', 1),
('PJL020112019011', '2019-11-20 11:40:48', 'CTM025102019002', 1, 'PGW026102019004', '', 82500, 0, 0, 7500, 0, 0, 82500, 0, 0, 1, '', 1),
('PJL023112019012', '2019-11-23 21:00:41', 'CTM025102019002', 1, 'PGW026092019002', '', 80000, 0, 0, 0, 0, 0, 80000, 0, 0, 1, '', 1),
('PJL023112019013', '2019-11-23 21:01:26', 'CTM025102019002', 1, 'PGW026092019002', '', 80000, 0, 0, 0, 0, 0, 80000, 0, 0, 1, '', 1),
('PJL023112019014', '2019-11-23 21:02:21', 'CTM025102019002', 1, 'PGW026092019002', '', 80000, 0, 0, 0, 0, 0, 80000, 0, 0, 1, '', 1),
('PJL023112019015', '2019-11-23 21:05:03', 'CTM025102019002', 1, 'PGW026092019002', '', 80000, 0, 0, 0, 0, 0, 80000, 0, 0, 1, '', 1),
('PJL023112019016', '2019-09-23 21:05:34', 'CTM025102019002', 1, 'PGW026102019004', '', 80000, 0, 0, 0, 0, 0, 80000, 0, 0, 1, '', 1),
('PJL023112019017', '2019-11-23 21:06:46', 'CTM025102019002', 1, 'PGW024092019001', '', 32000, 0, 0, 0, 0, 0, 32000, 0, 0, 1, '', 1),
('PJL023112019018', '2019-11-23 21:12:38', 'CTM025102019002', 1, 'PGW024092019001', '', 32000, 0, 0, 0, 0, 0, 32000, 0, 0, 1, '', 1),
('PJL024112019019', '2019-11-24 15:22:22', 'CTM025102019002', 1, '', '', 144000, 0, 0, 6000, 0, 0, 144000, 0, 0, 1, '', 1),
('PJL024112019020', '2019-11-24 15:27:14', 'CTM027092019001', 1, 'PGW026092019002', '', 544500, 0, 0, 49500, 2, 0, 533610, 0, 0, 1, '', 1),
('PJL027112019021', '2019-11-27 07:53:16', 'CTM025102019002', 1, 'PGW026102019004', '', 66000, 0, 0, 6000, 0, 0, 66000, 70000, 4000, 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE IF NOT EXISTS `produksi` (
  `id_produksi` varchar(24) NOT NULL,
  `tanggal_produksi` date NOT NULL,
  `id_bahan_baku` varchar(24) NOT NULL,
  `id_barang_produksi` varchar(24) DEFAULT NULL,
  `jumlah` double NOT NULL,
  `harga_produksi` double NOT NULL,
  `total_produksi` double NOT NULL,
  `status_produksi` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi`
--

INSERT INTO `produksi` (`id_produksi`, `tanggal_produksi`, `id_bahan_baku`, `id_barang_produksi`, `jumlah`, `harga_produksi`, `total_produksi`, `status_produksi`) VALUES
('PRD003112019005', '2019-11-03', 'RSP029102019015', 'BRG001102019004', 4, 50000, 200000, 1),
('PRD003112019006', '2019-11-03', 'RSP012102019010', 'BRG001102019004', 2, 0, 0, 0),
('PRD003112019007', '2019-11-03', 'RSP012102019010', 'BRG001102019004', 2, 600000, 1200000, 1),
('PRD003112019008', '2019-11-03', 'RSP012102019013', 'BRG001102019004', 2, 60000, 120000, 1),
('PRD003112019009', '2019-11-03', 'RSP012102019011', 'BRG001102019004', 2, 50000, 100000, 0),
('PRD003112019010', '2019-11-03', 'RSP012102019013', 'BRG001102019004', 5, 0, 0, 0),
('PRD008112019011', '2019-11-08', 'RSP012102019013', 'BRG001102019004', 5, 50000, 250000, 1),
('PRD008112019012', '2019-11-08', 'RSP012102019011', 'BRG001102019004', 5, 20000, 100000, 1),
('PRD012112019013', '2019-11-12', 'RSP012112019020', 'BRG012112019008', 5, 20000, 100000, 1),
('PRD012112019014', '2019-11-12', 'RSP001112019018', 'BRG001102019004', 6, 560000, 0, 1),
('PRD019112019015', '2019-11-19', 'RSP019112019021', 'BRG018112019010', 2, 50000, 100000, 1),
('PRD020112019016', '2019-11-20', 'RSP012112019020', 'BRG012112019008', 2, 20000, 40000, 1),
('PRD021112019017', '2019-11-21', 'RSP012112019020', 'BRG012112019008', 1.5, 20000, 30000, 1),
('PRD024112019018', '2019-11-24', 'RSP030102019016', 'BRG001102019004', 3, 100000, 300000, 1),
('PRD024112019019', '2019-11-24', 'RSP012112019019', 'BRG001102019004', 2, 15000, 30000, 1),
('PRD024112019020', '2019-11-24', 'RSP031102019017', 'BRG015102019006', 1, 600000, 600000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(4) NOT NULL,
  `nama_satuan` varchar(64) NOT NULL,
  `alias` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `alias`) VALUES
(1, 'Kilogram', 'kg'),
(3, 'Biji', 'Biji'),
(4, 'Gram', 'gr');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` varchar(24) NOT NULL,
  `nama_supplier` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `telephone`, `email`, `tanggal_terdaftar`) VALUES
('SPL025102019002', 'PT. Jaya Abadi', 'Mojokerto', '087999000777', 'jayaabadi@gmail.com', '2019-10-25'),
('SPL025102019003', 'PT. Makmur Sejahtera', 'Banyuwangi', '087888999000', 'makmursejahtera@gmail.com', '2019-10-25'),
('SPL027092019001', 'PT. Tani Jaya', 'Malang', '087888999000', 'tanijaya@gmail.com', '2019-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL,
  `id_pegawai` varchar(24) NOT NULL,
  `hak_akses` int(4) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` int(4) DEFAULT NULL,
  `tanggal_terdaftar` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `hak_akses`, `username`, `password`, `status`, `tanggal_terdaftar`) VALUES
(1, 'PGW024092019001', 3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-09-26'),
(2, 'PGW026092019002', 2, 'jaya', 'ce9689abdeab50b5bee3b56c7aadee27', 0, '2019-09-26'),
(4, 'PGW027092019003', 1, 'nurmala', '60d075b97943df3727e2150ea7a20eb5', 1, '2019-09-27'),
(5, 'PGW026102019004', 3, 'nurilliaratri', '25f9e794323b453885f5181f1b624d0b', 1, '2019-10-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_baku`
--
ALTER TABLE `bahan_baku`
  ADD PRIMARY KEY (`id_bahan_baku`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  ADD PRIMARY KEY (`id_detail_bahan_baku`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id_detail_pembelian`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_detail_penjualan`);

--
-- Indexes for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  ADD PRIMARY KEY (`id_detail_produksi`);

--
-- Indexes for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opname`
--
ALTER TABLE `opname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id_produksi`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_bahan_baku`
--
ALTER TABLE `detail_bahan_baku`
  MODIFY `id_detail_bahan_baku` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id_detail_pembelian` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_detail_penjualan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  MODIFY `id_detail_produksi` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `monitoring`
--
ALTER TABLE `monitoring`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `opname`
--
ALTER TABLE `opname`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=208;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
