function toFloat(value) {
    value = parseFloat(value);
    if (!$.isNumeric(value)) {
        return 0;
    } else {
        return value;
    }
}

function number_value(str, return_as_zero = true) {
    var parse = str.split(',');
    var result = parse[0].split('.').join('');
    if ($.isNumeric(result)) {
        if (parse[1]) {
            result += '.' + parse[1];
        }
        return parseFloat(result);
    } else {
        if (return_as_zero) {
            return 0;
        } else {
            return result;
        }
    }
}